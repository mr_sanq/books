package com.sanq.product.books.controller;

import com.sanq.product.books.entity.vo.BlockJoinBooksVo;
import com.sanq.product.books.service.BlockJoinBooksService;
import com.sanq.product.config.utils.annotation.LogAnnotation;
import com.sanq.product.config.utils.entity.Pager;
import com.sanq.product.config.utils.entity.Pagination;
import com.sanq.product.config.utils.entity.Response;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/block_join_books")
public class BlockJoinBooksController {

	@Resource
	private BlockJoinBooksService blockJoinBooksService;

	@LogAnnotation(description = "通过ID得到详情")
	@GetMapping(value="/get/{id}")
	public Response getById(HttpServletRequest request, @PathVariable("id") Integer id) {
		BlockJoinBooksVo blockJoinBooksVo = blockJoinBooksService.findById(id);

		return blockJoinBooksVo != null ? new Response().success(blockJoinBooksVo) : new Response().failure();
	}

	@LogAnnotation(description = "删除数据")
	@DeleteMapping(value="/delete")
	public Response deleteById(HttpServletRequest request, @RequestBody BlockJoinBooksVo blockJoinBooksVo) {

		int result = blockJoinBooksService.delete(blockJoinBooksVo);
		return result == 1 ? new Response().success() : new Response().failure();
	}

	@LogAnnotation(description = "分页查询数据")
	@GetMapping(value="/list")
	public Response findListByPager(HttpServletRequest request, BlockJoinBooksVo blockJoinBooksVo, Pagination pagination) {

		Pager<BlockJoinBooksVo> pager = blockJoinBooksService.findListByPage(blockJoinBooksVo, pagination);

		return pager != null ? new Response().success(pager) : new Response().failure();
	}

	@LogAnnotation(description = "查询所有数据")
	@GetMapping(value="/all")
	public Response findList(HttpServletRequest request, BlockJoinBooksVo blockJoinBooksVo) {

		List<BlockJoinBooksVo> list = blockJoinBooksService.findList(blockJoinBooksVo);
		return list != null ? new Response().success(list) : new Response().failure();
	}

	@LogAnnotation(description = "添加数据")
	@PostMapping(value="/save")
	public Response add(HttpServletRequest request, @RequestBody BlockJoinBooksVo blockJoinBooksVo) {

		if (blockJoinBooksVo.getBooksIds() == null || blockJoinBooksVo.getBooksIds().isEmpty())
			return new Response().failure("请选择小说");

		List<BlockJoinBooksVo> list = new ArrayList<>(blockJoinBooksVo.getBooksIds().size());

		blockJoinBooksVo.getBooksIds().stream().forEach(item -> {
			BlockJoinBooksVo joinBooksVo = new BlockJoinBooksVo();
			joinBooksVo.setBlockId(blockJoinBooksVo.getBlockId());
			joinBooksVo.setBookId(item);
			list.add(joinBooksVo);
		});

		try {
            blockJoinBooksService.saveByList(list);
        } catch (Exception e) {
		    return new Response().failure(e.getMessage());
        }

		return new Response().success();
	}

	@LogAnnotation(description = "通过ID修改数据")
	@GetMapping(value="/range")
	public Response range(HttpServletRequest request, Integer blockId) {

		int result = blockJoinBooksService.saveRange(blockId);

		return result != 0 ? new Response().success() : new Response().failure();
	}
}