package com.sanq.product.books.controller;

import com.sanq.product.books.entity.vo.AdvertiseVo;
import com.sanq.product.books.service.AdvertiseService;
import com.sanq.product.config.utils.annotation.LogAnnotation;
import com.sanq.product.config.utils.entity.Pager;
import com.sanq.product.config.utils.entity.Pagination;
import com.sanq.product.config.utils.entity.Response;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/api/advertise")
public class AdvertiseController {

	@Resource
	private AdvertiseService advertiseService;

	@LogAnnotation(description = "通过ID得到详情")
	@GetMapping(value="/get/{id}")
	public Response getById(HttpServletRequest request, @PathVariable("id") Integer id) {
		AdvertiseVo advertiseVo = advertiseService.findById(id);

		return advertiseVo != null ? new Response().success(advertiseVo) : new Response().failure();
	}

	@LogAnnotation(description = "删除数据")
	@DeleteMapping(value="/delete")
	public Response deleteById(HttpServletRequest request, @RequestBody AdvertiseVo advertiseVo) {

		int result = advertiseService.delete(advertiseVo);
		return result == 1 ? new Response().success() : new Response().failure();
	}

	@LogAnnotation(description = "分页查询数据")
	@GetMapping(value="/list")
	public Response findListByPager(HttpServletRequest request, AdvertiseVo advertiseVo, Pagination pagination) {

		Pager<AdvertiseVo> pager = advertiseService.findListByPage(advertiseVo, pagination);

		return pager != null ? new Response().success(pager) : new Response().failure();
	}

	@LogAnnotation(description = "查询所有数据")
	@GetMapping(value="/all")
	public Response findList(HttpServletRequest request, AdvertiseVo advertiseVo) {

		List<AdvertiseVo> list = advertiseService.findList(advertiseVo);
		return list != null ? new Response().success(list) : new Response().failure();
	}

	@LogAnnotation(description = "添加数据")
	@PostMapping(value="/save")
	public Response add(HttpServletRequest request, @RequestBody AdvertiseVo advertiseVo) {

		int result = advertiseService.save(advertiseVo);

		return result == 1 ? new Response().success() : new Response().failure();
	}

	@LogAnnotation(description = "通过ID修改数据")
	@PutMapping(value="/update/{id}")
	public Response updateByKey(HttpServletRequest request,
        @RequestBody AdvertiseVo advertiseVo,
        @PathVariable("id") Integer id) {

		int result = advertiseService.update(advertiseVo, id);

		return result == 1 ? new Response().success() : new Response().failure();
	}
}