package com.sanq.product.books.utils;

import com.sanq.product.books.config.Redis;
import com.sanq.product.books.entity.module.ServiceModule;
import com.sanq.product.books.entity.vo.MemberVo;
import com.sanq.product.config.utils.date.LocalDateUtils;
import com.sanq.product.config.utils.web.JsonUtil;
import com.sanq.product.redis.service.JedisPoolService;

/**
 * com.sanq.product.books.utils.Util
 *
 * @author sanq.Yan
 * @date 2019/8/8
 */
public class Util {

    public static MemberVo getMemberVo (JedisPoolService jedisPoolService, String token) {
        return JsonUtil.json2Obj(jedisPoolService.get(Redis.ReplaceKey.getSysTokenKey(token)), MemberVo.class);
    }
}
