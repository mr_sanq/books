package com.sanq.product.books.controller;

import com.sanq.product.books.entity.vo.SuggestionsVo;
import com.sanq.product.books.kafka.producer.service.NoticeMQService;
import com.sanq.product.books.service.SuggestionsService;
import com.sanq.product.config.utils.annotation.LogAnnotation;
import com.sanq.product.config.utils.entity.Pager;
import com.sanq.product.config.utils.entity.Pagination;
import com.sanq.product.config.utils.entity.Response;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/api/suggestions")
public class SuggestionsController {

    @Resource
    private SuggestionsService suggestionsService;
    @Resource
    private NoticeMQService noticeMQService;

    @LogAnnotation(description = "通过ID得到详情")
    @GetMapping(value = "/get/{id}")
    public Response getById(HttpServletRequest request, @PathVariable("id") Integer id) {
        SuggestionsVo suggestionsVo = suggestionsService.findById(id);

        return suggestionsVo != null ? new Response().success(suggestionsVo) : new Response().failure();
    }

    @LogAnnotation(description = "删除数据")
    @DeleteMapping(value = "/delete")
    public Response deleteById(HttpServletRequest request, @RequestBody SuggestionsVo suggestionsVo) {

        int result = suggestionsService.delete(suggestionsVo);
        return result == 1 ? new Response().success() : new Response().failure();
    }

    @LogAnnotation(description = "分页查询数据")
    @GetMapping(value = "/list")
    public Response findListByPager(HttpServletRequest request, SuggestionsVo suggestionsVo, Pagination pagination) {

        Pager<SuggestionsVo> pager = suggestionsService.findListByPage(suggestionsVo, pagination);

        return pager != null ? new Response().success(pager) : new Response().failure();
    }

    @LogAnnotation(description = "查询所有数据")
    @GetMapping(value = "/all")
    public Response findList(HttpServletRequest request, SuggestionsVo suggestionsVo) {

        List<SuggestionsVo> list = suggestionsService.findList(suggestionsVo);
        return list != null ? new Response().success(list) : new Response().failure();
    }

    @LogAnnotation(description = "添加数据")
    @PostMapping(value = "/save")
    public Response add(HttpServletRequest request, @RequestBody SuggestionsVo suggestionsVo) {

        int result = suggestionsService.save(suggestionsVo);

        return result == 1 ? new Response().success() : new Response().failure();
    }

    @LogAnnotation(description = "通过ID修改数据")
    @PutMapping(value = "/update/{id}")
    public Response updateByKey(HttpServletRequest request,
                                @RequestBody SuggestionsVo suggestionsVo,
                                @PathVariable("id") Integer id) {

        int result = suggestionsService.update(suggestionsVo, id);

        if (result != 0) {
            try {
                noticeMQService.reply2Suggestion(id);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return result == 1 ? new Response().success() : new Response().failure();
    }
}