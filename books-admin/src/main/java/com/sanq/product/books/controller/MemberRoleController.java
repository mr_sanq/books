package com.sanq.product.books.controller;

import com.sanq.product.books.entity.vo.MemberRoleVo;
import com.sanq.product.books.service.MemberRoleService;
import com.sanq.product.config.utils.annotation.LogAnnotation;
import com.sanq.product.config.utils.entity.Pager;
import com.sanq.product.config.utils.entity.Pagination;
import com.sanq.product.config.utils.entity.Response;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/member_role")
public class MemberRoleController {

    @Resource
    private MemberRoleService memberRoleService;

    @LogAnnotation(description = "通过ID得到详情")
    @RequestMapping(value="/get/{id}",method= RequestMethod.GET)
    public Response getById(HttpServletRequest request, @PathVariable("id") Integer id) {
        MemberRoleVo memberRoleVo = memberRoleService.findById(id);

        return memberRoleVo != null ? new Response().success(memberRoleVo) : new Response().failure();
    }

    @LogAnnotation(description = "删除数据")
    @RequestMapping(value="/delete",method= RequestMethod.DELETE)
        public Response deleteById(HttpServletRequest request, @RequestBody MemberRoleVo memberRoleVo) {

        int result = memberRoleService.delete(memberRoleVo);
        return result == 1 ? new Response().success() : new Response().failure();
    }

    @LogAnnotation(description = "分页查询数据")
    @RequestMapping(value="/list",method= RequestMethod.GET)
    public Response findListByPager(HttpServletRequest request, MemberRoleVo memberRoleVo, Pagination pagination) {

        Pager<MemberRoleVo> pager = memberRoleService.findListByPage(memberRoleVo, pagination);

        return pager != null ? new Response().success(pager) : new Response().failure();
    }

    @LogAnnotation(description = "分页查询数据")
    @RequestMapping(value="/getRoleIdListByMemberId",method= RequestMethod.GET)
    public Response getRoleIdListByMemberId(HttpServletRequest request,Integer memberId) {

        List<Integer> roleList = memberRoleService.findRoleIdListByMemberId(memberId);

        return new Response().success(roleList);
    }

    @LogAnnotation(description = "查询所有数据")
    @RequestMapping(value="/all",method= RequestMethod.GET)
    public Response findList(HttpServletRequest request, MemberRoleVo memberRoleVo) {

        List<MemberRoleVo> list = memberRoleService.findList(memberRoleVo);
        return list != null ? new Response().success(list) : new Response().failure();
    }

    @LogAnnotation(description = "添加数据")
    @RequestMapping(value="/save",method= RequestMethod.POST)
    public Response add(HttpServletRequest request, @RequestBody MemberRoleVo memberRoleVo) {

        if(memberRoleVo == null || memberRoleVo.getRoleIds() == null || memberRoleVo.getRoleIds().isEmpty())
            return new Response().failure("请选择角色");

        MemberRoleVo param = new MemberRoleVo();
        param.setUserId(memberRoleVo.getUserId());

        memberRoleService.delete(param);

        List<MemberRoleVo> list = new ArrayList<>(memberRoleVo.getRoleIds().size());

        for(Integer roleId : memberRoleVo.getRoleIds()) {
            param = new MemberRoleVo();
            param.setRoleId(roleId);
            param.setUserId(memberRoleVo.getUserId());
            list.add(param);
        }

        memberRoleService.saveByList(list);
        return new Response().success();
    }

    @LogAnnotation(description = "通过ID修改数据")
    @RequestMapping(value="/update/{id}",method= RequestMethod.PUT)
    public Response updateByKey(HttpServletRequest request, @RequestBody MemberRoleVo memberRoleVo, @PathVariable("id") Integer id) {

        int result = memberRoleService.update(memberRoleVo, id);

        return result == 1 ? new Response().success() : new Response().failure();
    }
}