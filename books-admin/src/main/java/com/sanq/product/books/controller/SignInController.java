package com.sanq.product.books.controller;

import com.sanq.product.books.entity.vo.SignInVo;
import com.sanq.product.books.service.SignInService;
import com.sanq.product.config.utils.annotation.LogAnnotation;
import com.sanq.product.config.utils.entity.Pager;
import com.sanq.product.config.utils.entity.Pagination;
import com.sanq.product.config.utils.entity.Response;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/api/sign_in")
public class SignInController {

	@Resource
	private SignInService signInService;

	@LogAnnotation(description = "通过ID得到详情")
	@GetMapping(value="/get/{id}")
	public Response getById(HttpServletRequest request, @PathVariable("id") Integer id) {
		SignInVo signInVo = signInService.findById(id);

		return signInVo != null ? new Response().success(signInVo) : new Response().failure();
	}

	@LogAnnotation(description = "删除数据")
	@DeleteMapping(value="/delete")
	public Response deleteById(HttpServletRequest request, @RequestBody SignInVo signInVo) {

		int result = signInService.delete(signInVo);
		return result == 1 ? new Response().success() : new Response().failure();
	}

	@LogAnnotation(description = "分页查询数据")
	@GetMapping(value="/list")
	public Response findListByPager(HttpServletRequest request, SignInVo signInVo, Pagination pagination) {

		Pager<SignInVo> pager = signInService.findListByPage(signInVo, pagination);

		return pager != null ? new Response().success(pager) : new Response().failure();
	}

	@LogAnnotation(description = "查询所有数据")
	@GetMapping(value="/all")
	public Response findList(HttpServletRequest request, SignInVo signInVo) {

		List<SignInVo> list = signInService.findList(signInVo);
		return list != null ? new Response().success(list) : new Response().failure();
	}

	@LogAnnotation(description = "添加数据")
	@PostMapping(value="/save")
	public Response add(HttpServletRequest request, @RequestBody SignInVo signInVo) {

		int result = signInService.save(signInVo);

		return result == 1 ? new Response().success() : new Response().failure();
	}

	@LogAnnotation(description = "通过ID修改数据")
	@PutMapping(value="/update/{id}")
	public Response updateByKey(HttpServletRequest request,
        @RequestBody SignInVo signInVo,
        @PathVariable("id") Integer id) {

		int result = signInService.update(signInVo, id);

		return result == 1 ? new Response().success() : new Response().failure();
	}
}