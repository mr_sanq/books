package com.sanq.product.books.controller;

import com.sanq.product.books.entity.vo.RoleVo;
import com.sanq.product.books.service.RoleService;
import com.sanq.product.config.utils.annotation.LogAnnotation;
import com.sanq.product.config.utils.entity.Pager;
import com.sanq.product.config.utils.entity.Pagination;
import com.sanq.product.config.utils.entity.Response;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/api/role")
public class RoleController {

    @Resource
    private RoleService roleService;

    @LogAnnotation(description = "通过ID得到详情")
    @RequestMapping(value="/get/{id}",method= RequestMethod.GET)
    public Response getById(HttpServletRequest request, @PathVariable("id") Integer id) {
        RoleVo roleVo = roleService.findById(id);

        return roleVo != null ? new Response().success(roleVo) : new Response().failure();
    }

    @LogAnnotation(description = "删除数据")
    @RequestMapping(value="/delete",method= RequestMethod.DELETE)
        public Response deleteById(HttpServletRequest request, @RequestBody RoleVo roleVo) {

        int result = roleService.delete(roleVo);
        return result == 1 ? new Response().success() : new Response().failure();
    }

    @LogAnnotation(description = "分页查询数据")
    @RequestMapping(value="/list",method= RequestMethod.GET)
    public Response findListByPager(HttpServletRequest request, RoleVo roleVo, Pagination pagination) {

        Pager<RoleVo> pager = roleService.findListByPage(roleVo, pagination);

        return pager != null ? new Response().success(pager) : new Response().failure();
    }

    @LogAnnotation(description = "查询所有数据")
    @RequestMapping(value="/all",method= RequestMethod.GET)
    public Response findList(HttpServletRequest request, RoleVo roleVo) {

        List<RoleVo> list = roleService.findList(roleVo);
        return list != null ? new Response().success(list) : new Response().failure();
    }

    @LogAnnotation(description = "添加数据")
    @RequestMapping(value="/save",method= RequestMethod.POST)
    public Response add(HttpServletRequest request, @RequestBody RoleVo roleVo) {

        int result = roleService.save(roleVo);

        return result == 1 ? new Response().success() : new Response().failure();
    }

    @LogAnnotation(description = "通过ID修改数据")
    @RequestMapping(value="/update/{id}",method= RequestMethod.PUT)
    public Response updateByKey(HttpServletRequest request, @RequestBody RoleVo roleVo, @PathVariable("id") Integer id) {

        int result = roleService.update(roleVo, id);

        return result == 1 ? new Response().success() : new Response().failure();
    }
}