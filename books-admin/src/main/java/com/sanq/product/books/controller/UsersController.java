package com.sanq.product.books.controller;

import com.sanq.product.books.entity.vo.UsersVo;
import com.sanq.product.books.service.UsersService;
import com.sanq.product.config.utils.annotation.LogAnnotation;
import com.sanq.product.config.utils.date.LocalDateUtils;
import com.sanq.product.config.utils.entity.Pager;
import com.sanq.product.config.utils.entity.Pagination;
import com.sanq.product.config.utils.entity.Response;
import com.sanq.product.config.utils.string.StringUtil;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/users")
public class UsersController {

	@Resource
	private UsersService usersService;

	@LogAnnotation(description = "通过ID得到详情")
	@GetMapping(value="/get/{id}")
	public Response getById(HttpServletRequest request, @PathVariable("id") Integer id) {
		UsersVo usersVo = usersService.findById(id);

		return usersVo != null ? new Response().success(usersVo) : new Response().failure();
	}

	@LogAnnotation(description = "删除数据")
	@DeleteMapping(value="/delete")
	public Response deleteById(HttpServletRequest request, @RequestBody UsersVo usersVo) {

		int result = usersService.delete(usersVo);
		return result == 1 ? new Response().success() : new Response().failure();
	}

	@LogAnnotation(description = "分页查询数据")
	@GetMapping(value="/list")
	public Response findListByPager(HttpServletRequest request, UsersVo usersVo, Pagination pagination) {

		Pager<UsersVo> pager = usersService.findListByPage(usersVo, pagination);

		return pager != null ? new Response().success(pager) : new Response().failure();
	}

	@LogAnnotation(description = "查询所有数据")
	@GetMapping(value="/all")
	public Response findList(HttpServletRequest request, UsersVo usersVo) {

		List<UsersVo> list = usersService.findList(usersVo);
		return list != null ? new Response().success(list) : new Response().failure();
	}

	@LogAnnotation(description = "添加数据")
	@PostMapping(value="/save")
	public Response add(HttpServletRequest request, @RequestBody UsersVo usersVo) {

		int result = usersService.save(usersVo);

		return result == 1 ? new Response().success() : new Response().failure();
	}

	@LogAnnotation(description = "通过ID修改数据")
	@PutMapping(value="/update/{id}")
	public Response updateByKey(HttpServletRequest request,
								@RequestBody UsersVo usersVo,
								@PathVariable("id") Integer id) {

		int result = usersService.update(usersVo, id);

		return result == 1 ? new Response().success() : new Response().failure();
	}

	@LogAnnotation(description = "通过ID修改数据")
	@PutMapping(value="/updateObtainById/{id}")
	public Response updateObtainById(HttpServletRequest request,
								@RequestBody Map<String, Object> map,
								@PathVariable("id") Integer id) {

		UsersVo usersVo = usersService.findById(id);

		switch (StringUtil.toInteger(map.get("isVip"))) {
			case 1:
				//金币
				usersVo.setGold(usersVo.getGold().add(new BigDecimal(StringUtil.toInteger(map.get("obtain")))));
				break;
			case 2:
				//VIP
				Calendar instance = Calendar.getInstance();
				if (usersVo.getVipEndTime().getTime() > LocalDateUtils.nowTime().getTime())
					instance.setTime(usersVo.getVipEndTime());


				instance.add(Calendar.DAY_OF_MONTH, StringUtil.toInteger(map.get("obtain")));

				usersVo.setVipEndTime(instance.getTime());
				break;
		}
		int update = usersService.update(usersVo, id);

		return update != 0 ? new Response().success() : new Response().failure();
	}


}