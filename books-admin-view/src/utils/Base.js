import { MessageBox, Message } from 'element-ui'

export default {
	clearObj(obj) {
		for (var key in obj) {
			var types = typeof obj[key];
			obj[key] = ''
		}
	},
	set2Form(target, source) {
		for (var key in target) {
			target[key] = source[key];
		}
	},
	deleteConfirm(fn) {
		MessageBox.confirm('此操作将永久删除该数据, 是否继续?', '提示', {
			confirmButtonText: '确定',
			cancelButtonText: '取消',
			type: 'warning'
		}).then(fn).catch(() => {
			Message.info({
				type: 'info',
				message: '已取消删除'
			});
		});
	},
	deleteOk() {
		Message.success({
			type: 'success',
			message: '删除成功'
		});
	},
	showOk() {
		Message.success({
			type: 'success',
			message: arguments.length == 0 ? '操作成功' : arguments[0]
		});
	}
}
