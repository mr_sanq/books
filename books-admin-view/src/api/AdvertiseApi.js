import $axios from 'utils/Request.js'

//列表分页
export const getAdvertiseList = data => {
    return $axios({
        url: '/api/advertise/list',
        method: 'get',
        data
    });
}

//获取所有数据
export const getAdvertiseAllList = data => {
    return $axios({
        url: '/api/advertise/all',
        method: 'get',
        data
    });
}

//保存
export const saveAdvertise = data => {
    return $axios({
        url: '/api/advertise/save',
        method: 'post',
        data
    });
}

//删除数据
export const deleteAdvertise = data => {
    return $axios({
        url: '/api/advertise/delete',
        method: 'delete',
        data
    });
}

//修改数据
export const updateAdvertiseById = data => {
    return $axios({
        url: '/api/advertise/update/' + data.id,
        method: 'put',
        data
    });
}