import $axios from 'utils/Request.js'

//列表分页
export const getUsersList = data => {
    return $axios({
        url: '/api/users/list',
        method: 'get',
        data
    });
}

//获取所有数据
export const getUsersAllList = data => {
    return $axios({
        url: '/api/users/all',
        method: 'get',
        data
    });
}

//保存
export const saveUsers = data => {
    return $axios({
        url: '/api/users/save',
        method: 'post',
        data
    });
}

//删除数据
export const deleteUsers = data => {
    return $axios({
        url: '/api/users/delete',
        method: 'delete',
        data
    });
}

//修改数据
export const updateUsersById = data => {
    return $axios({
        url: '/api/users/update/' + data.id,
        method: 'put',
        data
    });
}

//修改数据
export const updateObtainById = data => {
    return $axios({
        url: '/api/users/updateObtainById/' + data.id,
        method: 'put',
        data
    });
}