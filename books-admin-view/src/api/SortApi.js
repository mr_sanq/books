import $axios from 'utils/Request.js'

//列表分页
export const getSortList = data => {
    return $axios({
        url: '/api/sort/list',
        method: 'get',
        data
    });
}

//获取所有数据
export const getSortAllList = data => {
    return $axios({
        url: '/api/sort/all',
        method: 'get',
        data
    });
}

//保存
export const saveSort = data => {
    return $axios({
        url: '/api/sort/save',
        method: 'post',
        data
    });
}

//删除数据
export const deleteSort = data => {
    return $axios({
        url: '/api/sort/delete',
        method: 'delete',
        data
    });
}

//修改数据
export const updateSortById = data => {
    return $axios({
        url: '/api/sort/update/' + data.id,
        method: 'put',
        data
    });
}