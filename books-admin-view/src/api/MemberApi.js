import $axios from 'utils/Request.js'

export const getMemberList = data => {
	return $axios({
		url: '/api/member/list',
		method: 'get',
		data
	});
}

export const getMemberAllList = data => {
	return $axios({
		url: '/api/member/all',
		method: 'get',
		data
	});
}

export const saveMember = data => {
	return $axios({
		url: '/api/member/save',
		method: 'post',
		data
	});
}

export const deleteMemberById = data => {
	return $axios({
		url: '/api/member/delete',
		method: 'delete',
		data
	});
}

export const updateMemberById = data => {
	return $axios({
		url: '/api/member/update/' + data.id,
		method: 'put',
		data
	});
}
export const login = data => {
	return $axios({
		url: '/api/member/login',
		method: 'post',
		data
	});
}


export const getInfoByToken = data => {
	return $axios({
		url: '/api/member/getInfoByToken',
		method: 'get',
		data
	});
}

export const updatePwd = data => {
	return $axios({
		url: '/api/member/updatePwd',
		method: 'put',
		data
	});
}