import $axios from 'utils/Request.js'

export const getRolesList = data => {
	return $axios({
		url: '/api/role/list',
		method: 'get',
		data
	});
}

export const getAllRolesList = data => {
	return $axios({
		url: '/api/role/all',
		method: 'get',
		data
	});
}


export const saveRoles = data => {
	return $axios({
		url: '/api/role/save',
		method: 'post',
		data
	});
}

export const deleteRolesById = data => {
	return $axios({
		url: '/api/role/delete',
		method: 'delete',
		data
	});
}

export const updateRolesById = data => {
	return $axios({
		url: '/api/role/update/' + data.id,
		method: 'put',
		data
	});
}

export const saveRoleByMemberId = data => {
	return $axios({
		url: '/api/member_role/save',
		method: 'post',
		data
	});
}

export const getRoleIdListByMemberId = data => {
	return $axios({
		url: '/api/member_role/getRoleIdListByMemberId',
		method: 'get',
		data
	});
}