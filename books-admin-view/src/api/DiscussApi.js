import $axios from 'utils/Request.js'

//列表分页
export const getDiscussList = data => {
    return $axios({
        url: '/api/discuss/list',
        method: 'get',
        data
    });
}

//获取所有数据
export const getDiscussAllList = data => {
    return $axios({
        url: '/api/discuss/all',
        method: 'get',
        data
    });
}

//保存
export const saveDiscuss = data => {
    return $axios({
        url: '/api/discuss/save',
        method: 'post',
        data
    });
}

//删除数据
export const deleteDiscuss = data => {
    return $axios({
        url: '/api/discuss/delete',
        method: 'delete',
        data
    });
}

//修改数据
export const updateDiscussById = data => {
    return $axios({
        url: '/api/discuss/update/' + data.id,
        method: 'put',
        data
    });
}