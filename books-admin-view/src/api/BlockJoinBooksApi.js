import $axios from 'utils/Request.js'

//列表分页
export const getBlockJoinBooksList = data => {
    return $axios({
        url: '/api/block_join_books/list',
        method: 'get',
        data
    });
}

//获取所有数据
export const getBlockJoinBooksAllList = data => {
    return $axios({
        url: '/api/block_join_books/all',
        method: 'get',
        data
    });
}

//保存
export const saveBlockJoinBooks = data => {
    return $axios({
        url: '/api/block_join_books/save',
        method: 'post',
        data
    });
}

//删除数据
export const deleteBlockJoinBooks = data => {
    return $axios({
        url: '/api/block_join_books/delete',
        method: 'delete',
        data
    });
}

//修改数据
export const updateBlockJoinBooksById = data => {
    return $axios({
        url: '/api/block_join_books/update/' + data.id,
        method: 'put',
        data
    });
}

//
export const range = data => {
    return $axios({
        url: '/api/block_join_books/range',
        method: 'get',
        data
    });
}