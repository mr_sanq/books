import $axios from 'utils/Request.js'
import { config } from 'assets/js/Data'

//列表分页
export const getJobsList = data => {
    data = {
        ...data,
        module: config.module.JOBS
    }
    return $axios({
        url: '/api/jobs/list',
        method: 'get',
        data
    });
}

//获取所有数据
export const getJobsAllList = data => {
    data = {
        ...data,
        module: config.module.JOBS
    }
    return $axios({
        url: '/api/jobs/all',
        method: 'get',
        data
    });
}

//保存
export const saveJobs = data => {
    data = {
        ...data,
        module: config.module.JOBS
    }
    return $axios({
        url: '/api/jobs/save',
        method: 'post',
        data
    });
}

//删除数据
export const deleteJobs = data => {
    data = {
        ...data,
        module: config.module.JOBS
    }
    return $axios({
        url: '/api/jobs/delete',
        method: 'delete',
        data
    });
}

//修改数据
export const updateJobsById = data => {
    data = {
        ...data,
        module: config.module.JOBS
    }
    return $axios({
        url: '/api/jobs/update/' + data.id,
        method: 'put',
        data
    });
}