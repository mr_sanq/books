import $axios from 'utils/Request.js'

//列表分页
export const getChapterList = data => {
    return $axios({
        url: '/api/chapter/list',
        method: 'get',
        data
    });
}

//获取所有数据
export const getChapterAllList = data => {
    return $axios({
        url: '/api/chapter/all',
        method: 'get',
        data
    });
}

//保存
export const saveChapter = data => {
    return $axios({
        url: '/api/chapter/save',
        method: 'post',
        data
    });
}

//删除数据
export const deleteChapter = data => {
    return $axios({
        url: '/api/chapter/delete',
        method: 'delete',
        data
    });
}

//修改数据
export const updateChapterById = data => {
    return $axios({
        url: '/api/chapter/update/' + data.id,
        method: 'put',
        data
    });
}