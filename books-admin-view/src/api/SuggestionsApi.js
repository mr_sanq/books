import $axios from 'utils/Request.js'

//列表分页
export const getSuggestionsList = data => {
    return $axios({
        url: '/api/suggestions/list',
        method: 'get',
        data
    });
}

//获取所有数据
export const getSuggestionsAllList = data => {
    return $axios({
        url: '/api/suggestions/all',
        method: 'get',
        data
    });
}

//保存
export const saveSuggestions = data => {
    return $axios({
        url: '/api/suggestions/save',
        method: 'post',
        data
    });
}

//删除数据
export const deleteSuggestions = data => {
    return $axios({
        url: '/api/suggestions/delete',
        method: 'delete',
        data
    });
}

//修改数据
export const updateSuggestionsById = data => {
    return $axios({
        url: '/api/suggestions/update/' + data.id,
        method: 'put',
        data
    });
}