
import $axios from 'utils/Request.js'


export const deleteFileByUrl = data => {
	return $axios({
		url: '/api/file/delete',
		method: 'post',
		data
	});
}
