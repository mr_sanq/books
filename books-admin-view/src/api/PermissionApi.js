import $axios from 'utils/Request.js'

export const getPermissionList = data => {
	return $axios({
		url: '/api/permission/list',
		method: 'get',
		data
	});
}

export const getPermissionAllList = data => {
	return $axios({
		url: '/api/permission/all',
		method: 'get',
		data
	});
}

export const getPermissionByParentList = data => {
	return $axios({
		url: '/api/permission/getPermissionByParentList',
		method: 'get',
		data
	});
}


export const savePermission = data => {
	return $axios({
		url: '/api/permission/save',
		method: 'post',
		data
	});
}

export const deletePermissionById = data => {
	return $axios({
		url: '/api/permission/delete',
		method: 'delete',
		data
	});
}

export const updatePermissionById = data => {
	return $axios({
		url: '/api/permission/update/' + data.id,
		method: 'put',
		data
	});
}

export const findPermissionIdByRoleId = data => {
	return $axios({
		url: '/api/role_permission/findPermissionIdByRoleId',
		method: 'get',
		data
	});
}

export const savePermissionByRoleId = data => {
	return $axios({
		url: '/api/role_permission/save',
		method: 'post',
		data
	});
}


export const findPermissionByMemberId = data => {
	return $axios({
		url: '/api/permission/findPermissionByMemberId',
		method: 'get',
		data
	});
}

