import $axios from 'utils/Request.js'

//列表分页
export const getNoticeList = data => {
    return $axios({
        url: '/api/notice/list',
        method: 'get',
        data
    });
}

//获取所有数据
export const getNoticeAllList = data => {
    return $axios({
        url: '/api/notice/all',
        method: 'get',
        data
    });
}

//保存
export const saveNotice = data => {
    return $axios({
        url: '/api/notice/save',
        method: 'post',
        data
    });
}

//删除数据
export const deleteNotice = data => {
    return $axios({
        url: '/api/notice/delete',
        method: 'delete',
        data
    });
}

//修改数据
export const updateNoticeById = data => {
    return $axios({
        url: '/api/notice/update/' + data.id,
        method: 'put',
        data
    });
}