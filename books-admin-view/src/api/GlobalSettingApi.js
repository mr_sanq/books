import $axios from 'utils/Request.js'

//列表分页
export const getGlobalSettingList = data => {
    return $axios({
        url: '/api/global_setting/list',
        method: 'get',
        data
    });
}

//获取所有数据
export const getGlobalSettingAllList = data => {
    return $axios({
        url: '/api/global_setting/all',
        method: 'get',
        data
    });
}

//保存
export const saveGlobalSetting = data => {
    return $axios({
        url: '/api/global_setting/save',
        method: 'post',
        data
    });
}

//删除数据
export const deleteGlobalSetting = data => {
    return $axios({
        url: '/api/global_setting/delete',
        method: 'delete',
        data
    });
}

//修改数据
export const updateGlobalSettingById = data => {
    return $axios({
        url: '/api/global_setting/update/' + data.id,
        method: 'put',
        data
    });
}