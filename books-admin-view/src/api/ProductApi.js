import $axios from 'utils/Request.js'

//列表分页
export const getProductList = data => {
    return $axios({
        url: '/api/product/list',
        method: 'get',
        data
    });
}

//获取所有数据
export const getProductAllList = data => {
    return $axios({
        url: '/api/product/all',
        method: 'get',
        data
    });
}

//保存
export const saveProduct = data => {
    return $axios({
        url: '/api/product/save',
        method: 'post',
        data
    });
}

//删除数据
export const deleteProduct = data => {
    return $axios({
        url: '/api/product/delete',
        method: 'delete',
        data
    });
}

//修改数据
export const updateProductById = data => {
    return $axios({
        url: '/api/product/update/' + data.id,
        method: 'put',
        data
    });
}