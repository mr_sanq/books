import $axios from 'utils/Request.js'

//列表分页
export const getOrdersList = data => {
    return $axios({
        url: '/api/orders/list',
        method: 'get',
        data
    });
}

//获取所有数据
export const getOrdersAllList = data => {
    return $axios({
        url: '/api/orders/all',
        method: 'get',
        data
    });
}

//保存
export const saveOrders = data => {
    return $axios({
        url: '/api/orders/save',
        method: 'post',
        data
    });
}

//删除数据
export const deleteOrders = data => {
    return $axios({
        url: '/api/orders/delete',
        method: 'delete',
        data
    });
}

//修改数据
export const updateOrdersById = data => {
    return $axios({
        url: '/api/orders/update/' + data.id,
        method: 'put',
        data
    });
}