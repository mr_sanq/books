import $axios from 'utils/Request.js'

//列表分页
export const getHistoryList = data => {
    return $axios({
        url: '/api/history/list',
        method: 'get',
        data
    });
}

//获取所有数据
export const getHistoryAllList = data => {
    return $axios({
        url: '/api/history/all',
        method: 'get',
        data
    });
}

//保存
export const saveHistory = data => {
    return $axios({
        url: '/api/history/save',
        method: 'post',
        data
    });
}

//删除数据
export const deleteHistory = data => {
    return $axios({
        url: '/api/history/delete',
        method: 'delete',
        data
    });
}

//修改数据
export const updateHistoryById = data => {
    return $axios({
        url: '/api/history/update/' + data.id,
        method: 'put',
        data
    });
}