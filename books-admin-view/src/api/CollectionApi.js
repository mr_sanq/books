import $axios from 'utils/Request.js'

//列表分页
export const getCollectionList = data => {
    return $axios({
        url: '/api/collection/list',
        method: 'get',
        data
    });
}

//获取所有数据
export const getCollectionAllList = data => {
    return $axios({
        url: '/api/collection/all',
        method: 'get',
        data
    });
}

//保存
export const saveCollection = data => {
    return $axios({
        url: '/api/collection/save',
        method: 'post',
        data
    });
}

//删除数据
export const deleteCollection = data => {
    return $axios({
        url: '/api/collection/delete',
        method: 'delete',
        data
    });
}

//修改数据
export const updateCollectionById = data => {
    return $axios({
        url: '/api/collection/update/' + data.id,
        method: 'put',
        data
    });
}