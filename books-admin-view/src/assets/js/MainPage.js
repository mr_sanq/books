import MainComponent from 'common/Main'

const MainPage = {
    install (Vue) {
        Vue.component('MainPage', MainComponent)
    }
}
export default MainPage