import PaginationComponent from 'common/Pagination'

const Pagination = {
    install (Vue) {
        Vue.component('Pagination', PaginationComponent)
    }
}
export default Pagination