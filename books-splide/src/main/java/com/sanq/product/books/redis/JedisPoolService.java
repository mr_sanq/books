package com.sanq.product.books.redis;

import java.util.List;
import java.util.Set;

/**
 * version: redis
 * ---------------------
 *
 * @param
 * @author sanq.Yan
 * @date 2019/6/29
 */
public interface JedisPoolService extends com.sanq.product.redis.service.JedisPoolService {
    double getScope(String key, String member);
}
