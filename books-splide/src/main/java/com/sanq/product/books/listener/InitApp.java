package com.sanq.product.books.listener;

import com.sanq.product.books.entity.vo.JobsVo;
import com.sanq.product.books.kafka.producer.service.SplideMQService;
import com.sanq.product.books.redis.JedisPoolService;
import com.sanq.product.books.service.JobsService;
import com.sanq.product.books.utils.JobManage;
import org.apache.commons.lang3.RandomUtils;
import org.quartz.Scheduler;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

/**
 * com.sanq.product.office.cabinet.web.listener.InitApp
 *
 * @author sanq.Yan
 * @date 2019/11/15
 */
@Component
public class InitApp {

    @Resource
    private JobsService jobsService;
    @Resource
    private JedisPoolService jedisPoolService;
    @Resource
    private Scheduler scheduler;
    @Resource
    private SplideMQService splideMQService;

    @PostConstruct
    @Scope(scopeName = "singleton")
    public void init() {
        String key = "JOBS:INIT";
        //初始化启动job
        if (!jedisPoolService.exists(key)) {
            JobsVo jobsVo = new JobsVo();
            jobsVo.setIsEnable(1);
            JobManage.startJob(scheduler, jobsService.findList(jobsVo));

            jedisPoolService.incrAtTime(key, 50 + RandomUtils.nextInt(0, 50));

            //启动mq， 存储数据
            splideMQService.startSaveToDB("1", "books");
            splideMQService.startSaveToDB("2", "chapter");
            splideMQService.startSaveToDB("3", "content");
        }


    }

}
