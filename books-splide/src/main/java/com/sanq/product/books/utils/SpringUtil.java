package com.sanq.product.books.utils;

import com.google.common.base.Preconditions;
import com.sanq.product.config.utils.string.StringUtil;
import org.quartz.JobExecutionContext;
import org.springframework.context.ApplicationContext;

public class SpringUtil {

    private SpringUtil() {
    }

    private volatile static SpringUtil INSTANCE = null;

    private static ApplicationContext mApplicationContext;

    public static SpringUtil getInstance(JobExecutionContext context) throws Exception {
        if (INSTANCE == null) {
            synchronized (SpringUtil.class) {
                if (INSTANCE == null) {
                    INSTANCE = new SpringUtil();
                    mApplicationContext = (ApplicationContext) context.getScheduler().getContext().get("applicationContext");
                }
            }
        }
        return INSTANCE;
    }

    public Object getBean(String clazzName) throws NullPointerException {


        if (StringUtil.isEmpty(clazzName))
            return null;

        Preconditions.checkNotNull(mApplicationContext);

        return mApplicationContext.getBean(clazzName);

    }

    public Object getBean(Class clazzName) throws NullPointerException {

        Preconditions.checkNotNull(mApplicationContext);

        return mApplicationContext.getBean(clazzName);

    }
}
