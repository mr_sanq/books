package com.sanq.product.books.jobs;

import com.sanq.product.books.module.ServiceModule;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * com.sanq.product.books.jobs.BiQuGeLaJobs
 *
 * <p>
 * 新笔趣阁
 * <p>
 * http://www.xbiquge.la/xiaoshuodaquan/
 *
 * @author sanq.Yan
 * @date 2019/12/17
 */
public class BiQuGeLaJobs extends SplideJob implements Job {

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        try {
            ServiceModule module = getModule(context);

            getService(context).startSplideBooks(module, module.getUrl().concat("xiaoshuodaquan/"));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
