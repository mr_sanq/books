package com.sanq.product.books.redis.impl.single;

import com.sanq.product.books.redis.JedisPoolService;
import org.springframework.stereotype.Service;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import javax.annotation.Resource;

/**
 * Created by Xiezhyan on 2019/1/18.
 */

@Service("jedisPoolService")
public class JedisSingleServiceImpl extends com.sanq.product.redis.service.impl.single.JedisSingleServiceImpl implements JedisPoolService {

    @Resource
    private JedisPool jedisPool;

    @Override
    public double getScope(String key, String member) {
        try (Jedis jedis = jedisPool.getResource()) {
            return jedis.zscore(key, member);
        }
    }
}
