package com.sanq.product.books.redis.impl.cluster;

import com.sanq.product.books.redis.JedisPoolService;
import org.springframework.stereotype.Service;
import redis.clients.jedis.*;

import javax.annotation.Resource;

/**
 * Created by Xiezhyan on 2019/1/18.
 */
@Service("jedisPoolService")
public class JedisClusterServiceImpl extends com.sanq.product.redis.service.impl.cluster.JedisClusterServiceImpl implements JedisPoolService {

    @Resource
    private JedisCluster jedisCluster;

    @Override
    public double getScope(String key, String member) {
        return jedisCluster.zscore(key, member);
    }
}
