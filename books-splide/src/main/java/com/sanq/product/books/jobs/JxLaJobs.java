package com.sanq.product.books.jobs;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * com.sanq.product.books.jobs.JxLaJobs
 *
 * <p>
 * 笔趣阁
 * <p>
 * https://www.jx.la/
 *
 * @author sanq.Yan
 * @date 2019/12/18
 */
public class JxLaJobs extends SplideJob implements Job {
    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        super.execute(context);
    }
}
