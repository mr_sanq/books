package com.sanq.product.books.jobs;

import com.sanq.product.books.module.ServiceModule;
import com.sanq.product.books.splide.SplideAppService;
import com.sanq.product.books.utils.SpringUtil;
import com.sanq.product.config.utils.web.JsonUtil;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * com.sanq.product.books.jobs.SplideJob
 *
 * 基础任务类，
 *
 * @author sanq.Yan
 * @date 2019/12/19
 */
public class SplideJob implements Job {

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        try {
            ServiceModule module = getModule(context);

            getService(context).startSplideBooks(module, module.getUrl());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected ServiceModule getModule(JobExecutionContext context) {
        return JsonUtil.json2Obj(context.getJobDetail().getJobDataMap().getString("extra"), ServiceModule.class);
    }

    protected SplideAppService getService(JobExecutionContext context) throws Exception {
        return (SplideAppService) SpringUtil.getInstance(context).getBean(SplideAppService.class);
    }
}
