package com.sanq.product.books.kafka.producer.listeners;

import com.sanq.product.books.config.Redis;
import com.sanq.product.books.kafka.producer.service.SplideMQService;
import com.sanq.product.books.kafka.proxy.StepProxy;
import com.sanq.product.books.service.BooksService;
import com.sanq.product.books.service.ChapterContentService;
import com.sanq.product.books.service.ChapterService;
import com.sanq.product.redis.service.JedisPoolService;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * com.sanq.product.books.kafka.producer.listeners.SplideTopicListener
 *
 * @author sanq.Yan
 * @date 2019/12/18
 */
@Component
public class SplideTopicListener {

    @Resource
    private SplideMQService splideMQService;
    @Resource
    private JedisPoolService jedisPoolService;
    @Resource
    private StepProxy stepProxy;

    @Resource
    private BooksService booksService;
    @Resource
    private ChapterService chapterService;
    @Resource
    private ChapterContentService chapterContentService;


    public void saveData2DB(String value, String key) {
        switch (key) {
            case "books":
                if (value.equals("1")) {
                    saveBooks();
                }
                break;
            case "chapter":
                if (value.equals("2")) {
                    saveChapter();
                }
                break;
            case "content":
                if (value.equals("3")) {
                    saveChapterContent();
                }
                break;
        }

        splideMQService.startSaveToDB(value, key);

    }

    private void saveChapterContent() {

        long llen = jedisPoolService.llen(Redis.RedisKey.SPLIDE_CHAPTER_CONTENT_LIST_KEY);
        chapterContentService.saveChapterContents(stepProxy.get(llen));
    }

    private void saveChapter() {

        long llen = jedisPoolService.llen(Redis.RedisKey.SPLIDE_CHAPTER_LIST_KEY);
        chapterService.saveChapters(stepProxy.get(llen));
    }

    private void saveBooks() {
        long llen = jedisPoolService.llen(Redis.RedisKey.SPLIDE_BOOKS_LIST_KEY);

        booksService.saveBooks(stepProxy.get(llen));
    }
}
