package com.sanq.product.books.kafka.producer.listeners;

import com.sanq.product.books.config.Redis;
import com.sanq.product.books.entity.vo.*;
import com.sanq.product.books.es.module.vo.BooksSearchVo;
import com.sanq.product.books.es.service.BooksSearchService;
import com.sanq.product.books.service.*;
import com.sanq.product.config.utils.string.StringUtil;
import com.sanq.product.config.utils.web.JsonUtil;
import com.sanq.product.redis.service.JedisPoolService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * com.sanq.product.books.kafka.producer.listeners.BooksTopicListener
 *
 * @author sanq.Yan
 * @date 2019/8/15
 */
@Component
public class BooksTopicListener {

    @Resource
    private BooksService booksService;
    @Resource
    private ChapterService chapterService;
    @Resource
    private SortJoinBooksService sortJoinBooksService;
    @Resource
    private BlockJoinBooksService blockJoinBooksService;
    @Resource
    private BooksShelfService booksShelfService;
    @Resource
    private CollectionService collectionService;
    @Resource
    private DiscussService discussService;
    @Resource
    private HistoryService historyService;
    @Resource
    private PayHistoryService payHistoryService;
    @Resource
    private BooksSearchService booksSearchService;
    @Resource
    private JedisPoolService jedisPoolService;


    public void deleteBooks(String data) {
        Long booksId = StringUtil.toLong(data);

        ChapterVo chapterVo = new ChapterVo();
        chapterVo.setBookId(booksId);
        chapterService.delete(chapterVo);

        SortJoinBooksVo sortJoinBooksVo = new SortJoinBooksVo();
        sortJoinBooksVo.setBooksId(booksId);
        sortJoinBooksService.delete(sortJoinBooksVo);

        BlockJoinBooksVo blockJoinBooksVo = new BlockJoinBooksVo();
        blockJoinBooksVo.setBookId(booksId);
        blockJoinBooksService.delete(blockJoinBooksVo);

        BooksShelfVo booksShelfVo = new BooksShelfVo();
        booksShelfVo.setBookId(booksId);
        booksShelfService.delete(booksShelfVo);

        CollectionVo collectionVo = new CollectionVo();
        collectionVo.setBookId(booksId);
        collectionService.delete(collectionVo);

        DiscussVo discussVo = new DiscussVo();
        discussVo.setBookId(booksId);
        discussService.delete(discussVo);

        PayHistoryVo payHistoryVo = new PayHistoryVo();
        payHistoryVo.setBookId(booksId);
        payHistoryService.delete(payHistoryVo);

        HistoryVo historyVo = new HistoryVo();
        historyVo.setBookId(booksId);
        historyService.delete(historyVo);

        //从redis中删除章节内容
        jedisPoolService.deletes(Redis.ReplaceKey.getChapterContentKey(booksId, -1L));

        booksSearchService.deleteBooksById(booksId + "");
    }

    public void save2Es(String data, String key) {

        if ("SPLIDE".equals(key)) {
            // 批量添加
            List<BooksVo> list = JsonUtil.json2ObjList(data, BooksVo.class);

            List<BooksSearchVo> booksSearchVoList = new ArrayList<>(list.size());

            list.forEach(booksVo -> {
                BooksSearchVo booksSearchVo = new BooksSearchVo();

                BeanUtils.copyProperties(booksVo, booksSearchVo);

                booksSearchVo.setChildSortId(sortJoinBooksService.findSortIdList(booksVo.getId()));
                booksSearchVoList.add(booksSearchVo);
            });

            booksSearchService.saveBooks(booksSearchVoList);

        } else {

            Long booksId = StringUtil.toLong(data);

            BooksVo booksVo = null;

            while (true) {
                if (booksVo != null)
                    break;

                booksVo = booksService.findById(booksId);
            }

            BooksSearchVo booksSearchVo;
            switch (key) {
                case "C":
                    //保存
                    booksSearchVo = new BooksSearchVo();
                    BeanUtils.copyProperties(booksVo, booksSearchVo);

                    booksSearchService.saveBooks(booksSearchVo);
                    break;
                case "U":
                    booksSearchVo = new BooksSearchVo();
                    BeanUtils.copyProperties(booksVo, booksSearchVo);

                    booksSearchService.updateBooks(booksSearchVo);
                    //修改
                    break;
                case "S":

                    booksSearchVo = booksSearchService.findBooksById(booksId + "");
                    booksSearchVo.setChildSortId(sortJoinBooksService.findSortIdList(booksId));

                    booksSearchService.updateBooks(booksSearchVo);
                    break;
            }
        }
    }

    public void deleteChapter(String data) {
        Long chapterId = StringUtil.toLong(data);

        PayHistoryVo payHistoryVo = new PayHistoryVo();
        payHistoryVo.setChapterId(chapterId);
        payHistoryService.delete(payHistoryVo);

        jedisPoolService.deletes(Redis.ReplaceKey.getChapterContentKey(-1L, chapterId));
    }
}
