package com.sanq.product.books.kafka.proxy;

/**
 * com.sanq.product.books.kafka.proxy.StepProxy
 *
 * @author sanq.Yan
 * @date 2019/12/18
 */
public interface StepProxy {
    long getStep();

    long get(long len);
}
