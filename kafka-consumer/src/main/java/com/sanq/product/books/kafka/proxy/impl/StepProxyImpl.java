package com.sanq.product.books.kafka.proxy.impl;

import com.sanq.product.books.kafka.proxy.StepProxy;
import org.springframework.stereotype.Component;

/**
 * com.sanq.product.books.kafka.proxy.impl.StepProxyImpl
 *
 * @author sanq.Yan
 * @date 2019/12/18
 */
@Component
public class StepProxyImpl implements StepProxy {
    @Override
    public long getStep() {
        return 30L;
    }

    @Override
    public long get(long len) {
        if (len >= getStep()) {
            long setup = len / getStep();
            long i;
            if (setup > 10L) {
                i = 10L * getStep();
            } else {
                i = setup * getStep();
            }
            return i;
        }
        return 0;
    }
}
