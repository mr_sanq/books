package com.sanq.product.books.kafka;

import com.sanq.product.books.kafka.producer.service.SplideMQService;
import com.sanq.product.books.kafka.producer.service.VisiLogMQService;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * com.sanq.product.books.kafka.InitLineRunner
 *
 * @author sanq.Yan
 * @date 2019/8/31
 */
@Component
public class InitLineRunner implements ApplicationListener<ContextRefreshedEvent> {

    @Resource
    private VisiLogMQService visiLogMQService;
    @Resource
    private SplideMQService splideMQService;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        if (contextRefreshedEvent.getApplicationContext().getParent() == null) {
            //开始保存日志信息
            visiLogMQService.saveLog2Es("1");
            splideMQService.startSaveToDB("1", "books");
            splideMQService.startSaveToDB("2", "chapter");
            splideMQService.startSaveToDB("3", "content");
        }
    }
}
