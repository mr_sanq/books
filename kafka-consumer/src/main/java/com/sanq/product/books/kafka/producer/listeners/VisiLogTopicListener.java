package com.sanq.product.books.kafka.producer.listeners;

import com.sanq.product.books.config.EsIndexs;
import com.sanq.product.books.config.Redis;
import com.sanq.product.books.es.service.VisiLogSearchService;
import com.sanq.product.books.kafka.producer.service.VisiLogMQService;
import com.sanq.product.books.kafka.proxy.StepProxy;
import com.sanq.product.config.utils.date.LocalDateUtils;
import com.sanq.product.redis.service.JedisPoolService;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class VisiLogTopicListener {

    @Resource
    private VisiLogMQService visiLogMQService;
    @Resource
    private JedisPoolService jedisPoolService;
    @Resource
    private VisiLogSearchService visiLogSearchService;

    @Resource
    private StepProxy stepProxy;

    public void saveLog(String data) {

        switch (data) {
            case "1":
                writeLog2Es();
                visiLogMQService.saveLog2Es("1");
                break;
            case "2":
                // 删除reids 删除es索引
                jedisPoolService.delete(Redis.ReplaceKey.getVisiLogKey(LocalDateUtils.lastDayStartTime()));
                //删除索引
                try {
                    visiLogSearchService.deleteIndex(EsIndexs.ReplaceIndex.getVisiLogIndex(LocalDateUtils.lastDayStartTime()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    private void writeLog2Es() {
        String visiLogKey = Redis.ReplaceKey.getVisiLogKey(LocalDateUtils.nowTime());

        long len = jedisPoolService.llen(visiLogKey);

        visiLogSearchService.saveLogs(stepProxy.get(len));
    }
}
