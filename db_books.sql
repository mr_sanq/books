/*
Navicat MySQL Data Transfer

Source Server         : 192.168.87.130
Source Server Version : 50728
Source Host           : 192.168.87.130:3306
Source Database       : db_books

Target Server Type    : MYSQL
Target Server Version : 50728
File Encoding         : 65001

Date: 2020-01-10 09:01:29
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for advertise
-- ----------------------------
DROP TABLE IF EXISTS `advertise`;
CREATE TABLE `advertise` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data_key` varchar(10) DEFAULT NULL COMMENT '基础数据key',
  `adv_name` varchar(50) DEFAULT NULL COMMENT '广告名称',
  `adv_url` varchar(200) DEFAULT NULL COMMENT '广告链接',
  `adv_description` varchar(200) DEFAULT NULL COMMENT '描述',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `img_url` varchar(200) DEFAULT NULL COMMENT '图片地址',
  `target` varchar(20) DEFAULT '_self' COMMENT '打开方式: target的属性 _blank, _self，_parent, _top',
  `can_in_site` tinyint(4) DEFAULT '1' COMMENT '1. 站内 2. 站外',
  `weight` tinyint(4) DEFAULT '1' COMMENT '权重',
  `adv_load_type` tinyint(4) DEFAULT '1' COMMENT '1. 直连   2. js',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='广告信息';

-- ----------------------------
-- Records of advertise
-- ----------------------------
INSERT INTO `advertise` VALUES ('1', '1.1.1.2', 'banner1', 'https://img.17k.com/cmsimg/default/2019/07/1562637705.59742885.jpg', 'banner1', '2019-07-11 20:49:36', 'https://img.17k.com/cmsimg/default/2019/07/1562637705.59742885.jpg', '_self', '1', '2', '1');
INSERT INTO `advertise` VALUES ('2', '1.1.1.1', 'banner2', 'https://img.17k.com/cmsimg/default/2019/07/1562637666.56964285.jpg', 'banner2', '2019-07-11 20:49:54', 'https://img.17k.com/cmsimg/default/2019/07/1562637666.56964285.jpg', '_self', '1', '3', '1');
INSERT INTO `advertise` VALUES ('3', '1.1.1.3', 'banner3', 'https://img.17k.com/cmsimg/default/2019/07/1562116898.65366572.jpg', 'banner3', '2019-07-11 20:50:15', 'https://img.17k.com/cmsimg/default/2019/07/1562116898.65366572.jpg', '_self', '1', '1', '1');
INSERT INTO `advertise` VALUES ('4', '1.1.2.1', '聪投广告', 'http://www.baidu.com', '聪投广告', '2019-07-14 15:32:28', 'https://img.yhqfx1.com//2019/07/01/6298A2792DFE428BBCDCF20F7B60001A.jpg', '_blank', '2', '3', '1');

-- ----------------------------
-- Table structure for area_report
-- ----------------------------
DROP TABLE IF EXISTS `area_report`;
CREATE TABLE `area_report` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `day` date DEFAULT NULL,
  `views` int(11) DEFAULT NULL,
  `ip_views` int(11) DEFAULT NULL,
  `city_name` varchar(50) DEFAULT NULL,
  `prov_name` varchar(50) DEFAULT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='区域统计报表';

-- ----------------------------
-- Records of area_report
-- ----------------------------

-- ----------------------------
-- Table structure for base_data
-- ----------------------------
DROP TABLE IF EXISTS `base_data`;
CREATE TABLE `base_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data_name` varchar(50) DEFAULT NULL COMMENT '名称',
  `parent_id` varchar(10) DEFAULT NULL COMMENT '父节点',
  `data_key` varchar(10) DEFAULT NULL COMMENT 'key: 1.1.1 不能重复',
  `data_desc` varchar(200) DEFAULT NULL COMMENT '描述',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 COMMENT='基础数据';

-- ----------------------------
-- Records of base_data
-- ----------------------------
INSERT INTO `base_data` VALUES ('1', '广告', '0', '1.0.1', '广告父标记', '2019-07-11 20:46:04');
INSERT INTO `base_data` VALUES ('2', 'banner轮播', '1', '1.1.1', 'banner轮播', '2019-07-11 20:46:32');
INSERT INTO `base_data` VALUES ('3', '精选banner广告', '2', '1.1.1.1', '精选banner广告', '2019-07-11 20:47:04');
INSERT INTO `base_data` VALUES ('4', '男生banner广告', '2', '1.1.1.2', '男生banner广告', '2019-07-11 20:47:17');
INSERT INTO `base_data` VALUES ('5', '女生banner广告', '2', '1.1.1.3', '女生banner广告', '2019-07-11 20:47:30');
INSERT INTO `base_data` VALUES ('6', '版块类型', '0', '2.0.1', '版块类型', '2019-07-13 07:58:30');
INSERT INTO `base_data` VALUES ('7', '精选', '6', '2.6.1', '精选', '2019-07-13 07:58:46');
INSERT INTO `base_data` VALUES ('8', '男生', '6', '2.6.2', '男生', '2019-07-13 07:59:17');
INSERT INTO `base_data` VALUES ('9', '女生', '6', '2.6.3', '女生', '2019-07-13 07:59:35');
INSERT INTO `base_data` VALUES ('10', '小说', '0', '3.0.1', '小说', '2019-07-14 15:18:49');
INSERT INTO `base_data` VALUES ('11', '是否完结', '10', '3.10.1', '是否完结', '2019-07-14 15:19:34');
INSERT INTO `base_data` VALUES ('12', '完结', '11', '3.11.1', '完结', '2019-07-14 15:20:29');
INSERT INTO `base_data` VALUES ('13', '连载', '11', '3.11.2', '连载', '2019-07-14 15:21:08');
INSERT INTO `base_data` VALUES ('14', '首页-中间banner', '24', '1.1.2.1', '首页-中间banner-最多加载3条', '2019-07-14 15:32:16');
INSERT INTO `base_data` VALUES ('15', '消息中心', '0', '4.0.1', '消息中心，是否已经阅读', '2019-07-28 08:56:00');
INSERT INTO `base_data` VALUES ('16', '未阅读', '15', '4.15.1', '未阅读', '2019-07-28 08:56:27');
INSERT INTO `base_data` VALUES ('17', '已阅读', '15', '4.15.2', '已阅读', '2019-07-28 08:56:41');
INSERT INTO `base_data` VALUES ('18', '个人消息', '15', '4.15.3', '个人消息', '2019-07-28 09:07:55');
INSERT INTO `base_data` VALUES ('19', '集体消息', '15', '4.15.4', '集体消息', '2019-07-28 09:08:04');
INSERT INTO `base_data` VALUES ('20', '意见反馈', '0', '5.0.1', '意见反馈中的选择问题', '2019-08-10 08:58:10');
INSERT INTO `base_data` VALUES ('21', '功能反馈', '20', '5.20.1', '功能反馈', '2019-07-31 07:49:20');
INSERT INTO `base_data` VALUES ('22', '充值反馈', '20', '5.20.2', '充值反馈', '2019-07-31 07:49:33');
INSERT INTO `base_data` VALUES ('23', '其他问题', '20', '5.20.3', '其他问题', '2019-07-31 07:49:47');
INSERT INTO `base_data` VALUES ('24', '第三方广告', '1', '1.1.2', '第三方广告', '2019-08-20 21:57:28');
INSERT INTO `base_data` VALUES ('25', '小说阅读-底部', '24', '1.1.2.2', '小说阅读-底部-单图模式-3家权重轮训', '2019-08-21 11:42:25');
INSERT INTO `base_data` VALUES ('26', '小说阅读-底部-文字链', '24', '1.1.2.3', '小说阅读-底部-文字链-5条-权重排序', '2019-08-21 11:43:25');

-- ----------------------------
-- Table structure for base_report
-- ----------------------------
DROP TABLE IF EXISTS `base_report`;
CREATE TABLE `base_report` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `day` date DEFAULT NULL,
  `views` int(11) DEFAULT NULL,
  `ip_views` int(11) DEFAULT NULL,
  `active_views` int(11) DEFAULT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='基础统计报表';

-- ----------------------------
-- Records of base_report
-- ----------------------------
INSERT INTO `base_report` VALUES ('1', '2019-09-17', '180', '1', '1', '2019-09-18 11:01:00');

-- ----------------------------
-- Table structure for block
-- ----------------------------
DROP TABLE IF EXISTS `block`;
CREATE TABLE `block` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `block_name` varchar(20) DEFAULT NULL COMMENT '名称',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `type_id` varchar(20) DEFAULT '' COMMENT '类型 对应base_data中',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COMMENT='版块';

-- ----------------------------
-- Records of block
-- ----------------------------
INSERT INTO `block` VALUES ('1', '精品汇聚', '2019-07-13 08:00:16', '2.6.1');
INSERT INTO `block` VALUES ('2', '最好看的书', '2019-07-13 08:00:25', '2.6.1');
INSERT INTO `block` VALUES ('3', '独家首发', '2019-07-13 08:00:42', '2.6.1');
INSERT INTO `block` VALUES ('4', '口碑精选', '2019-07-13 08:00:50', '2.6.1');
INSERT INTO `block` VALUES ('5', '大家都在搜', '2019-07-14 15:58:10', '2.6.1');
INSERT INTO `block` VALUES ('6', '精品荟萃', '2019-07-24 07:09:53', '2.6.2');
INSERT INTO `block` VALUES ('7', '限时免费', '2019-07-24 07:10:39', '2.6.2');
INSERT INTO `block` VALUES ('8', '新书精选', '2019-07-24 07:11:57', '2.6.2');
INSERT INTO `block` VALUES ('9', '男生必读', '2019-07-24 07:12:10', '2.6.2');
INSERT INTO `block` VALUES ('10', '大家都在搜', '2019-07-24 07:12:29', '2.6.2');
INSERT INTO `block` VALUES ('11', '本期强推', '2019-07-24 07:13:03', '2.6.3');
INSERT INTO `block` VALUES ('12', '新书抢鲜', '2019-07-24 07:13:25', '2.6.3');
INSERT INTO `block` VALUES ('13', '人气完本', '2019-07-24 07:13:31', '2.6.3');
INSERT INTO `block` VALUES ('14', '女生必读', '2019-07-24 07:13:46', '2.6.3');
INSERT INTO `block` VALUES ('15', '大家都在搜', '2019-07-24 07:14:02', '2.6.3');

-- ----------------------------
-- Table structure for block_join_books
-- ----------------------------
DROP TABLE IF EXISTS `block_join_books`;
CREATE TABLE `block_join_books` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `block_id` int(11) DEFAULT NULL COMMENT '版块ID',
  `book_id` bigint(11) DEFAULT NULL COMMENT '小说ID',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='版块和小说关联';

-- ----------------------------
-- Records of block_join_books
-- ----------------------------

-- ----------------------------
-- Table structure for books
-- ----------------------------
DROP TABLE IF EXISTS `books`;
CREATE TABLE `books` (
  `id` bigint(11) NOT NULL,
  `books_name` varchar(50) DEFAULT NULL COMMENT '小说名称',
  `author` varchar(50) DEFAULT NULL COMMENT '作者',
  `sort_id` int(11) DEFAULT NULL COMMENT '分类',
  `is_over` varchar(20) DEFAULT '3.11.1' COMMENT 'base_data 中key',
  `books_cover` varchar(200) DEFAULT NULL COMMENT '封面',
  `books_description` text COMMENT '小说备注',
  `reader_sex` varchar(20) DEFAULT NULL COMMENT '阅读人群性别',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='小说';

-- ----------------------------
-- Records of books
-- ----------------------------

-- ----------------------------
-- Table structure for books_shelf
-- ----------------------------
DROP TABLE IF EXISTS `books_shelf`;
CREATE TABLE `books_shelf` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `book_id` bigint(11) DEFAULT NULL COMMENT '小说ID',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='书架表';

-- ----------------------------
-- Records of books_shelf
-- ----------------------------

-- ----------------------------
-- Table structure for cab_jobs
-- ----------------------------
DROP TABLE IF EXISTS `cab_jobs`;
CREATE TABLE `cab_jobs` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `job_clazz` varchar(300) DEFAULT NULL COMMENT '任务类名',
  `job_name` varchar(100) DEFAULT NULL COMMENT '任务名称',
  `job_desc` varchar(200) DEFAULT NULL COMMENT '描述',
  `extra` text COMMENT '额外参数',
  `cron` varchar(50) DEFAULT NULL COMMENT '定时表达式',
  `is_enable` tinyint(4) DEFAULT '0' COMMENT '是否开始执行',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1576656300051 DEFAULT CHARSET=utf8mb4 COMMENT='定时任务列表';

-- ----------------------------
-- Records of cab_jobs
-- ----------------------------
INSERT INTO `cab_jobs` VALUES ('1576555712572', 'com.sanq.product.books.jobs.BiQuGeLaJobs', '笔趣阁.la', '笔趣阁.la内容获取', '{\n	\"url\": \"http://www.xbiquge.la/\",\n	\"list\": \"#main a\",\n        \"name\": \"#info h1\",\n	\"cover\": \"#fmimg img\",\n	\"author\": \"#info p:eq(1)\",\n	\"description\": \"#intro p:eq(1)\",\n	\"chapter\": \"#list a\",\n	\"content\": \"#content\",\n        \"userAgent\": \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36\"\n}', '0/5 * * * * ? ', '0', '2019-12-17 04:08:31');
INSERT INTO `cab_jobs` VALUES ('1576656300050', 'com.sanq.product.books.jobs.JxLaJobs', 'qu.la', 'qu.la内容获取', '{\n    \"url\": \"https://www.jx.la/\",\n    \"list\": \"#newscontent li\",\n    \"sortName\": \".s1\",\n    \"booksUrl\": \".s2 a\",\n    \"name\": \"#info h1\",\n    \"cover\": \"#fmimg img\",\n    \"author\": \"#info p:eq(1)\",\n    \"description\": \"#intro\",\n    \"chapter\": \"#list a\",\n    \"content\": \"#content\",\n    \"userAgent\": \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36\"\n}', '0/5 * * * * ?', '0', '2019-12-18 08:04:59');

-- ----------------------------
-- Table structure for chapter
-- ----------------------------
DROP TABLE IF EXISTS `chapter`;
CREATE TABLE `chapter` (
  `id` bigint(11) NOT NULL,
  `book_id` bigint(11) DEFAULT NULL COMMENT '小说ID',
  `chapter_name` varchar(100) DEFAULT NULL COMMENT '章节名称',
  `cost_gold` decimal(10,0) DEFAULT '0' COMMENT '花费金币 0为免费',
  `last_chapter_id` bigint(11) DEFAULT NULL COMMENT '上一章',
  `next_chapter_id` bigint(11) DEFAULT NULL COMMENT '下一章',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='章节表';

-- ----------------------------
-- Records of chapter
-- ----------------------------

-- ----------------------------
-- Table structure for chapter_content
-- ----------------------------
DROP TABLE IF EXISTS `chapter_content`;
CREATE TABLE `chapter_content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `book_id` bigint(11) DEFAULT NULL COMMENT '小说ID',
  `chapter_id` bigint(11) DEFAULT NULL COMMENT '章节ID',
  `content` longtext COMMENT '章节内容',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='章节内容';

-- ----------------------------
-- Records of chapter_content
-- ----------------------------

-- ----------------------------
-- Table structure for city
-- ----------------------------
DROP TABLE IF EXISTS `city`;
CREATE TABLE `city` (
  `city_id` int(11) NOT NULL,
  `prov_id` int(11) DEFAULT NULL,
  `city_name` varchar(50) DEFAULT NULL,
  `city_abbr` varchar(50) DEFAULT NULL,
  `is_delete` varchar(5) DEFAULT NULL,
  `create_time` date DEFAULT NULL,
  PRIMARY KEY (`city_id`),
  KEY `city_name` (`city_name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of city
-- ----------------------------
INSERT INTO `city` VALUES ('110100', '110000', '北京市', '', 'N', null);
INSERT INTO `city` VALUES ('120100', '120000', '天津市', '', 'N', null);
INSERT INTO `city` VALUES ('130100', '130000', '石家庄市', '', 'N', null);
INSERT INTO `city` VALUES ('130200', '130000', '唐山市', '', 'N', null);
INSERT INTO `city` VALUES ('130300', '130000', '秦皇岛市', '', 'N', null);
INSERT INTO `city` VALUES ('130400', '130000', '邯郸市', '', 'N', null);
INSERT INTO `city` VALUES ('130500', '130000', '邢台市', '', 'N', null);
INSERT INTO `city` VALUES ('130600', '130000', '保定市', '', 'N', null);
INSERT INTO `city` VALUES ('130700', '130000', '张家口市', '', 'N', null);
INSERT INTO `city` VALUES ('130800', '130000', '承德市', '', 'N', null);
INSERT INTO `city` VALUES ('130900', '130000', '沧州市', '', 'N', null);
INSERT INTO `city` VALUES ('131000', '130000', '廊坊市', '', 'N', null);
INSERT INTO `city` VALUES ('131100', '130000', '衡水市', '', 'N', null);
INSERT INTO `city` VALUES ('140100', '140000', '太原市', '', 'N', null);
INSERT INTO `city` VALUES ('140200', '140000', '大同市', '', 'N', null);
INSERT INTO `city` VALUES ('140300', '140000', '阳泉市', '', 'N', null);
INSERT INTO `city` VALUES ('140400', '140000', '长治市', '', 'N', null);
INSERT INTO `city` VALUES ('140500', '140000', '晋城市', '', 'N', null);
INSERT INTO `city` VALUES ('140600', '140000', '朔州市', '', 'N', null);
INSERT INTO `city` VALUES ('140700', '140000', '晋中市', '', 'N', null);
INSERT INTO `city` VALUES ('140800', '140000', '运城市', '', 'N', null);
INSERT INTO `city` VALUES ('140900', '140000', '忻州市', '', 'N', null);
INSERT INTO `city` VALUES ('141000', '140000', '临汾市', '', 'N', null);
INSERT INTO `city` VALUES ('141100', '140000', '吕梁市', '', 'N', null);
INSERT INTO `city` VALUES ('150100', '150000', '呼和浩特市', '', 'N', null);
INSERT INTO `city` VALUES ('150200', '150000', '包头市', '', 'N', null);
INSERT INTO `city` VALUES ('150300', '150000', '乌海市', '', 'N', null);
INSERT INTO `city` VALUES ('150400', '150000', '赤峰市', '', 'N', null);
INSERT INTO `city` VALUES ('150500', '150000', '通辽市', '', 'N', null);
INSERT INTO `city` VALUES ('150600', '150000', '鄂尔多斯市', '', 'N', null);
INSERT INTO `city` VALUES ('150700', '150000', '呼伦贝尔市', '', 'N', null);
INSERT INTO `city` VALUES ('150800', '150000', '巴彦淖尔市', '', 'N', null);
INSERT INTO `city` VALUES ('150900', '150000', '乌兰察布市', '', 'N', null);
INSERT INTO `city` VALUES ('152200', '150000', '兴安盟', '', 'N', null);
INSERT INTO `city` VALUES ('152500', '150000', '锡林郭勒盟', '', 'N', null);
INSERT INTO `city` VALUES ('152900', '150000', '阿拉善盟', '', 'N', null);
INSERT INTO `city` VALUES ('210100', '210000', '沈阳市', '', 'N', null);
INSERT INTO `city` VALUES ('210200', '210000', '大连市', '', 'N', null);
INSERT INTO `city` VALUES ('210300', '210000', '鞍山市', '', 'N', null);
INSERT INTO `city` VALUES ('210400', '210000', '抚顺市', '', 'N', null);
INSERT INTO `city` VALUES ('210500', '210000', '本溪市', '', 'N', null);
INSERT INTO `city` VALUES ('210600', '210000', '丹东市', '', 'N', null);
INSERT INTO `city` VALUES ('210700', '210000', '锦州市', '', 'N', null);
INSERT INTO `city` VALUES ('210800', '210000', '营口市', '', 'N', null);
INSERT INTO `city` VALUES ('210900', '210000', '阜新市', '', 'N', null);
INSERT INTO `city` VALUES ('211000', '210000', '辽阳市', '', 'N', null);
INSERT INTO `city` VALUES ('211100', '210000', '盘锦市', '', 'N', null);
INSERT INTO `city` VALUES ('211200', '210000', '铁岭市', '', 'N', null);
INSERT INTO `city` VALUES ('211300', '210000', '朝阳市', '', 'N', null);
INSERT INTO `city` VALUES ('211400', '210000', '葫芦岛市', '', 'N', null);
INSERT INTO `city` VALUES ('220100', '220000', '长春市', '', 'N', null);
INSERT INTO `city` VALUES ('220200', '220000', '吉林市', '', 'N', null);
INSERT INTO `city` VALUES ('220300', '220000', '四平市', '', 'N', null);
INSERT INTO `city` VALUES ('220400', '220000', '辽源市', '', 'N', null);
INSERT INTO `city` VALUES ('220500', '220000', '通化市', '', 'N', null);
INSERT INTO `city` VALUES ('220600', '220000', '白山市', '', 'N', null);
INSERT INTO `city` VALUES ('220700', '220000', '松原市', '', 'N', null);
INSERT INTO `city` VALUES ('220800', '220000', '白城市', '', 'N', null);
INSERT INTO `city` VALUES ('222400', '220000', '延边朝鲜族自治州', '', 'N', null);
INSERT INTO `city` VALUES ('230100', '230000', '哈尔滨市', '', 'N', null);
INSERT INTO `city` VALUES ('230200', '230000', '齐齐哈尔市', '', 'N', null);
INSERT INTO `city` VALUES ('230300', '230000', '鸡西市', '', 'N', null);
INSERT INTO `city` VALUES ('230400', '230000', '鹤岗市', '', 'N', null);
INSERT INTO `city` VALUES ('230500', '230000', '双鸭山市', '', 'N', null);
INSERT INTO `city` VALUES ('230600', '230000', '大庆市', '', 'N', null);
INSERT INTO `city` VALUES ('230700', '230000', '伊春市', '', 'N', null);
INSERT INTO `city` VALUES ('230800', '230000', '佳木斯市', '', 'N', null);
INSERT INTO `city` VALUES ('230900', '230000', '七台河市', '', 'N', null);
INSERT INTO `city` VALUES ('231000', '230000', '牡丹江市', '', 'N', null);
INSERT INTO `city` VALUES ('231100', '230000', '黑河市', '', 'N', null);
INSERT INTO `city` VALUES ('231200', '230000', '绥化市', '', 'N', null);
INSERT INTO `city` VALUES ('232700', '230000', '大兴安岭地区', '', 'N', null);
INSERT INTO `city` VALUES ('310100', '310000', '上海市', '', 'N', null);
INSERT INTO `city` VALUES ('320100', '320000', '南京市', '', 'N', null);
INSERT INTO `city` VALUES ('320200', '320000', '无锡市', '', 'N', null);
INSERT INTO `city` VALUES ('320300', '320000', '徐州市', '', 'N', null);
INSERT INTO `city` VALUES ('320400', '320000', '常州市', '', 'N', null);
INSERT INTO `city` VALUES ('320500', '320000', '苏州市', '', 'N', null);
INSERT INTO `city` VALUES ('320600', '320000', '南通市', '', 'N', null);
INSERT INTO `city` VALUES ('320700', '320000', '连云港市', '', 'N', null);
INSERT INTO `city` VALUES ('320800', '320000', '淮安市', '', 'N', null);
INSERT INTO `city` VALUES ('320900', '320000', '盐城市', '', 'N', null);
INSERT INTO `city` VALUES ('321000', '320000', '扬州市', '', 'N', null);
INSERT INTO `city` VALUES ('321100', '320000', '镇江市', '', 'N', null);
INSERT INTO `city` VALUES ('321200', '320000', '泰州市', '', 'N', null);
INSERT INTO `city` VALUES ('321300', '320000', '宿迁市', '', 'N', null);
INSERT INTO `city` VALUES ('330100', '330000', '杭州市', '', 'N', null);
INSERT INTO `city` VALUES ('330200', '330000', '宁波市', '', 'N', null);
INSERT INTO `city` VALUES ('330300', '330000', '温州市', '', 'N', null);
INSERT INTO `city` VALUES ('330400', '330000', '嘉兴市', '', 'N', null);
INSERT INTO `city` VALUES ('330500', '330000', '湖州市', '', 'N', null);
INSERT INTO `city` VALUES ('330600', '330000', '绍兴市', '', 'N', null);
INSERT INTO `city` VALUES ('330700', '330000', '金华市', '', 'N', null);
INSERT INTO `city` VALUES ('330800', '330000', '衢州市', '', 'N', null);
INSERT INTO `city` VALUES ('330900', '330000', '舟山市', '', 'N', null);
INSERT INTO `city` VALUES ('331000', '330000', '台州市', '', 'N', null);
INSERT INTO `city` VALUES ('331100', '330000', '丽水市', '', 'N', null);
INSERT INTO `city` VALUES ('340100', '340000', '合肥市', '', 'N', null);
INSERT INTO `city` VALUES ('340200', '340000', '芜湖市', '', 'N', null);
INSERT INTO `city` VALUES ('340300', '340000', '蚌埠市', '', 'N', null);
INSERT INTO `city` VALUES ('340400', '340000', '淮南市', '', 'N', null);
INSERT INTO `city` VALUES ('340500', '340000', '马鞍山市', '', 'N', null);
INSERT INTO `city` VALUES ('340600', '340000', '淮北市', '', 'N', null);
INSERT INTO `city` VALUES ('340700', '340000', '铜陵市', '', 'N', null);
INSERT INTO `city` VALUES ('340800', '340000', '安庆市', '', 'N', null);
INSERT INTO `city` VALUES ('341000', '340000', '黄山市', '', 'N', null);
INSERT INTO `city` VALUES ('341100', '340000', '滁州市', '', 'N', null);
INSERT INTO `city` VALUES ('341200', '340000', '阜阳市', '', 'N', null);
INSERT INTO `city` VALUES ('341300', '340000', '宿州市', '', 'N', null);
INSERT INTO `city` VALUES ('341500', '340000', '六安市', '', 'N', null);
INSERT INTO `city` VALUES ('341600', '340000', '亳州市', '', 'N', null);
INSERT INTO `city` VALUES ('341700', '340000', '池州市', '', 'N', null);
INSERT INTO `city` VALUES ('341800', '340000', '宣城市', '', 'N', null);
INSERT INTO `city` VALUES ('350100', '350000', '福州市', '', 'N', null);
INSERT INTO `city` VALUES ('350200', '350000', '厦门市', '', 'N', null);
INSERT INTO `city` VALUES ('350300', '350000', '莆田市', '', 'N', null);
INSERT INTO `city` VALUES ('350400', '350000', '三明市', '', 'N', null);
INSERT INTO `city` VALUES ('350500', '350000', '泉州市', '', 'N', null);
INSERT INTO `city` VALUES ('350600', '350000', '漳州市', '', 'N', null);
INSERT INTO `city` VALUES ('350700', '350000', '南平市', '', 'N', null);
INSERT INTO `city` VALUES ('350800', '350000', '龙岩市', '', 'N', null);
INSERT INTO `city` VALUES ('350900', '350000', '宁德市', '', 'N', null);
INSERT INTO `city` VALUES ('360100', '360000', '南昌市', '', 'N', null);
INSERT INTO `city` VALUES ('360200', '360000', '景德镇市', '', 'N', null);
INSERT INTO `city` VALUES ('360300', '360000', '萍乡市', '', 'N', null);
INSERT INTO `city` VALUES ('360400', '360000', '九江市', '', 'N', null);
INSERT INTO `city` VALUES ('360500', '360000', '新余市', '', 'N', null);
INSERT INTO `city` VALUES ('360600', '360000', '鹰潭市', '', 'N', null);
INSERT INTO `city` VALUES ('360700', '360000', '赣州市', '', 'N', null);
INSERT INTO `city` VALUES ('360800', '360000', '吉安市', '', 'N', null);
INSERT INTO `city` VALUES ('360900', '360000', '宜春市', '', 'N', null);
INSERT INTO `city` VALUES ('361000', '360000', '抚州市', '', 'N', null);
INSERT INTO `city` VALUES ('361100', '360000', '上饶市', '', 'N', null);
INSERT INTO `city` VALUES ('370100', '370000', '济南市', '', 'N', null);
INSERT INTO `city` VALUES ('370200', '370000', '青岛市', '', 'N', null);
INSERT INTO `city` VALUES ('370300', '370000', '淄博市', '', 'N', null);
INSERT INTO `city` VALUES ('370400', '370000', '枣庄市', '', 'N', null);
INSERT INTO `city` VALUES ('370500', '370000', '东营市', '', 'N', null);
INSERT INTO `city` VALUES ('370600', '370000', '烟台市', '', 'N', null);
INSERT INTO `city` VALUES ('370700', '370000', '潍坊市', '', 'N', null);
INSERT INTO `city` VALUES ('370800', '370000', '济宁市', '', 'N', null);
INSERT INTO `city` VALUES ('370900', '370000', '泰安市', '', 'N', null);
INSERT INTO `city` VALUES ('371000', '370000', '威海市', '', 'N', null);
INSERT INTO `city` VALUES ('371100', '370000', '日照市', '', 'N', null);
INSERT INTO `city` VALUES ('371200', '370000', '莱芜市', '', 'N', null);
INSERT INTO `city` VALUES ('371300', '370000', '临沂市', '', 'N', null);
INSERT INTO `city` VALUES ('371400', '370000', '德州市', '', 'N', null);
INSERT INTO `city` VALUES ('371500', '370000', '聊城市', '', 'N', null);
INSERT INTO `city` VALUES ('371600', '370000', '滨州市', '', 'N', null);
INSERT INTO `city` VALUES ('371700', '370000', '菏泽市', '', 'N', null);
INSERT INTO `city` VALUES ('410100', '410000', '郑州市', '', 'N', null);
INSERT INTO `city` VALUES ('410200', '410000', '开封市', '', 'N', null);
INSERT INTO `city` VALUES ('410300', '410000', '洛阳市', '', 'N', null);
INSERT INTO `city` VALUES ('410400', '410000', '平顶山市', '', 'N', null);
INSERT INTO `city` VALUES ('410500', '410000', '安阳市', '', 'N', null);
INSERT INTO `city` VALUES ('410600', '410000', '鹤壁市', '', 'N', null);
INSERT INTO `city` VALUES ('410700', '410000', '新乡市', '', 'N', null);
INSERT INTO `city` VALUES ('410800', '410000', '焦作市', '', 'N', null);
INSERT INTO `city` VALUES ('410881', '410000', '济源市', '', 'N', null);
INSERT INTO `city` VALUES ('410900', '410000', '濮阳市', '', 'N', null);
INSERT INTO `city` VALUES ('411000', '410000', '许昌市', '', 'N', null);
INSERT INTO `city` VALUES ('411100', '410000', '漯河市', '', 'N', null);
INSERT INTO `city` VALUES ('411200', '410000', '三门峡市', '', 'N', null);
INSERT INTO `city` VALUES ('411300', '410000', '南阳市', '', 'N', null);
INSERT INTO `city` VALUES ('411400', '410000', '商丘市', '', 'N', null);
INSERT INTO `city` VALUES ('411500', '410000', '信阳市', '', 'N', null);
INSERT INTO `city` VALUES ('411600', '410000', '周口市', '', 'N', null);
INSERT INTO `city` VALUES ('411700', '410000', '驻马店市', '', 'N', null);
INSERT INTO `city` VALUES ('420100', '420000', '武汉市', '', 'N', null);
INSERT INTO `city` VALUES ('420200', '420000', '黄石市', '', 'N', null);
INSERT INTO `city` VALUES ('420300', '420000', '十堰市', '', 'N', null);
INSERT INTO `city` VALUES ('420500', '420000', '宜昌市', '', 'N', null);
INSERT INTO `city` VALUES ('420600', '420000', '襄阳市', '', 'N', null);
INSERT INTO `city` VALUES ('420700', '420000', '鄂州市', '', 'N', null);
INSERT INTO `city` VALUES ('420800', '420000', '荆门市', '', 'N', null);
INSERT INTO `city` VALUES ('420900', '420000', '孝感市', '', 'N', null);
INSERT INTO `city` VALUES ('421000', '420000', '荆州市', '', 'N', null);
INSERT INTO `city` VALUES ('421100', '420000', '黄冈市', '', 'N', null);
INSERT INTO `city` VALUES ('421200', '420000', '咸宁市', '', 'N', null);
INSERT INTO `city` VALUES ('421300', '420000', '随州市', '', 'N', null);
INSERT INTO `city` VALUES ('422800', '420000', '恩施土家族苗族自治州', '', 'N', null);
INSERT INTO `city` VALUES ('429004', '420000', '仙桃市', '', 'N', null);
INSERT INTO `city` VALUES ('429005', '420000', '潜江市', '', 'N', null);
INSERT INTO `city` VALUES ('429006', '420000', '天门市', '', 'N', null);
INSERT INTO `city` VALUES ('429021', '420000', '神农架林区', '', 'N', null);
INSERT INTO `city` VALUES ('430100', '430000', '长沙市', '', 'N', null);
INSERT INTO `city` VALUES ('430200', '430000', '株洲市', '', 'N', null);
INSERT INTO `city` VALUES ('430300', '430000', '湘潭市', '', 'N', null);
INSERT INTO `city` VALUES ('430400', '430000', '衡阳市', '', 'N', null);
INSERT INTO `city` VALUES ('430500', '430000', '邵阳市', '', 'N', null);
INSERT INTO `city` VALUES ('430600', '430000', '岳阳市', '', 'N', null);
INSERT INTO `city` VALUES ('430700', '430000', '常德市', '', 'N', null);
INSERT INTO `city` VALUES ('430800', '430000', '张家界市', '', 'N', null);
INSERT INTO `city` VALUES ('430900', '430000', '益阳市', '', 'N', null);
INSERT INTO `city` VALUES ('431000', '430000', '郴州市', '', 'N', null);
INSERT INTO `city` VALUES ('431100', '430000', '永州市', '', 'N', null);
INSERT INTO `city` VALUES ('431200', '430000', '怀化市', '', 'N', null);
INSERT INTO `city` VALUES ('431300', '430000', '娄底市', '', 'N', null);
INSERT INTO `city` VALUES ('433100', '430000', '湘西土家族苗族自治州', '', 'N', null);
INSERT INTO `city` VALUES ('440100', '440000', '广州市', '', 'N', null);
INSERT INTO `city` VALUES ('440200', '440000', '韶关市', '', 'N', null);
INSERT INTO `city` VALUES ('440300', '440000', '深圳市', '', 'N', null);
INSERT INTO `city` VALUES ('440400', '440000', '珠海市', '', 'N', null);
INSERT INTO `city` VALUES ('440500', '440000', '汕头市', '', 'N', null);
INSERT INTO `city` VALUES ('440600', '440000', '佛山市', '', 'N', null);
INSERT INTO `city` VALUES ('440700', '440000', '江门市', '', 'N', null);
INSERT INTO `city` VALUES ('440800', '440000', '湛江市', '', 'N', null);
INSERT INTO `city` VALUES ('440900', '440000', '茂名市', '', 'N', null);
INSERT INTO `city` VALUES ('441200', '440000', '肇庆市', '', 'N', null);
INSERT INTO `city` VALUES ('441300', '440000', '惠州市', '', 'N', null);
INSERT INTO `city` VALUES ('441400', '440000', '梅州市', '', 'N', null);
INSERT INTO `city` VALUES ('441500', '440000', '汕尾市', '', 'N', null);
INSERT INTO `city` VALUES ('441600', '440000', '河源市', '', 'N', null);
INSERT INTO `city` VALUES ('441700', '440000', '阳江市', '', 'N', null);
INSERT INTO `city` VALUES ('441800', '440000', '清远市', '', 'N', null);
INSERT INTO `city` VALUES ('441900', '440000', '东莞市', '', 'N', null);
INSERT INTO `city` VALUES ('442000', '440000', '中山市', '', 'N', null);
INSERT INTO `city` VALUES ('442101', '440000', '东沙群岛', '', 'N', null);
INSERT INTO `city` VALUES ('445100', '440000', '潮州市', '', 'N', null);
INSERT INTO `city` VALUES ('445200', '440000', '揭阳市', '', 'N', null);
INSERT INTO `city` VALUES ('445300', '440000', '云浮市', '', 'N', null);
INSERT INTO `city` VALUES ('450100', '450000', '南宁市', '', 'N', null);
INSERT INTO `city` VALUES ('450200', '450000', '柳州市', '', 'N', null);
INSERT INTO `city` VALUES ('450300', '450000', '桂林市', '', 'N', null);
INSERT INTO `city` VALUES ('450400', '450000', '梧州市', '', 'N', null);
INSERT INTO `city` VALUES ('450500', '450000', '北海市', '', 'N', null);
INSERT INTO `city` VALUES ('450600', '450000', '防城港市', '', 'N', null);
INSERT INTO `city` VALUES ('450700', '450000', '钦州市', '', 'N', null);
INSERT INTO `city` VALUES ('450800', '450000', '贵港市', '', 'N', null);
INSERT INTO `city` VALUES ('450900', '450000', '玉林市', '', 'N', null);
INSERT INTO `city` VALUES ('451000', '450000', '百色市', '', 'N', null);
INSERT INTO `city` VALUES ('451100', '450000', '贺州市', '', 'N', null);
INSERT INTO `city` VALUES ('451200', '450000', '河池市', '', 'N', null);
INSERT INTO `city` VALUES ('451300', '450000', '来宾市', '', 'N', null);
INSERT INTO `city` VALUES ('451400', '450000', '崇左市', '', 'N', null);
INSERT INTO `city` VALUES ('460100', '460000', '海口市', '', 'N', null);
INSERT INTO `city` VALUES ('460200', '460000', '三亚市', '', 'N', null);
INSERT INTO `city` VALUES ('460300', '460000', '三沙市', '', 'N', null);
INSERT INTO `city` VALUES ('469001', '460000', '五指山市', '', 'N', null);
INSERT INTO `city` VALUES ('469002', '460000', '琼海市', '', 'N', null);
INSERT INTO `city` VALUES ('469003', '460000', '儋州市', '', 'N', null);
INSERT INTO `city` VALUES ('469005', '460000', '文昌市', '', 'N', null);
INSERT INTO `city` VALUES ('469006', '460000', '万宁市', '', 'N', null);
INSERT INTO `city` VALUES ('469007', '460000', '东方市', '', 'N', null);
INSERT INTO `city` VALUES ('469025', '460000', '定安县', '', 'N', null);
INSERT INTO `city` VALUES ('469026', '460000', '屯昌县', '', 'N', null);
INSERT INTO `city` VALUES ('469027', '460000', '澄迈县', '', 'N', null);
INSERT INTO `city` VALUES ('469028', '460000', '临高县', '', 'N', null);
INSERT INTO `city` VALUES ('469030', '460000', '白沙黎族自治县', '', 'N', null);
INSERT INTO `city` VALUES ('469031', '460000', '昌江黎族自治县', '', 'N', null);
INSERT INTO `city` VALUES ('469033', '460000', '乐东黎族自治县', '', 'N', null);
INSERT INTO `city` VALUES ('469034', '460000', '陵水黎族自治县', '', 'N', null);
INSERT INTO `city` VALUES ('469035', '460000', '保亭黎族苗族自治县', '', 'N', null);
INSERT INTO `city` VALUES ('469036', '460000', '琼中黎族苗族自治县', '', 'N', null);
INSERT INTO `city` VALUES ('500100', '500000', '重庆市', '', 'N', null);
INSERT INTO `city` VALUES ('510100', '510000', '成都市', '', 'N', null);
INSERT INTO `city` VALUES ('510300', '510000', '自贡市', '', 'N', null);
INSERT INTO `city` VALUES ('510400', '510000', '攀枝花市', '', 'N', null);
INSERT INTO `city` VALUES ('510500', '510000', '泸州市', '', 'N', null);
INSERT INTO `city` VALUES ('510600', '510000', '德阳市', '', 'N', null);
INSERT INTO `city` VALUES ('510700', '510000', '绵阳市', '', 'N', null);
INSERT INTO `city` VALUES ('510800', '510000', '广元市', '', 'N', null);
INSERT INTO `city` VALUES ('510900', '510000', '遂宁市', '', 'N', null);
INSERT INTO `city` VALUES ('511000', '510000', '内江市', '', 'N', null);
INSERT INTO `city` VALUES ('511100', '510000', '乐山市', '', 'N', null);
INSERT INTO `city` VALUES ('511300', '510000', '南充市', '', 'N', null);
INSERT INTO `city` VALUES ('511400', '510000', '眉山市', '', 'N', null);
INSERT INTO `city` VALUES ('511500', '510000', '宜宾市', '', 'N', null);
INSERT INTO `city` VALUES ('511600', '510000', '广安市', '', 'N', null);
INSERT INTO `city` VALUES ('511700', '510000', '达州市', '', 'N', null);
INSERT INTO `city` VALUES ('511800', '510000', '雅安市', '', 'N', null);
INSERT INTO `city` VALUES ('511900', '510000', '巴中市', '', 'N', null);
INSERT INTO `city` VALUES ('512000', '510000', '资阳市', '', 'N', null);
INSERT INTO `city` VALUES ('513200', '510000', '阿坝藏族羌族自治州', '', 'N', null);
INSERT INTO `city` VALUES ('513300', '510000', '甘孜藏族自治州', '', 'N', null);
INSERT INTO `city` VALUES ('513400', '510000', '凉山彝族自治州', '', 'N', null);
INSERT INTO `city` VALUES ('520100', '520000', '贵阳市', '', 'N', null);
INSERT INTO `city` VALUES ('520200', '520000', '六盘水市', '', 'N', null);
INSERT INTO `city` VALUES ('520300', '520000', '遵义市', '', 'N', null);
INSERT INTO `city` VALUES ('520400', '520000', '安顺市', '', 'N', null);
INSERT INTO `city` VALUES ('522200', '520000', '铜仁市', '', 'N', null);
INSERT INTO `city` VALUES ('522300', '520000', '黔西南布依族苗族自治州', '', 'N', null);
INSERT INTO `city` VALUES ('522400', '520000', '毕节市', '', 'N', null);
INSERT INTO `city` VALUES ('522600', '520000', '黔东南苗族侗族自治州', '', 'N', null);
INSERT INTO `city` VALUES ('522700', '520000', '黔南布依族苗族自治州', '', 'N', null);
INSERT INTO `city` VALUES ('530100', '530000', '昆明市', '', 'N', null);
INSERT INTO `city` VALUES ('530300', '530000', '曲靖市', '', 'N', null);
INSERT INTO `city` VALUES ('530400', '530000', '玉溪市', '', 'N', null);
INSERT INTO `city` VALUES ('530500', '530000', '保山市', '', 'N', null);
INSERT INTO `city` VALUES ('530600', '530000', '昭通市', '', 'N', null);
INSERT INTO `city` VALUES ('530700', '530000', '丽江市', '', 'N', null);
INSERT INTO `city` VALUES ('530800', '530000', '普洱市', '', 'N', null);
INSERT INTO `city` VALUES ('530900', '530000', '临沧市', '', 'N', null);
INSERT INTO `city` VALUES ('532300', '530000', '楚雄彝族自治州', '', 'N', null);
INSERT INTO `city` VALUES ('532500', '530000', '红河哈尼族彝族自治州', '', 'N', null);
INSERT INTO `city` VALUES ('532600', '530000', '文山壮族苗族自治州', '', 'N', null);
INSERT INTO `city` VALUES ('532800', '530000', '西双版纳傣族自治州', '', 'N', null);
INSERT INTO `city` VALUES ('532900', '530000', '大理白族自治州', '', 'N', null);
INSERT INTO `city` VALUES ('533100', '530000', '德宏傣族景颇族自治州', '', 'N', null);
INSERT INTO `city` VALUES ('533300', '530000', '怒江傈僳族自治州', '', 'N', null);
INSERT INTO `city` VALUES ('533400', '530000', '迪庆藏族自治州', '', 'N', null);
INSERT INTO `city` VALUES ('540100', '540000', '拉萨市', '', 'N', null);
INSERT INTO `city` VALUES ('542100', '540000', '昌都地区', '', 'N', null);
INSERT INTO `city` VALUES ('542200', '540000', '山南地区', '', 'N', null);
INSERT INTO `city` VALUES ('542300', '540000', '日喀则地区', '', 'N', null);
INSERT INTO `city` VALUES ('542400', '540000', '那曲地区', '', 'N', null);
INSERT INTO `city` VALUES ('542500', '540000', '阿里地区', '', 'N', null);
INSERT INTO `city` VALUES ('542600', '540000', '林芝地区', '', 'N', null);
INSERT INTO `city` VALUES ('610100', '610000', '西安市', '', 'N', null);
INSERT INTO `city` VALUES ('610200', '610000', '铜川市', '', 'N', null);
INSERT INTO `city` VALUES ('610300', '610000', '宝鸡市', '', 'N', null);
INSERT INTO `city` VALUES ('610400', '610000', '咸阳市', '', 'N', null);
INSERT INTO `city` VALUES ('610500', '610000', '渭南市', '', 'N', null);
INSERT INTO `city` VALUES ('610600', '610000', '延安市', '', 'N', null);
INSERT INTO `city` VALUES ('610700', '610000', '汉中市', '', 'N', null);
INSERT INTO `city` VALUES ('610800', '610000', '榆林市', '', 'N', null);
INSERT INTO `city` VALUES ('610900', '610000', '安康市', '', 'N', null);
INSERT INTO `city` VALUES ('611000', '610000', '商洛市', '', 'N', null);
INSERT INTO `city` VALUES ('620100', '620000', '兰州市', '', 'N', null);
INSERT INTO `city` VALUES ('620200', '620000', '嘉峪关市', '', 'N', null);
INSERT INTO `city` VALUES ('620300', '620000', '金昌市', '', 'N', null);
INSERT INTO `city` VALUES ('620400', '620000', '白银市', '', 'N', null);
INSERT INTO `city` VALUES ('620500', '620000', '天水市', '', 'N', null);
INSERT INTO `city` VALUES ('620600', '620000', '武威市', '', 'N', null);
INSERT INTO `city` VALUES ('620700', '620000', '张掖市', '', 'N', null);
INSERT INTO `city` VALUES ('620800', '620000', '平凉市', '', 'N', null);
INSERT INTO `city` VALUES ('620900', '620000', '酒泉市', '', 'N', null);
INSERT INTO `city` VALUES ('621000', '620000', '庆阳市', '', 'N', null);
INSERT INTO `city` VALUES ('621100', '620000', '定西市', '', 'N', null);
INSERT INTO `city` VALUES ('621200', '620000', '陇南市', '', 'N', null);
INSERT INTO `city` VALUES ('622900', '620000', '临夏回族自治州', '', 'N', null);
INSERT INTO `city` VALUES ('623000', '620000', '甘南藏族自治州', '', 'N', null);
INSERT INTO `city` VALUES ('630100', '630000', '西宁市', '', 'N', null);
INSERT INTO `city` VALUES ('632100', '630000', '海东市', '', 'N', null);
INSERT INTO `city` VALUES ('632200', '630000', '海北藏族自治州', '', 'N', null);
INSERT INTO `city` VALUES ('632300', '630000', '黄南藏族自治州', '', 'N', null);
INSERT INTO `city` VALUES ('632500', '630000', '海南藏族自治州', '', 'N', null);
INSERT INTO `city` VALUES ('632600', '630000', '果洛藏族自治州', '', 'N', null);
INSERT INTO `city` VALUES ('632700', '630000', '玉树藏族自治州', '', 'N', null);
INSERT INTO `city` VALUES ('632800', '630000', '海西蒙古族藏族自治州', '', 'N', null);
INSERT INTO `city` VALUES ('640100', '640000', '银川市', '', 'N', null);
INSERT INTO `city` VALUES ('640200', '640000', '石嘴山市', '', 'N', null);
INSERT INTO `city` VALUES ('640300', '640000', '吴忠市', '', 'N', null);
INSERT INTO `city` VALUES ('640400', '640000', '固原市', '', 'N', null);
INSERT INTO `city` VALUES ('640500', '640000', '中卫市', '', 'N', null);
INSERT INTO `city` VALUES ('650100', '650000', '乌鲁木齐市', '', 'N', null);
INSERT INTO `city` VALUES ('650200', '650000', '克拉玛依市', '', 'N', null);
INSERT INTO `city` VALUES ('652100', '650000', '吐鲁番地区', '', 'N', null);
INSERT INTO `city` VALUES ('652200', '650000', '哈密地区', '', 'N', null);
INSERT INTO `city` VALUES ('652300', '650000', '昌吉回族自治州', '', 'N', null);
INSERT INTO `city` VALUES ('652700', '650000', '博尔塔拉蒙古自治州', '', 'N', null);
INSERT INTO `city` VALUES ('652800', '650000', '巴音郭楞蒙古自治州', '', 'N', null);
INSERT INTO `city` VALUES ('652900', '650000', '阿克苏地区', '', 'N', null);
INSERT INTO `city` VALUES ('653000', '650000', '克孜勒苏柯尔克孜自治州', '', 'N', null);
INSERT INTO `city` VALUES ('653100', '650000', '喀什地区', '', 'N', null);
INSERT INTO `city` VALUES ('653200', '650000', '和田地区', '', 'N', null);
INSERT INTO `city` VALUES ('654000', '650000', '伊犁哈萨克自治州', '', 'N', null);
INSERT INTO `city` VALUES ('654200', '650000', '塔城地区', '', 'N', null);
INSERT INTO `city` VALUES ('654300', '650000', '阿勒泰地区', '', 'N', null);
INSERT INTO `city` VALUES ('659001', '650000', '石河子市', '', 'N', null);
INSERT INTO `city` VALUES ('659002', '650000', '阿拉尔市', '', 'N', null);
INSERT INTO `city` VALUES ('659003', '650000', '图木舒克市', '', 'N', null);
INSERT INTO `city` VALUES ('659004', '650000', '五家渠市', '', 'N', null);
INSERT INTO `city` VALUES ('710100', '710000', '台北市', '', 'N', null);
INSERT INTO `city` VALUES ('710200', '710000', '高雄市', '', 'N', null);
INSERT INTO `city` VALUES ('710300', '710000', '台南市', '', 'N', null);
INSERT INTO `city` VALUES ('710400', '710000', '台中市', '', 'N', null);
INSERT INTO `city` VALUES ('710500', '710000', '金门县', '', 'N', null);
INSERT INTO `city` VALUES ('710600', '710000', '南投县', '', 'N', null);
INSERT INTO `city` VALUES ('710700', '710000', '基隆市', '', 'N', null);
INSERT INTO `city` VALUES ('710800', '710000', '新竹市', '', 'N', null);
INSERT INTO `city` VALUES ('710900', '710000', '嘉义市', '', 'N', null);
INSERT INTO `city` VALUES ('711100', '710000', '新北市', '', 'N', null);
INSERT INTO `city` VALUES ('711200', '710000', '宜兰县', '', 'N', null);
INSERT INTO `city` VALUES ('711300', '710000', '新竹县', '', 'N', null);
INSERT INTO `city` VALUES ('711400', '710000', '桃园县', '', 'N', null);
INSERT INTO `city` VALUES ('711500', '710000', '苗栗县', '', 'N', null);
INSERT INTO `city` VALUES ('711700', '710000', '彰化县', '', 'N', null);
INSERT INTO `city` VALUES ('711900', '710000', '嘉义县', '', 'N', null);
INSERT INTO `city` VALUES ('712100', '710000', '云林县', '', 'N', null);
INSERT INTO `city` VALUES ('712400', '710000', '屏东县', '', 'N', null);
INSERT INTO `city` VALUES ('712500', '710000', '台东县', '', 'N', null);
INSERT INTO `city` VALUES ('712600', '710000', '花莲县', '', 'N', null);
INSERT INTO `city` VALUES ('712700', '710000', '澎湖县', '', 'N', null);
INSERT INTO `city` VALUES ('712800', '710000', '连江县', '', 'N', null);
INSERT INTO `city` VALUES ('810100', '810000', '香港岛', '', 'N', null);
INSERT INTO `city` VALUES ('810200', '810000', '九龙', '', 'N', null);
INSERT INTO `city` VALUES ('810300', '810000', '新界', '', 'N', null);
INSERT INTO `city` VALUES ('820100', '820000', '澳门半岛', '', 'N', null);
INSERT INTO `city` VALUES ('820200', '820000', '离岛', '', 'N', null);

-- ----------------------------
-- Table structure for collection
-- ----------------------------
DROP TABLE IF EXISTS `collection`;
CREATE TABLE `collection` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `book_id` bigint(11) DEFAULT NULL COMMENT '小说ID',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='收藏';

-- ----------------------------
-- Records of collection
-- ----------------------------

-- ----------------------------
-- Table structure for discuss
-- ----------------------------
DROP TABLE IF EXISTS `discuss`;
CREATE TABLE `discuss` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `book_id` bigint(11) DEFAULT NULL COMMENT '小说ID',
  `star` int(11) DEFAULT NULL COMMENT '星级',
  `discuss_content` varchar(1000) DEFAULT NULL COMMENT '评论内容',
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='评论';

-- ----------------------------
-- Records of discuss
-- ----------------------------

-- ----------------------------
-- Table structure for global_setting
-- ----------------------------
DROP TABLE IF EXISTS `global_setting`;
CREATE TABLE `global_setting` (
  `id` int(11) unsigned zerofill DEFAULT '00000000000',
  `setting_content` text,
  `setting_type` varchar(10) NOT NULL DEFAULT '' COMMENT 'splide',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`setting_type`),
  KEY `setting_type` (`setting_type`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='全局配置';

-- ----------------------------
-- Records of global_setting
-- ----------------------------
INSERT INTO `global_setting` VALUES ('00000000000', '[{\"name\":\"新笔趣阁\",\"url\":\"http://www.xbiquge.la/xiaoshuodaquan/\"}]', 'splide', '2019-08-25 16:29:53');

-- ----------------------------
-- Table structure for history
-- ----------------------------
DROP TABLE IF EXISTS `history`;
CREATE TABLE `history` (
  `id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL COMMENT '用户ID',
  `book_id` bigint(11) NOT NULL COMMENT '小说ID',
  `read_day` timestamp NULL DEFAULT NULL COMMENT '阅读日期',
  `read_chapter_id` bigint(11) NOT NULL COMMENT '阅读章节ID',
  `read_chapter_count` int(11) DEFAULT NULL COMMENT '已经阅读的章节数',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`,`book_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='阅读历史';

-- ----------------------------
-- Records of history
-- ----------------------------

-- ----------------------------
-- Table structure for member
-- ----------------------------
DROP TABLE IF EXISTS `member`;
CREATE TABLE `member` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login_name` varchar(20) DEFAULT NULL,
  `login_pwd` varchar(35) DEFAULT NULL,
  `real_name` varchar(20) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `tel` varchar(32) DEFAULT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_status` tinyint(4) DEFAULT '0' COMMENT '账号状态：1.正常 0.锁定',
  `user_type` tinyint(4) DEFAULT NULL COMMENT '1. 系统管理员',
  `cover` varchar(200) DEFAULT NULL COMMENT '头像',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='后台登录账号';

-- ----------------------------
-- Records of member
-- ----------------------------
INSERT INTO `member` VALUES ('3', 'admin@sina.com', '0192023a7bbd73250516f069df18b500', 'admin', 'admin@sina.com', '15110148609', '2019-05-23 10:06:21', '1', '1', 'https://bucket-test002.oss-cn-beijing.aliyuncs.com//2019/11/13/2326C5D1D7E14B2A9EE885AE8A4B95B5.jpg');

-- ----------------------------
-- Table structure for member_role
-- ----------------------------
DROP TABLE IF EXISTS `member_role`;
CREATE TABLE `member_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of member_role
-- ----------------------------
INSERT INTO `member_role` VALUES ('12', '3', '2');
INSERT INTO `member_role` VALUES ('13', '3', '3');

-- ----------------------------
-- Table structure for notice
-- ----------------------------
DROP TABLE IF EXISTS `notice`;
CREATE TABLE `notice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `notice_title` varchar(50) DEFAULT NULL COMMENT '消息名称',
  `notice_content` text COMMENT '消息内容',
  `is_read` varchar(10) DEFAULT '4.15.1' COMMENT '4.15.1.未阅读 4.15.2.已阅读',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `read_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `notice_type` varchar(10) DEFAULT '4.15.4' COMMENT '消息类型',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='消息';

-- ----------------------------
-- Records of notice
-- ----------------------------
INSERT INTO `notice` VALUES ('1', '4', '意见反馈回复', '是得到', '4.15.1', '2019-12-18 10:04:26', '2019-12-18 10:04:26', '4.15.3');

-- ----------------------------
-- Table structure for orders
-- ----------------------------
DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT '0' COMMENT '用户id',
  `combo_title` varchar(50) DEFAULT '' COMMENT '订单名称',
  `combo_price` decimal(10,0) DEFAULT '0' COMMENT '价格',
  `order_status` int(11) DEFAULT '0' COMMENT '支付状态 0.未支付 1. 已支付',
  `order_create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '订单生成时间',
  `order_num` varchar(100) DEFAULT NULL COMMENT '订单号',
  `pay_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '订单支付时间',
  `glob_vip_count` decimal(10,0) DEFAULT '0' COMMENT 'vip & glob数量',
  `pay_type` varchar(10) DEFAULT '' COMMENT '支付方式',
  `pay_order_num` varchar(32) DEFAULT '' COMMENT '支付订单',
  `order_type` tinyint(4) DEFAULT '0' COMMENT '1. 金币 2. vip',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of orders
-- ----------------------------
INSERT INTO `orders` VALUES ('6', '4', '50元充值', '50', '1', '2019-08-05 08:10:42', '4320F62A84CF4E98A0DF6799CD94C5AF', '2019-08-05 08:10:42', '500', '支付宝', '', '1');
INSERT INTO `orders` VALUES ('7', '4', '50元充值', '50', '1', '2019-08-05 08:10:51', 'F76EFEECB3484FC8A397FD75AD03C291', '2019-08-05 08:10:51', '500', '支付宝', '', '1');
INSERT INTO `orders` VALUES ('8', '4', '50元充值', '50', '0', '2019-09-01 22:07:24', 'D1F7068201F74B359CBEF7FB74FE8D63', '2019-09-01 22:07:24', '500', '微信', '', '1');
INSERT INTO `orders` VALUES ('9', '4', '50元充值', '50', '0', '2019-09-01 22:07:27', '63D6808898BF4CAEBA93A33AAC99F8A7', '2019-09-01 22:07:27', '500', '微信', '', '1');
INSERT INTO `orders` VALUES ('10', '4', '50元充值', '50', '0', '2019-09-01 22:07:54', '425DD15B7D824408B4A5257A78D1F27E', '2019-09-01 22:07:54', '500', '微信', '', '1');

-- ----------------------------
-- Table structure for os_report
-- ----------------------------
DROP TABLE IF EXISTS `os_report`;
CREATE TABLE `os_report` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `day` date DEFAULT NULL,
  `views` int(11) DEFAULT NULL,
  `ip_views` int(11) DEFAULT NULL,
  `os` varchar(15) DEFAULT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='设备统计报表';

-- ----------------------------
-- Records of os_report
-- ----------------------------
INSERT INTO `os_report` VALUES ('1', '2019-09-17', '179', '1', 'android', '2019-09-18 11:01:01');
INSERT INTO `os_report` VALUES ('2', '2019-09-17', '1', '1', 'windows', '2019-09-18 11:01:01');

-- ----------------------------
-- Table structure for pay_history
-- ----------------------------
DROP TABLE IF EXISTS `pay_history`;
CREATE TABLE `pay_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `book_id` int(11) DEFAULT NULL,
  `chapter_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `pay_price` decimal(10,0) DEFAULT NULL COMMENT '花费金币',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='消费记录';

-- ----------------------------
-- Records of pay_history
-- ----------------------------

-- ----------------------------
-- Table structure for permission
-- ----------------------------
DROP TABLE IF EXISTS `permission`;
CREATE TABLE `permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT '0',
  `permission_type` varchar(6) DEFAULT 'MENU' COMMENT 'DIR, MENU, BUTTON',
  `permission_name` varchar(20) DEFAULT NULL,
  `permission_url` varchar(100) DEFAULT NULL,
  `is_home` tinyint(4) DEFAULT '0' COMMENT '0.否 1. 是',
  `order_num` int(6) DEFAULT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `icon` varchar(50) DEFAULT NULL COMMENT '图标 iconfont',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=112 DEFAULT CHARSET=utf8 COMMENT='资源表';

-- ----------------------------
-- Records of permission
-- ----------------------------
INSERT INTO `permission` VALUES ('1', '0', 'DIR', '系统设置', '1', '0', '1', '2019-05-23 11:16:58', '');
INSERT INTO `permission` VALUES ('3', '1', 'MENU', '用户管理', '/list/member', '0', '11000', '2019-05-23 14:57:45', null);
INSERT INTO `permission` VALUES ('8', '1', 'MENU', '角色管理', '/list/roles', '0', '12000', '2019-05-23 18:19:20', null);
INSERT INTO `permission` VALUES ('10', '1', 'MENU', '资源管理', '/list/permission', '0', '14000', '2019-05-23 18:21:41', null);
INSERT INTO `permission` VALUES ('11', '0', 'DIR', '基础设置', '2', '0', '2', '2019-08-10 07:36:45', '');
INSERT INTO `permission` VALUES ('12', '11', 'MENU', '数据字典', '/list/base/data', '0', '21000', '2019-08-10 08:08:08', '');
INSERT INTO `permission` VALUES ('13', '11', 'MENU', '全局设置', '/global/settting', '0', '22000', '2019-08-10 08:10:34', '');
INSERT INTO `permission` VALUES ('14', '0', 'DIR', '小说管理', '3', '0', '3', '2019-08-10 19:07:49', '');
INSERT INTO `permission` VALUES ('15', '14', 'MENU', '小说列表', '/list/books', '0', '31000', '2019-08-10 19:08:17', '');
INSERT INTO `permission` VALUES ('16', '14', 'MENU', '模块列表', '/list/blocks', '0', '32000', '2019-08-15 07:05:29', '');
INSERT INTO `permission` VALUES ('17', '3', 'BUTTON', '列表-用户管理', '/api/member/list', '0', '11100', '2019-08-15 07:54:01', '');
INSERT INTO `permission` VALUES ('18', '3', 'BUTTON', '全部不分页-用户管理', '/api/member/all', '0', '11200', '2019-08-15 07:55:16', '');
INSERT INTO `permission` VALUES ('19', '3', 'BUTTON', '保存用户-用户管理', '/api/member/save', '0', '11300', '2019-08-15 07:55:42', '');
INSERT INTO `permission` VALUES ('20', '3', 'BUTTON', '删除用户-用户管理', '/api/member/delete', '0', '11400', '2019-08-15 07:56:02', '');
INSERT INTO `permission` VALUES ('21', '3', 'BUTTON', '修改用户-用户管理', '/api/member/update/*', '0', '11500', '2019-08-15 07:56:36', '');
INSERT INTO `permission` VALUES ('22', '3', 'BUTTON', '账户登录-用户管理', '/api/member/login', '0', '11600', '2019-08-15 07:57:18', '');
INSERT INTO `permission` VALUES ('23', '3', 'BUTTON', '通过Token获取用户-用户管理', '/api/member/getInfoByToken', '0', '11700', '2019-08-15 07:57:47', '');
INSERT INTO `permission` VALUES ('24', '3', 'BUTTON', '修改密码-用户管理', '/api/member/updatePwd', '0', '11800', '2019-08-15 07:58:14', '');
INSERT INTO `permission` VALUES ('25', '8', 'BUTTON', '列表-角色管理', '/api/role/list', '0', '12100', '2019-08-15 07:58:47', '');
INSERT INTO `permission` VALUES ('26', '8', 'BUTTON', '全部不分页-角色管理', '/api/role/all', '0', '12200', '2019-08-15 07:59:11', '');
INSERT INTO `permission` VALUES ('27', '8', 'BUTTON', '保存角色-角色管理', '/api/role/save', '0', '12300', '2019-08-15 07:59:31', '');
INSERT INTO `permission` VALUES ('28', '8', 'BUTTON', '删除角色-角色管理', '/api/role/delete', '0', '12400', '2019-08-15 07:59:51', '');
INSERT INTO `permission` VALUES ('29', '8', 'BUTTON', '修改角色-角色管理', '/api/role/update/*', '0', '12500', '2019-08-15 08:00:19', '');
INSERT INTO `permission` VALUES ('30', '8', 'BUTTON', '会员授权-角色管理', '/api/member_role/save', '0', '12600', '2019-08-15 08:00:47', '');
INSERT INTO `permission` VALUES ('31', '8', 'BUTTON', '得到授权角色-角色管理', '/api/member_role/getRoleIdListByMemberId', '0', '12700', '2019-08-15 08:01:10', '');
INSERT INTO `permission` VALUES ('32', '10', 'BUTTON', '列表-资源管理', '/api/permission/list', '0', '14100', '2019-08-15 08:03:04', '');
INSERT INTO `permission` VALUES ('33', '10', 'BUTTON', '全部不分页-资源管理', '/api/permission/all', '0', '14200', '2019-08-15 08:03:30', '');
INSERT INTO `permission` VALUES ('34', '10', 'BUTTON', '得到父节点-资源管理', '/api/permission/getPermissionByParentList', '0', '14300', '2019-08-15 08:03:58', '');
INSERT INTO `permission` VALUES ('35', '10', 'BUTTON', '保存-资源管理', '/api/permission/save', '0', '14400', '2019-08-15 08:04:27', '');
INSERT INTO `permission` VALUES ('36', '10', 'BUTTON', '资源删除-资源管理', '/api/permission/delete', '0', '14500', '2019-08-15 08:04:49', '');
INSERT INTO `permission` VALUES ('37', '10', 'BUTTON', '修改资源-资源管理', '/api/permission/update/*', '0', '14600', '2019-08-15 08:05:30', '');
INSERT INTO `permission` VALUES ('38', '10', 'BUTTON', '通过角色查询资源-资源管理', '/api/role_permission/findPermissionIdByRoleId', '0', '14700', '2019-08-15 08:05:52', '');
INSERT INTO `permission` VALUES ('39', '10', 'BUTTON', '角色授权-资源管理', '/api/role_permission/save', '0', '14800', '2019-08-15 08:06:12', '');
INSERT INTO `permission` VALUES ('40', '12', 'BUTTON', '列表-数据字典', '/api/base_data/list', '0', '21100', '2019-08-15 10:44:08', '');
INSERT INTO `permission` VALUES ('41', '12', 'BUTTON', '全部不分页-数据字典', '/api/base_data/all', '0', '21200', '2019-08-15 10:44:31', '');
INSERT INTO `permission` VALUES ('42', '12', 'BUTTON', '字典保存-数据字典', '/api/base_data/save', '0', '21300', '2019-08-15 10:44:57', '');
INSERT INTO `permission` VALUES ('43', '12', 'BUTTON', '删除-数据字典', '/api/base_data/delete', '0', '21400', '2019-08-15 10:45:21', '');
INSERT INTO `permission` VALUES ('44', '12', 'BUTTON', '修改-数据字典', '/api/base_data/update/*', '0', '21500', '2019-08-15 10:45:42', '');
INSERT INTO `permission` VALUES ('45', '15', 'BUTTON', '列表-小说列表', '/api/books/list', '0', '31100', '2019-08-15 10:46:50', '');
INSERT INTO `permission` VALUES ('46', '15', 'BUTTON', '全部不分页-小说列表', '/api/books/all', '0', '31200', '2019-08-15 10:47:20', '');
INSERT INTO `permission` VALUES ('47', '15', 'BUTTON', '保存-小说列表', '/api/books/save', '0', '31300', '2019-08-15 10:47:43', '');
INSERT INTO `permission` VALUES ('48', '15', 'BUTTON', '删除-小说列表', '/api/books/delete', '0', '31400', '2019-08-15 10:48:06', '');
INSERT INTO `permission` VALUES ('49', '15', 'BUTTON', '修改-小说列表', '/api/books/update/*', '0', '31500', '2019-08-15 10:48:27', '');
INSERT INTO `permission` VALUES ('50', '15', 'BUTTON', '列表-目录管理', '/api/chapter/list', '0', '31010', '2019-08-15 10:49:43', '');
INSERT INTO `permission` VALUES ('51', '15', 'BUTTON', '全部不分页-目录管理', '/api/chapter/all', '0', '31020', '2019-08-15 10:50:18', '');
INSERT INTO `permission` VALUES ('52', '15', 'BUTTON', '保存-目录管理', '/api/chapter/save', '0', '31030', '2019-08-15 10:50:52', '');
INSERT INTO `permission` VALUES ('53', '15', 'BUTTON', '删除-目录管理', '/api/chapter/delete', '0', '31040', '2019-08-15 10:51:16', '');
INSERT INTO `permission` VALUES ('54', '15', 'BUTTON', '修改-目录管理', '/api/chapter/update/*', '0', '31050', '2019-08-15 10:51:38', '');
INSERT INTO `permission` VALUES ('55', '15', 'BUTTON', '查看分类-小说列表', '/api/sort_join_books/getSortIdByBooksId', '0', '31600', '2019-08-15 10:54:26', '');
INSERT INTO `permission` VALUES ('56', '15', 'BUTTON', '保存分类-小说列表', '/api/sort_join_books/save', '0', '31700', '2019-08-15 10:56:40', '');
INSERT INTO `permission` VALUES ('57', '16', 'BUTTON', '列表-模块列表', '/api/block/list', '0', '32100', '2019-08-15 11:19:35', '');
INSERT INTO `permission` VALUES ('58', '16', 'BUTTON', '全部不分页-模块列表', '/api/block/all', '0', '32200', '2019-08-15 11:20:36', '');
INSERT INTO `permission` VALUES ('59', '16', 'BUTTON', '保存-模块列表', '/api/block/save', '0', '32300', '2019-08-15 11:21:04', '');
INSERT INTO `permission` VALUES ('60', '16', 'BUTTON', '删除-模块列表', '/api/block/delete', '0', '32400', '2019-08-15 11:21:29', '');
INSERT INTO `permission` VALUES ('61', '16', 'BUTTON', '修改-模块列表', '/api/block/update/*', '0', '32500', '2019-08-15 11:21:56', '');
INSERT INTO `permission` VALUES ('62', '16', 'BUTTON', '关联小说列表-模块列表', '/api/block_join_books/list', '0', '32010', '2019-08-16 07:27:29', '');
INSERT INTO `permission` VALUES ('63', '16', 'BUTTON', '保存-模块列表', '/api/block_join_books/save', '0', '32020', '2019-08-16 07:27:55', '');
INSERT INTO `permission` VALUES ('64', '16', 'BUTTON', '删除-模块列表', '/api/block_join_books/delete', '0', '32030', '2019-08-16 07:28:15', '');
INSERT INTO `permission` VALUES ('65', '16', 'BUTTON', '随机生成-模块列表', '/api/block_join_books/range', '0', '32040', '2019-08-16 07:28:38', '');
INSERT INTO `permission` VALUES ('66', '14', 'MENU', '小说分类', '/list/sort', '0', '33000', '2019-08-17 16:11:20', '');
INSERT INTO `permission` VALUES ('67', '66', 'BUTTON', '列表-小说分类', '/api/sort/list', '0', '33100', '2019-08-17 16:43:32', '');
INSERT INTO `permission` VALUES ('68', '66', 'BUTTON', '全部不分页-小说分类', '/api/sort/all', '0', '33200', '2019-08-17 16:44:07', '');
INSERT INTO `permission` VALUES ('69', '66', 'BUTTON', '保存-小说分类', '/api/sort/save', '0', '33300', '2019-08-17 16:44:44', '');
INSERT INTO `permission` VALUES ('70', '66', 'BUTTON', '删除-小说分类', '/api/sort/delete', '0', '33400', '2019-08-17 16:45:07', '');
INSERT INTO `permission` VALUES ('71', '66', 'BUTTON', '修改-小说分类', '/api/sort/update/*', '0', '33500', '2019-08-17 16:45:33', '');
INSERT INTO `permission` VALUES ('72', '0', 'DIR', '会员管理', '4', '0', '4', '2019-08-17 16:45:54', '');
INSERT INTO `permission` VALUES ('73', '72', 'MENU', '会员列表', '/list/users', '0', '41000', '2019-08-17 16:46:23', '');
INSERT INTO `permission` VALUES ('74', '73', 'BUTTON', '列表-会员列表', '/api/users/list', '0', '41100', '2019-08-17 17:09:44', '');
INSERT INTO `permission` VALUES ('75', '0', 'DIR', '订单管理', '5', '0', '5', '2019-08-17 23:25:17', '');
INSERT INTO `permission` VALUES ('76', '75', 'MENU', '订单列表', '/list/orders', '0', '51000', '2019-08-17 23:25:42', '');
INSERT INTO `permission` VALUES ('77', '76', 'BUTTON', '列表-订单列表', '/api/orders/list', '0', '51100', '2019-08-20 21:37:51', '');
INSERT INTO `permission` VALUES ('78', '0', 'DIR', '广告管理', '6', '0', '6', '2019-08-20 21:39:28', '');
INSERT INTO `permission` VALUES ('79', '78', 'MENU', '轮播列表', '/list/banner', '0', '61000', '2019-08-20 21:39:46', '');
INSERT INTO `permission` VALUES ('80', '78', 'MENU', '广告列表', '/list/advertises', '0', '62000', '2019-08-20 21:47:24', '');
INSERT INTO `permission` VALUES ('81', '79', 'BUTTON', '列表-轮播列表', '/api/advertise/list', '0', '61100', '2019-08-22 07:50:08', '');
INSERT INTO `permission` VALUES ('82', '79', 'BUTTON', '保存-轮播列表', '/api/advertise/save', '0', '61200', '2019-08-22 07:50:42', '');
INSERT INTO `permission` VALUES ('83', '79', 'BUTTON', '删除-轮播列表', '/api/advertise/delete', '0', '61300', '2019-08-22 07:51:12', '');
INSERT INTO `permission` VALUES ('84', '79', 'BUTTON', '修改-轮播列表', '/api/advertise/update/*', '0', '61400', '2019-08-22 07:51:37', '');
INSERT INTO `permission` VALUES ('85', '80', 'BUTTON', '列表-广告列表', '/api/advertise/list', '0', '62100', '2019-08-22 07:55:44', '');
INSERT INTO `permission` VALUES ('86', '80', 'BUTTON', '保存-广告列表', '/api/advertise/save', '0', '62200', '2019-08-22 07:55:44', '');
INSERT INTO `permission` VALUES ('87', '80', 'BUTTON', '删除-广告列表', '/api/advertise/delete', '0', '62300', '2019-08-22 07:55:44', '');
INSERT INTO `permission` VALUES ('88', '80', 'BUTTON', '修改-广告列表', '/api/advertise/update/*', '0', '62400', '2019-08-22 07:55:44', '');
INSERT INTO `permission` VALUES ('89', '11', 'MENU', '产品管理', '/list/product', '0', '23000', '2019-08-22 09:48:02', '');
INSERT INTO `permission` VALUES ('90', '89', 'BUTTON', '列表-产品管理', '/api/product/list', '0', '23100', '2019-08-22 10:23:12', '');
INSERT INTO `permission` VALUES ('91', '89', 'BUTTON', '保存-产品管理', '/api/product/save', '0', '23200', '2019-08-22 10:23:36', '');
INSERT INTO `permission` VALUES ('92', '89', 'BUTTON', '删除-产品管理', '/api/product/delete', '0', '23300', '2019-08-22 10:23:55', '');
INSERT INTO `permission` VALUES ('93', '89', 'BUTTON', '修改-产品管理', '/api/product/update/*', '0', '23400', '2019-08-22 10:24:18', '');
INSERT INTO `permission` VALUES ('94', '0', 'DIR', '消息中心', '7', '0', '7', '2019-08-22 13:51:48', '');
INSERT INTO `permission` VALUES ('95', '94', 'MENU', '消息列表', '/list/notice', '0', '71000', '2019-08-22 13:52:15', '');
INSERT INTO `permission` VALUES ('96', '94', 'MENU', '意见反馈', '/list/suggestion', '0', '72000', '2019-08-22 13:53:32', '');
INSERT INTO `permission` VALUES ('97', '95', 'BUTTON', '列表-消息列表', '/api/notice/list', '0', '71100', '2019-08-22 14:35:48', '');
INSERT INTO `permission` VALUES ('98', '95', 'BUTTON', '保存-消息列表', '/api/notice/save', '0', '71200', '2019-08-22 14:36:16', '');
INSERT INTO `permission` VALUES ('99', '95', 'BUTTON', '删除-消息列表', '/api/notice/delete', '0', '71300', '2019-08-22 14:36:39', '');
INSERT INTO `permission` VALUES ('100', '95', 'BUTTON', '修改-消息列表', '/api/notice/update/*', '0', '71400', '2019-08-22 14:37:00', '');
INSERT INTO `permission` VALUES ('101', '95', 'BUTTON', '搜索会员-消息列表', '/api/users/all', '0', '71500', '2019-08-22 14:37:33', '');
INSERT INTO `permission` VALUES ('102', '96', 'BUTTON', '列表-意见反馈', '/api/suggestions/list', '0', '72100', '2019-08-22 15:02:23', '');
INSERT INTO `permission` VALUES ('103', '96', 'BUTTON', '修改-意见反馈', '/api/suggestions/update/*', '0', '72200', '2019-08-22 15:02:51', '');
INSERT INTO `permission` VALUES ('104', '96', 'BUTTON', '删除-意见反馈', '/api/suggestions/delete', '0', '72300', '2019-08-22 15:03:23', '');
INSERT INTO `permission` VALUES ('105', '73', 'BUTTON', '修改充值-会员列表', '/api/users/updateObtainById/*', '0', '41200', '2019-08-23 07:26:54', '');
INSERT INTO `permission` VALUES ('106', '0', 'DIR', '系统总览', '0', '0', '0', '2019-08-26 16:53:21', '');
INSERT INTO `permission` VALUES ('107', '106', 'MENU', '总览', '/summary', '1', '1', '2019-08-26 16:54:10', '');
INSERT INTO `permission` VALUES ('108', '107', 'BUTTON', '用户总览-总览', '/api/summary/user', '0', '11000', '2019-08-26 17:10:08', '');
INSERT INTO `permission` VALUES ('109', '107', 'BUTTON', '订单总览-总览', '/api/summary/order', '0', '12000', '2019-08-26 17:10:43', '');
INSERT INTO `permission` VALUES ('110', '0', 'DIR', '爬虫设置', '9', '0', '9', '2019-12-17 03:06:05', '');
INSERT INTO `permission` VALUES ('111', '110', 'MENU', '规则列表', '/list/jobs', '0', '91000', '2019-12-17 03:06:35', '');

-- ----------------------------
-- Table structure for product
-- ----------------------------
DROP TABLE IF EXISTS `product`;
CREATE TABLE `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_name` varchar(35) DEFAULT NULL COMMENT '产品名称',
  `obtain` decimal(10,0) DEFAULT NULL COMMENT '金币/vip天数',
  `extra_obtain` decimal(10,0) DEFAULT NULL COMMENT '额外所得',
  `product_type` tinyint(4) DEFAULT '1' COMMENT '1.金币 2.VIP',
  `price` decimal(10,0) DEFAULT NULL COMMENT '价格',
  `is_hot` tinyint(4) DEFAULT '0' COMMENT '是否热卖',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='充值产品';

-- ----------------------------
-- Records of product
-- ----------------------------
INSERT INTO `product` VALUES ('1', '1元充值', '10', '0', '1', '1', '0', '2019-07-28 11:12:57');
INSERT INTO `product` VALUES ('2', '50元充值', '500', '0', '1', '50', '1', '2019-07-28 11:13:22');
INSERT INTO `product` VALUES ('3', '30元充值', '300', '150', '1', '30', '0', '2019-07-28 11:13:25');
INSERT INTO `product` VALUES ('4', '100元充值', '1000', '1000', '1', '100', '1', '2019-07-28 11:13:45');

-- ----------------------------
-- Table structure for province
-- ----------------------------
DROP TABLE IF EXISTS `province`;
CREATE TABLE `province` (
  `prov_id` int(11) NOT NULL,
  `prov_name` varchar(255) DEFAULT NULL,
  `prov_abbr` varchar(255) DEFAULT NULL,
  `is_delete` varchar(5) DEFAULT NULL,
  `create_time` date DEFAULT NULL,
  PRIMARY KEY (`prov_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of province
-- ----------------------------
INSERT INTO `province` VALUES ('110000', '北京', '京', 'N', null);
INSERT INTO `province` VALUES ('120000', '天津', '津', 'N', null);
INSERT INTO `province` VALUES ('130000', '河北', '冀', 'N', null);
INSERT INTO `province` VALUES ('140000', '山西', '冀', 'N', null);
INSERT INTO `province` VALUES ('150000', '内蒙古自治区', '内蒙古', 'N', null);
INSERT INTO `province` VALUES ('210000', '辽宁', '辽', 'N', null);
INSERT INTO `province` VALUES ('220000', '吉林', '吉', 'N', null);
INSERT INTO `province` VALUES ('230000', '黑龙江', '黑', 'N', null);
INSERT INTO `province` VALUES ('310000', '上海', '沪', 'N', null);
INSERT INTO `province` VALUES ('320000', '江苏', '苏', 'N', null);
INSERT INTO `province` VALUES ('330000', '浙江', '浙', 'N', null);
INSERT INTO `province` VALUES ('340000', '安徽', '皖', 'N', null);
INSERT INTO `province` VALUES ('350000', '福建', '闽', 'N', null);
INSERT INTO `province` VALUES ('360000', '江西', '赣', 'N', null);
INSERT INTO `province` VALUES ('370000', '山东', '鲁', 'N', null);
INSERT INTO `province` VALUES ('410000', '河南', '豫', 'N', null);
INSERT INTO `province` VALUES ('420000', '湖北', '鄂', 'N', null);
INSERT INTO `province` VALUES ('430000', '湖南', '湘', 'N', null);
INSERT INTO `province` VALUES ('440000', '广东', '粤', 'N', null);
INSERT INTO `province` VALUES ('450000', '广西壮族自治区', '桂', 'N', null);
INSERT INTO `province` VALUES ('460000', '海南', '琼', 'N', null);
INSERT INTO `province` VALUES ('500000', '重庆', '渝', 'N', null);
INSERT INTO `province` VALUES ('510000', '四川', '川', 'N', null);
INSERT INTO `province` VALUES ('520000', '贵州', '贵', 'N', null);
INSERT INTO `province` VALUES ('530000', '云南', '云', 'N', null);
INSERT INTO `province` VALUES ('540000', '西藏自治区', '藏', 'N', null);
INSERT INTO `province` VALUES ('610000', '陕西', '陕', 'N', null);
INSERT INTO `province` VALUES ('620000', '甘肃', '甘', 'N', null);
INSERT INTO `province` VALUES ('630000', '青海', '青', 'N', null);
INSERT INTO `province` VALUES ('640000', '宁夏回族自治区', '宁', 'N', null);
INSERT INTO `province` VALUES ('650000', '新疆维吾尔自治区', '新', 'N', null);
INSERT INTO `province` VALUES ('710000', '台湾', '台', 'N', null);
INSERT INTO `province` VALUES ('810000', '香港特别行政区', '港', 'N', null);
INSERT INTO `province` VALUES ('820000', '澳门特别行政区', '澳', 'N', null);

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(20) DEFAULT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='角色';

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO `role` VALUES ('2', '超级管理员', '2019-05-22 18:37:18');

-- ----------------------------
-- Table structure for role_permission
-- ----------------------------
DROP TABLE IF EXISTS `role_permission`;
CREATE TABLE `role_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permission_id` int(11) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2611 DEFAULT CHARSET=utf8 COMMENT='角色关联表';

-- ----------------------------
-- Records of role_permission
-- ----------------------------
INSERT INTO `role_permission` VALUES ('2507', '11', '2');
INSERT INTO `role_permission` VALUES ('2508', '1', '2');
INSERT INTO `role_permission` VALUES ('2509', '3', '2');
INSERT INTO `role_permission` VALUES ('2510', '17', '2');
INSERT INTO `role_permission` VALUES ('2511', '18', '2');
INSERT INTO `role_permission` VALUES ('2512', '19', '2');
INSERT INTO `role_permission` VALUES ('2513', '20', '2');
INSERT INTO `role_permission` VALUES ('2514', '21', '2');
INSERT INTO `role_permission` VALUES ('2515', '22', '2');
INSERT INTO `role_permission` VALUES ('2516', '23', '2');
INSERT INTO `role_permission` VALUES ('2517', '24', '2');
INSERT INTO `role_permission` VALUES ('2518', '8', '2');
INSERT INTO `role_permission` VALUES ('2519', '25', '2');
INSERT INTO `role_permission` VALUES ('2520', '26', '2');
INSERT INTO `role_permission` VALUES ('2521', '27', '2');
INSERT INTO `role_permission` VALUES ('2522', '28', '2');
INSERT INTO `role_permission` VALUES ('2523', '29', '2');
INSERT INTO `role_permission` VALUES ('2524', '30', '2');
INSERT INTO `role_permission` VALUES ('2525', '31', '2');
INSERT INTO `role_permission` VALUES ('2526', '10', '2');
INSERT INTO `role_permission` VALUES ('2527', '32', '2');
INSERT INTO `role_permission` VALUES ('2528', '33', '2');
INSERT INTO `role_permission` VALUES ('2529', '34', '2');
INSERT INTO `role_permission` VALUES ('2530', '35', '2');
INSERT INTO `role_permission` VALUES ('2531', '36', '2');
INSERT INTO `role_permission` VALUES ('2532', '37', '2');
INSERT INTO `role_permission` VALUES ('2533', '38', '2');
INSERT INTO `role_permission` VALUES ('2534', '39', '2');
INSERT INTO `role_permission` VALUES ('2535', '12', '2');
INSERT INTO `role_permission` VALUES ('2536', '40', '2');
INSERT INTO `role_permission` VALUES ('2537', '41', '2');
INSERT INTO `role_permission` VALUES ('2538', '42', '2');
INSERT INTO `role_permission` VALUES ('2539', '43', '2');
INSERT INTO `role_permission` VALUES ('2540', '44', '2');
INSERT INTO `role_permission` VALUES ('2541', '89', '2');
INSERT INTO `role_permission` VALUES ('2542', '90', '2');
INSERT INTO `role_permission` VALUES ('2543', '91', '2');
INSERT INTO `role_permission` VALUES ('2544', '92', '2');
INSERT INTO `role_permission` VALUES ('2545', '93', '2');
INSERT INTO `role_permission` VALUES ('2546', '14', '2');
INSERT INTO `role_permission` VALUES ('2547', '15', '2');
INSERT INTO `role_permission` VALUES ('2548', '45', '2');
INSERT INTO `role_permission` VALUES ('2549', '46', '2');
INSERT INTO `role_permission` VALUES ('2550', '47', '2');
INSERT INTO `role_permission` VALUES ('2551', '48', '2');
INSERT INTO `role_permission` VALUES ('2552', '49', '2');
INSERT INTO `role_permission` VALUES ('2553', '50', '2');
INSERT INTO `role_permission` VALUES ('2554', '51', '2');
INSERT INTO `role_permission` VALUES ('2555', '52', '2');
INSERT INTO `role_permission` VALUES ('2556', '53', '2');
INSERT INTO `role_permission` VALUES ('2557', '54', '2');
INSERT INTO `role_permission` VALUES ('2558', '55', '2');
INSERT INTO `role_permission` VALUES ('2559', '56', '2');
INSERT INTO `role_permission` VALUES ('2560', '16', '2');
INSERT INTO `role_permission` VALUES ('2561', '57', '2');
INSERT INTO `role_permission` VALUES ('2562', '58', '2');
INSERT INTO `role_permission` VALUES ('2563', '59', '2');
INSERT INTO `role_permission` VALUES ('2564', '60', '2');
INSERT INTO `role_permission` VALUES ('2565', '61', '2');
INSERT INTO `role_permission` VALUES ('2566', '62', '2');
INSERT INTO `role_permission` VALUES ('2567', '63', '2');
INSERT INTO `role_permission` VALUES ('2568', '64', '2');
INSERT INTO `role_permission` VALUES ('2569', '65', '2');
INSERT INTO `role_permission` VALUES ('2570', '66', '2');
INSERT INTO `role_permission` VALUES ('2571', '67', '2');
INSERT INTO `role_permission` VALUES ('2572', '68', '2');
INSERT INTO `role_permission` VALUES ('2573', '69', '2');
INSERT INTO `role_permission` VALUES ('2574', '70', '2');
INSERT INTO `role_permission` VALUES ('2575', '71', '2');
INSERT INTO `role_permission` VALUES ('2576', '72', '2');
INSERT INTO `role_permission` VALUES ('2577', '73', '2');
INSERT INTO `role_permission` VALUES ('2578', '74', '2');
INSERT INTO `role_permission` VALUES ('2579', '105', '2');
INSERT INTO `role_permission` VALUES ('2580', '75', '2');
INSERT INTO `role_permission` VALUES ('2581', '76', '2');
INSERT INTO `role_permission` VALUES ('2582', '77', '2');
INSERT INTO `role_permission` VALUES ('2583', '78', '2');
INSERT INTO `role_permission` VALUES ('2584', '79', '2');
INSERT INTO `role_permission` VALUES ('2585', '81', '2');
INSERT INTO `role_permission` VALUES ('2586', '82', '2');
INSERT INTO `role_permission` VALUES ('2587', '83', '2');
INSERT INTO `role_permission` VALUES ('2588', '84', '2');
INSERT INTO `role_permission` VALUES ('2589', '80', '2');
INSERT INTO `role_permission` VALUES ('2590', '85', '2');
INSERT INTO `role_permission` VALUES ('2591', '86', '2');
INSERT INTO `role_permission` VALUES ('2592', '87', '2');
INSERT INTO `role_permission` VALUES ('2593', '88', '2');
INSERT INTO `role_permission` VALUES ('2594', '94', '2');
INSERT INTO `role_permission` VALUES ('2595', '95', '2');
INSERT INTO `role_permission` VALUES ('2596', '97', '2');
INSERT INTO `role_permission` VALUES ('2597', '98', '2');
INSERT INTO `role_permission` VALUES ('2598', '99', '2');
INSERT INTO `role_permission` VALUES ('2599', '100', '2');
INSERT INTO `role_permission` VALUES ('2600', '101', '2');
INSERT INTO `role_permission` VALUES ('2601', '96', '2');
INSERT INTO `role_permission` VALUES ('2602', '102', '2');
INSERT INTO `role_permission` VALUES ('2603', '103', '2');
INSERT INTO `role_permission` VALUES ('2604', '104', '2');
INSERT INTO `role_permission` VALUES ('2605', '106', '2');
INSERT INTO `role_permission` VALUES ('2606', '107', '2');
INSERT INTO `role_permission` VALUES ('2607', '108', '2');
INSERT INTO `role_permission` VALUES ('2608', '109', '2');
INSERT INTO `role_permission` VALUES ('2609', '110', '2');
INSERT INTO `role_permission` VALUES ('2610', '111', '2');

-- ----------------------------
-- Table structure for sign
-- ----------------------------
DROP TABLE IF EXISTS `sign`;
CREATE TABLE `sign` (
  `id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL COMMENT '用户ID',
  `last_sign_time` varchar(10) NOT NULL,
  `sign_count` int(11) DEFAULT '1' COMMENT '连续签到天数',
  PRIMARY KEY (`user_id`,`last_sign_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='签到表';

-- ----------------------------
-- Records of sign
-- ----------------------------
INSERT INTO `sign` VALUES (null, '4', '2019-07', '1');
INSERT INTO `sign` VALUES (null, '4', '2019-08', '0');
INSERT INTO `sign` VALUES (null, '4', '2019-09', '0');

-- ----------------------------
-- Table structure for sign_in
-- ----------------------------
DROP TABLE IF EXISTS `sign_in`;
CREATE TABLE `sign_in` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `sign_day` date DEFAULT NULL COMMENT '签到日期',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COMMENT='签到明细';

-- ----------------------------
-- Records of sign_in
-- ----------------------------
INSERT INTO `sign_in` VALUES ('3', '4', '2019-07-25', '2019-07-25 11:50:25');
INSERT INTO `sign_in` VALUES ('4', '4', '2019-07-26', '2019-07-26 09:43:36');
INSERT INTO `sign_in` VALUES ('5', '4', '2019-07-26', '2019-07-26 09:46:37');
INSERT INTO `sign_in` VALUES ('6', '4', '2019-07-28', '2019-07-28 10:51:49');
INSERT INTO `sign_in` VALUES ('19', '4', '2019-09-01', '2019-09-01 22:06:35');
INSERT INTO `sign_in` VALUES ('20', '4', '2019-09-03', '2019-09-03 07:27:14');
INSERT INTO `sign_in` VALUES ('21', '4', '2019-09-17', '2019-09-17 10:57:57');
INSERT INTO `sign_in` VALUES ('22', '4', '2019-09-17', '2019-09-17 15:39:12');
INSERT INTO `sign_in` VALUES ('23', '4', '2019-09-18', '2019-09-18 10:54:01');

-- ----------------------------
-- Table structure for sort
-- ----------------------------
DROP TABLE IF EXISTS `sort`;
CREATE TABLE `sort` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `block_id` varchar(20) DEFAULT NULL COMMENT '版块ID',
  `parent_id` int(11) DEFAULT '0' COMMENT '父ID',
  `sort_name` varchar(20) DEFAULT NULL COMMENT '分类名称',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8 COMMENT='分类';

-- ----------------------------
-- Records of sort
-- ----------------------------
INSERT INTO `sort` VALUES ('1', '2.6.2', '0', '都市', '2019-07-13 08:29:15');
INSERT INTO `sort` VALUES ('2', '2.6.2', '0', '玄幻', '2019-07-13 08:29:15');
INSERT INTO `sort` VALUES ('3', '2.6.2', '0', '仙侠', '2019-07-13 08:29:15');
INSERT INTO `sort` VALUES ('4', '2.6.2', '0', '灵异', '2019-07-13 08:29:15');
INSERT INTO `sort` VALUES ('5', '2.6.2', '0', '历史', '2019-07-13 08:29:15');
INSERT INTO `sort` VALUES ('6', '2.6.2', '0', '游戏', '2019-07-13 08:29:15');
INSERT INTO `sort` VALUES ('7', '2.6.2', '0', '科幻', '2019-07-13 08:29:15');
INSERT INTO `sort` VALUES ('8', '2.6.2', '0', '武侠', '2019-07-13 08:29:15');
INSERT INTO `sort` VALUES ('9', '2.6.2', '0', '奇幻', '2019-07-13 08:29:15');
INSERT INTO `sort` VALUES ('10', '2.6.2', '0', '竞技', '2019-07-13 08:29:15');
INSERT INTO `sort` VALUES ('11', '2.6.2', '0', '其他', '2019-07-13 08:29:15');
INSERT INTO `sort` VALUES ('12', '2.6.3', '0', '现言', '2019-07-13 08:29:15');
INSERT INTO `sort` VALUES ('13', '2.6.3', '0', '古言', '2019-07-13 08:29:15');
INSERT INTO `sort` VALUES ('14', '2.6.3', '0', '穿越', '2019-07-13 08:29:15');
INSERT INTO `sort` VALUES ('15', '2.6.3', '0', '豪门', '2019-07-13 08:29:15');
INSERT INTO `sort` VALUES ('16', '2.6.3', '0', '幻言', '2019-07-13 08:29:15');
INSERT INTO `sort` VALUES ('17', '2.6.3', '0', '校园', '2019-07-13 08:29:15');
INSERT INTO `sort` VALUES ('18', '2.6.3', '0', '架空', '2019-07-13 08:29:15');
INSERT INTO `sort` VALUES ('19', '2.6.3', '0', '都市', '2019-07-13 08:29:15');
INSERT INTO `sort` VALUES ('20', '2.6.3', '0', '宫斗', '2019-07-13 08:29:15');
INSERT INTO `sort` VALUES ('21', '2.6.3', '0', '青春', '2019-07-13 08:29:15');
INSERT INTO `sort` VALUES ('22', '2.6.3', '0', '仙侣', '2019-07-13 08:29:15');
INSERT INTO `sort` VALUES ('23', '2.6.3', '0', '女尊', '2019-07-13 08:29:15');
INSERT INTO `sort` VALUES ('24', '2.6.2', '1', '生活', '2019-07-18 20:43:09');
INSERT INTO `sort` VALUES ('25', '2.6.2', '1', '异能', '2019-07-18 20:43:09');
INSERT INTO `sort` VALUES ('26', '2.6.2', '1', '重生', '2019-07-18 20:43:09');
INSERT INTO `sort` VALUES ('27', '2.6.2', '1', '热血', '2019-07-18 20:43:09');
INSERT INTO `sort` VALUES ('28', '2.6.2', '1', '都市人生', '2019-07-18 20:43:09');
INSERT INTO `sort` VALUES ('29', '2.6.2', '1', '极道江湖', '2019-07-18 20:43:09');
INSERT INTO `sort` VALUES ('30', '2.6.2', '1', '社会', '2019-07-18 20:43:09');
INSERT INTO `sort` VALUES ('31', '2.6.2', '1', '商战', '2019-07-18 20:43:09');
INSERT INTO `sort` VALUES ('32', '2.6.2', '1', '娱乐', '2019-07-18 20:43:09');
INSERT INTO `sort` VALUES ('33', '2.6.2', '1', '职场', '2019-07-18 20:43:09');
INSERT INTO `sort` VALUES ('34', '2.6.2', '1', '其他都市', '2019-07-18 20:43:09');
INSERT INTO `sort` VALUES ('35', '2.6.2', '1', '兵王', '2019-07-18 20:43:09');
INSERT INTO `sort` VALUES ('36', '2.6.2', '1', '保镖', '2019-07-18 20:43:09');
INSERT INTO `sort` VALUES ('37', '2.6.2', '2', '东方玄幻', '2019-07-18 20:43:09');
INSERT INTO `sort` VALUES ('38', '2.6.2', '2', '异界大陆', '2019-07-18 20:43:09');
INSERT INTO `sort` VALUES ('39', '2.6.2', '2', '转世重生', '2019-07-18 20:43:09');
INSERT INTO `sort` VALUES ('40', '2.6.2', '2', '远古神话', '2019-07-18 20:43:09');
INSERT INTO `sort` VALUES ('41', '2.6.2', '2', '其他玄幻', '2019-07-18 20:43:09');
INSERT INTO `sort` VALUES ('42', '2.6.2', '3', '现代修真', '2019-07-18 20:43:09');
INSERT INTO `sort` VALUES ('43', '2.6.2', '3', '奇幻修真', '2019-07-18 20:43:09');
INSERT INTO `sort` VALUES ('44', '2.6.2', '3', '洪荒封神', '2019-07-18 20:43:09');
INSERT INTO `sort` VALUES ('45', '2.6.2', '3', '古典仙侠', '2019-07-18 20:43:09');
INSERT INTO `sort` VALUES ('46', '2.6.3', '12', '唯美', '2019-07-18 22:05:14');
INSERT INTO `sort` VALUES ('47', '2.6.3', '12', '虐文', '2019-07-18 22:06:25');
INSERT INTO `sort` VALUES ('48', '2.6.3', '12', '网游', '2019-07-18 22:06:25');
INSERT INTO `sort` VALUES ('49', '2.6.3', '12', '总裁', '2019-07-18 22:06:25');
INSERT INTO `sort` VALUES ('50', '2.6.3', '12', '宝宝', '2019-07-18 22:06:25');
INSERT INTO `sort` VALUES ('51', '2.6.3', '12', '爽文', '2019-07-18 22:06:25');
INSERT INTO `sort` VALUES ('52', '2.6.3', '12', '宠文', '2019-07-18 22:06:25');
INSERT INTO `sort` VALUES ('53', '2.6.3', '12', '腹黑', '2019-07-18 22:06:25');
INSERT INTO `sort` VALUES ('54', '2.6.3', '12', '情感', '2019-07-18 22:06:25');
INSERT INTO `sort` VALUES ('55', '2.6.3', '12', '现代', '2019-07-18 22:06:25');

-- ----------------------------
-- Table structure for sort_join_books
-- ----------------------------
DROP TABLE IF EXISTS `sort_join_books`;
CREATE TABLE `sort_join_books` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sort_id` int(11) DEFAULT NULL COMMENT '分类ID',
  `books_id` bigint(11) DEFAULT '0' COMMENT '小说ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8 COMMENT='分类和小说关联';

-- ----------------------------
-- Records of sort_join_books
-- ----------------------------
INSERT INTO `sort_join_books` VALUES ('47', '40', '2');
INSERT INTO `sort_join_books` VALUES ('48', '41', '2');
INSERT INTO `sort_join_books` VALUES ('49', '37', '1');
INSERT INTO `sort_join_books` VALUES ('50', '38', '1');
INSERT INTO `sort_join_books` VALUES ('51', '39', '1');

-- ----------------------------
-- Table structure for sort_join_users
-- ----------------------------
DROP TABLE IF EXISTS `sort_join_users`;
CREATE TABLE `sort_join_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sort_id` int(11) DEFAULT NULL COMMENT '分类ID',
  `users_id` int(11) DEFAULT '0' COMMENT '用户ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='分类和用户关联';

-- ----------------------------
-- Records of sort_join_users
-- ----------------------------
INSERT INTO `sort_join_users` VALUES ('1', '1', '1');

-- ----------------------------
-- Table structure for suggestions
-- ----------------------------
DROP TABLE IF EXISTS `suggestions`;
CREATE TABLE `suggestions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `sug_type` varchar(10) DEFAULT NULL COMMENT '意见类型',
  `sug_content` varchar(400) DEFAULT NULL COMMENT '反馈内容',
  `sug_reply` varchar(400) DEFAULT NULL COMMENT '针对反馈做出的回复',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `reply_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `replier` int(11) DEFAULT NULL COMMENT '回复人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='意见反馈';

-- ----------------------------
-- Records of suggestions
-- ----------------------------
INSERT INTO `suggestions` VALUES ('2', '4', '5.20.1', '淡淡的', '对对对dd', '2019-08-01 21:43:48', '2019-08-22 21:19:37', null);
INSERT INTO `suggestions` VALUES ('3', '4', '5.20.2', '没办法充值啊', null, '2019-09-01 22:06:54', '2019-09-01 22:06:54', null);
INSERT INTO `suggestions` VALUES ('4', '4', '5.20.2', '没法充值', '多大点事', '2019-09-01 23:03:13', '2019-09-01 23:03:13', null);

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nick_name` varchar(200) DEFAULT NULL COMMENT '昵称',
  `sex` char(4) DEFAULT '男' COMMENT '性别',
  `avator` varchar(250) DEFAULT NULL COMMENT '头像',
  `tel` varchar(32) DEFAULT NULL COMMENT '手机号',
  `email` varchar(50) DEFAULT NULL COMMENT '邮箱',
  `login_pwd` varchar(50) DEFAULT NULL COMMENT '登录密码',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_login_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '上次登录时间',
  `imei` varchar(80) DEFAULT NULL COMMENT '设备号',
  `push_key` varchar(80) DEFAULT NULL COMMENT '推送key',
  `is_vip` tinyint(4) DEFAULT '0' COMMENT '1.是 0.否',
  `vip_end_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'vip到期时间',
  `gold` decimal(10,0) DEFAULT '0' COMMENT '阅读币',
  `qq` varchar(15) DEFAULT NULL COMMENT '绑定的qq',
  `weibo` varchar(50) DEFAULT NULL COMMENT '绑定的微博',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='用户表';

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('4', '大啊', '男', 'https://bucket-test002.oss-cn-beijing.aliyuncs.com//2019/07/24/61396CD43E154B3CBA898DF8CF5B567A.jpg', 'MTUxMTAxNDg2MDk=', null, '', '2019-07-21 17:41:35', '2019-09-18 10:53:57', null, null, '0', '2019-11-05 22:55:21', '2720', null, null);
