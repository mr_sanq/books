package com.sanq.product.books.service;

import com.sanq.product.books.entity.vo.OsReportVo;
import com.sanq.product.config.utils.entity.Pager;
import com.sanq.product.config.utils.entity.Pagination;
import java.util.List;

public interface OsReportService {
	
	int save(OsReportVo osReportVo);
	
	int delete(OsReportVo osReportVo);
	
	int update(OsReportVo osReportVo, Integer id);
	
	OsReportVo findById(Integer id);
	
	List<OsReportVo> findList(OsReportVo osReportVo);
	
	Pager<OsReportVo> findListByPage(OsReportVo osReportVo, Pagination pagination);
	
	int findCount(OsReportVo osReportVo);
	
	void saveByList(List<OsReportVo> osReportVos);
}