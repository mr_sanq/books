package com.sanq.product.books.entity.vo;


import com.sanq.product.books.entity.Permission;

import java.util.List;

public class PermissionVo extends Permission {

	/**
	 *	version: 资源表扩展实体
	 *----------------------
	 * 	author:xiezhyan
	 * 	date:2019-05-21
	 */
	private static final long serialVersionUID = 1L;

	private PermissionVo parent;

	private List<PermissionVo> permissionVos;

	private List<PermissionVo> childs = null;

	public List<PermissionVo> getChilds() {
		return childs;
	}

	public void setChilds(List<PermissionVo> childs) {
		this.childs = childs;
	}

	public List<PermissionVo> getPermissionVos() {
		return permissionVos;
	}

	public void setPermissionVos(List<PermissionVo> permissionVos) {
		this.permissionVos = permissionVos;
	}

	public PermissionVo getParent() {
		return parent;
	}

	public void setParent(PermissionVo parent) {
		this.parent = parent;
	}
}
