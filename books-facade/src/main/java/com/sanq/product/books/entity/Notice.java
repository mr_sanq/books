package com.sanq.product.books.entity;

import com.sanq.product.config.utils.entity.Base;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

public class Notice extends Base implements Serializable {

	/**
	 *	version: 消息
	 *----------------------
	 * 	author:xiezhyan
	 * 	date:2019-07-28
	 */
	private static final long serialVersionUID = 1L;
	
	/***/
	private Integer id;
	/**用户ID*/
	private Integer userId;
	/**消息名称*/
	private String noticeTitle;
	/**消息内容*/
	private String noticeContent;
	/**4.15.1.未阅读 4.15.2.已阅读*/
	private String isRead;
	/***/
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date createTime;
	/***/
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date readTime;

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getNoticeTitle() {
		return noticeTitle;
	}
	public void setNoticeTitle(String noticeTitle) {
		this.noticeTitle = noticeTitle;
	}

	public String getNoticeContent() {
		return noticeContent;
	}
	public void setNoticeContent(String noticeContent) {
		this.noticeContent = noticeContent;
	}

	public String getIsRead() {
		return isRead;
	}

	public void setIsRead(String isRead) {
		this.isRead = isRead;
	}

	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getReadTime() {
		return readTime;
	}
	public void setReadTime(Date readTime) {
		this.readTime = readTime;
	}

}
