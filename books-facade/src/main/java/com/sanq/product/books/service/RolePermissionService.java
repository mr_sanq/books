package com.sanq.product.books.service;

import com.sanq.product.books.entity.vo.RolePermissionVo;
import com.sanq.product.config.utils.entity.Pager;
import com.sanq.product.config.utils.entity.Pagination;

import java.util.List;

public interface RolePermissionService {
	
	int save(RolePermissionVo rolePermissionVo);
	
	int delete(RolePermissionVo rolePermissionVo);
	
	int update(RolePermissionVo rolePermissionVo, Integer id);
	
	RolePermissionVo findById(Integer id);
	
	List<RolePermissionVo> findList(RolePermissionVo rolePermissionVo);
	
	Pager<RolePermissionVo> findListByPage(RolePermissionVo rolePermissionVo, Pagination pagination);
	
	int findCount(RolePermissionVo rolePermissionVo);
	
	void saveByList(List<RolePermissionVo> rolePermissionVos);

    List<Integer> findPermissionIdByRoleId(Integer roleId);

}