package com.sanq.product.books.entity;

import java.io.Serializable;
import java.util.*;
import java.math.BigDecimal;
import org.springframework.format.annotation.DateTimeFormat;

public class Product  implements Serializable {

	/**
	 *	version: 充值产品
	 *----------------------
	 * 	author:xiezhyan
	 * 	date:2019-07-28
	 */
	private static final long serialVersionUID = 1L;
	
	/***/
	private Integer id;
	/**产品名称*/
	private String productName;
	/**金币/vip天数*/
	private BigDecimal obtain;
	/**额外所得*/
	private BigDecimal extraObtain;
	/**1.金币 2.VIP*/
	private Integer productType;
	/**价格*/
	private BigDecimal price;
	/***/
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date createTime;

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}

	public BigDecimal getObtain() {
		return obtain;
	}
	public void setObtain(BigDecimal obtain) {
		this.obtain = obtain;
	}

	public BigDecimal getExtraObtain() {
		return extraObtain;
	}
	public void setExtraObtain(BigDecimal extraObtain) {
		this.extraObtain = extraObtain;
	}

	public Integer getProductType() {
		return productType;
	}
	public void setProductType(Integer productType) {
		this.productType = productType;
	}

	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

}
