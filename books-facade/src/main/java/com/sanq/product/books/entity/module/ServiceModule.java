package com.sanq.product.books.entity.module;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by XieZhyan on 2019/1/22.
 */
public class ServiceModule implements Serializable{

    private Date dayStart;
    private Date dayEnd;

    private Date lastDayStart;
    private Date lastDayEnd;

    private Date monthDayStart;
    private Date monthDayEnd;

    private Date lastMonthDayStart;
    private Date lastMonthDayEnd;

    private Integer userId;

//    public ServiceModule() {
//    }

    public ServiceModule(Date dayStart, Date dayEnd, Date lastDayStart, Date lastDayEnd, Date monthDayStart, Date monthDayEnd, Date lastMonthDayStart, Date lastMonthDayEnd, Integer userId) {
        this.dayStart = dayStart;
        this.dayEnd = dayEnd;
        this.lastDayStart = lastDayStart;
        this.lastDayEnd = lastDayEnd;
        this.monthDayStart = monthDayStart;
        this.monthDayEnd = monthDayEnd;
        this.lastMonthDayStart = lastMonthDayStart;
        this.lastMonthDayEnd = lastMonthDayEnd;
        this.userId = userId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Date getDayStart() {
        return dayStart;
    }

    public void setDayStart(Date dayStart) {
        this.dayStart = dayStart;
    }

    public Date getDayEnd() {
        return dayEnd;
    }

    public void setDayEnd(Date dayEnd) {
        this.dayEnd = dayEnd;
    }

    public Date getLastDayStart() {
        return lastDayStart;
    }

    public void setLastDayStart(Date lastDayStart) {
        this.lastDayStart = lastDayStart;
    }

    public Date getLastDayEnd() {
        return lastDayEnd;
    }

    public void setLastDayEnd(Date lastDayEnd) {
        this.lastDayEnd = lastDayEnd;
    }

    public Date getMonthDayStart() {
        return monthDayStart;
    }

    public void setMonthDayStart(Date monthDayStart) {
        this.monthDayStart = monthDayStart;
    }

    public Date getMonthDayEnd() {
        return monthDayEnd;
    }

    public void setMonthDayEnd(Date monthDayEnd) {
        this.monthDayEnd = monthDayEnd;
    }

    public Date getLastMonthDayStart() {
        return lastMonthDayStart;
    }

    public void setLastMonthDayStart(Date lastMonthDayStart) {
        this.lastMonthDayStart = lastMonthDayStart;
    }

    public Date getLastMonthDayEnd() {
        return lastMonthDayEnd;
    }

    public void setLastMonthDayEnd(Date lastMonthDayEnd) {
        this.lastMonthDayEnd = lastMonthDayEnd;
    }

    public static class Builder {
        private Date dayStart;
        private Date dayEnd;

        private Date lastDayStart;
        private Date lastDayEnd;

        private Date monthDayStart;
        private Date monthDayEnd;

        private Date lastMonthDayStart;
        private Date lastMonthDayEnd;

        private Integer userId;

        public Builder setDayStart(Date dayStart) {
            this.dayStart = dayStart;
            return this;
        }

        public Builder setDayEnd(Date dayEnd) {
            this.dayEnd = dayEnd;
            return this;
        }

        public Builder setLastDayStart(Date lastDayStart) {
            this.lastDayStart = lastDayStart;
            return this;
        }

        public Builder setLastDayEnd(Date lastDayEnd) {
            this.lastDayEnd = lastDayEnd;
            return this;
        }

        public Builder setMonthDayStart(Date monthDayStart) {
            this.monthDayStart = monthDayStart;
            return this;
        }

        public Builder setMonthDayEnd(Date monthDayEnd) {
            this.monthDayEnd = monthDayEnd;
            return this;
        }

        public Builder setLastMonthDayStart(Date lastMonthDayStart) {
            this.lastMonthDayStart = lastMonthDayStart;
            return this;
        }

        public Builder setLastMonthDayEnd(Date lastMonthDayEnd) {
            this.lastMonthDayEnd = lastMonthDayEnd;
            return this;
        }

        public Builder setUserId(Integer userId) {
            this.userId = userId;
            return this;
        }

        public ServiceModule build() {
            return new ServiceModule(dayStart, dayEnd, lastDayStart, lastDayEnd, monthDayStart, monthDayEnd, lastMonthDayStart, lastMonthDayEnd, userId);
        }
    }
}
