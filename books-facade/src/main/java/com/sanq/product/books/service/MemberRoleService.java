package com.sanq.product.books.service;

import com.sanq.product.books.entity.vo.MemberRoleVo;
import com.sanq.product.config.utils.entity.Pager;
import com.sanq.product.config.utils.entity.Pagination;

import java.util.List;

public interface MemberRoleService {
	
	int save(MemberRoleVo memberRoleVo);
	
	int delete(MemberRoleVo memberRoleVo);
	
	int update(MemberRoleVo memberRoleVo, Integer id);
	
	MemberRoleVo findById(Integer id);
	
	List<MemberRoleVo> findList(MemberRoleVo memberRoleVo);
	
	Pager<MemberRoleVo> findListByPage(MemberRoleVo memberRoleVo, Pagination pagination);
	
	int findCount(MemberRoleVo memberRoleVo);
	
	void saveByList(List<MemberRoleVo> memberRoleVos);

    List<Integer> findRoleIdListByMemberId(Integer memberId);

}