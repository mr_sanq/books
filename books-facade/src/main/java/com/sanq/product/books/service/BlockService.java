package com.sanq.product.books.service;

import com.sanq.product.books.entity.vo.BlockVo;
import com.sanq.product.config.utils.entity.Pager;
import com.sanq.product.config.utils.entity.Pagination;
import java.util.List;

public interface BlockService {
	
	int save(BlockVo blockVo);
	
	int delete(BlockVo blockVo);
	
	int update(BlockVo blockVo, Integer id);
	
	BlockVo findById(Integer id);
	
	List<BlockVo> findList(BlockVo blockVo);
	
	Pager<BlockVo> findListByPage(BlockVo blockVo, Pagination pagination);
	
	int findCount(BlockVo blockVo);
	
	void saveByList(List<BlockVo> blockVos);
}