package com.sanq.product.books.entity;

import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

public class Role  implements Serializable {

	/**
	 *	version: 角色
	 *----------------------
	 * 	author:xiezhyan
	 * 	date:2019-05-23
	 */
	private static final long serialVersionUID = 1L;
	
	/***/
	private Integer id;
	/***/
	private String roleName;
	/***/
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date createTime;

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

}
