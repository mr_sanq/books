package com.sanq.product.books.entity;

import com.sanq.product.config.utils.entity.Base;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

public class Suggestions extends Base implements Serializable {

	/**
	 *	version: 意见反馈
	 *----------------------
	 * 	author:xiezhyan
	 * 	date:2019-07-28
	 */
	private static final long serialVersionUID = 1L;
	
	/***/
	private Integer id;
	/***/
	private Integer userId;
	/**意见类型*/
	private String sugType;
	/**反馈内容*/
	private String sugContent;
	/**针对反馈做出的回复*/
	private String sugReply;
	/***/
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date createTime;
	/***/
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date replyTime;
	/**回复人*/
	private Integer replier;

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getSugType() {
		return sugType;
	}
	public void setSugType(String sugType) {
		this.sugType = sugType;
	}

	public String getSugContent() {
		return sugContent;
	}
	public void setSugContent(String sugContent) {
		this.sugContent = sugContent;
	}

	public String getSugReply() {
		return sugReply;
	}
	public void setSugReply(String sugReply) {
		this.sugReply = sugReply;
	}

	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getReplyTime() {
		return replyTime;
	}
	public void setReplyTime(Date replyTime) {
		this.replyTime = replyTime;
	}

	public Integer getReplier() {
		return replier;
	}
	public void setReplier(Integer replier) {
		this.replier = replier;
	}

}
