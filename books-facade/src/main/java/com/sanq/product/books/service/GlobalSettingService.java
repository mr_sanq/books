package com.sanq.product.books.service;

import com.sanq.product.books.entity.vo.GlobalSettingVo;
import com.sanq.product.config.utils.entity.Pager;
import com.sanq.product.config.utils.entity.Pagination;
import java.util.List;

public interface GlobalSettingService {
	
	int save(GlobalSettingVo globalSettingVo);
	
	int delete(GlobalSettingVo globalSettingVo);
	
	int update(GlobalSettingVo globalSettingVo, Integer id);
	
	GlobalSettingVo findById(Integer id);
	
	List<GlobalSettingVo> findList(GlobalSettingVo globalSettingVo);
	
	Pager<GlobalSettingVo> findListByPage(GlobalSettingVo globalSettingVo, Pagination pagination);
	
	int findCount(GlobalSettingVo globalSettingVo);
	
	void saveByList(List<GlobalSettingVo> globalSettingVos);
}