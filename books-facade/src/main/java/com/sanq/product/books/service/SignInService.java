package com.sanq.product.books.service;

import com.sanq.product.books.entity.module.ServiceModule;
import com.sanq.product.books.entity.vo.SignInVo;
import com.sanq.product.config.utils.entity.Pager;
import com.sanq.product.config.utils.entity.Pagination;
import java.util.List;

public interface SignInService {
	
	int save(SignInVo signInVo);
	
	int delete(SignInVo signInVo);
	
	int update(SignInVo signInVo, Integer id);
	
	SignInVo findById(Integer id);
	
	List<SignInVo> findList(SignInVo signInVo);
	
	Pager<SignInVo> findListByPage(SignInVo signInVo, Pagination pagination);
	
	int findCount(SignInVo signInVo);
	
	void saveByList(List<SignInVo> signInVos);

	void deleteLastMonth(ServiceModule module);

}