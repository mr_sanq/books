package com.sanq.product.books.service;

import com.sanq.product.books.entity.vo.SuggestionsVo;
import com.sanq.product.config.utils.entity.Pager;
import com.sanq.product.config.utils.entity.Pagination;
import java.util.List;

public interface SuggestionsService {
	
	int save(SuggestionsVo suggestionsVo);
	
	int delete(SuggestionsVo suggestionsVo);
	
	int update(SuggestionsVo suggestionsVo, Integer id);
	
	SuggestionsVo findById(Integer id);
	
	List<SuggestionsVo> findList(SuggestionsVo suggestionsVo);
	
	Pager<SuggestionsVo> findListByPage(SuggestionsVo suggestionsVo,Pagination pagination);
	
	int findCount(SuggestionsVo suggestionsVo);
	
	void saveByList(List<SuggestionsVo> suggestionsVos);
}