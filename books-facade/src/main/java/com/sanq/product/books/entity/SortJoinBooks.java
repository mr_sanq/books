package com.sanq.product.books.entity;

import java.io.Serializable;

public class SortJoinBooks  implements Serializable {

	/**
	 *	version: 分类和小说关联
	 *----------------------
	 * 	author:xiezhyan
	 * 	date:2019-07-13
	 */
	private static final long serialVersionUID = 1L;
	
	/***/
	private Integer id;
	/**分类ID*/
	private Integer sortId;
	/**小说ID*/
	private Long booksId;

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getSortId() {
		return sortId;
	}
	public void setSortId(Integer sortId) {
		this.sortId = sortId;
	}

	public Long getBooksId() {
		return booksId;
	}

	public void setBooksId(Long booksId) {
		this.booksId = booksId;
	}
}
