package com.sanq.product.books.service;

import com.sanq.product.books.entity.vo.PayHistoryVo;
import com.sanq.product.config.utils.entity.Pager;
import com.sanq.product.config.utils.entity.Pagination;
import java.util.List;

public interface PayHistoryService {
	
	int save(PayHistoryVo payHistoryVo);
	
	int delete(PayHistoryVo payHistoryVo);
	
	int update(PayHistoryVo payHistoryVo, Integer id);
	
	PayHistoryVo findById(Integer id);
	
	List<PayHistoryVo> findList(PayHistoryVo payHistoryVo);
	
	Pager<PayHistoryVo> findListByPage(PayHistoryVo payHistoryVo, Pagination pagination);
	
	int findCount(PayHistoryVo payHistoryVo);
	
	void saveByList(List<PayHistoryVo> payHistoryVos);
}