package com.sanq.product.books.service;

import com.sanq.product.books.entity.vo.ProductVo;
import com.sanq.product.config.utils.entity.Pager;
import com.sanq.product.config.utils.entity.Pagination;
import java.util.List;

public interface ProductService {
	
	int save(ProductVo productVo);
	
	int delete(ProductVo productVo);
	
	int update(ProductVo productVo, Integer id);
	
	ProductVo findById(Integer id);
	
	List<ProductVo> findList(ProductVo productVo);
	
	Pager<ProductVo> findListByPage(ProductVo productVo,Pagination pagination);
	
	int findCount(ProductVo productVo);
	
	void saveByList(List<ProductVo> productVos);
}