package com.sanq.product.books.entity;

import java.io.Serializable;

/**
 * com.sanq.product.books.entity.VisiLog
 *
 * @author sanq.Yan
 * @date 2019/8/31
 */
public class VisiLog implements Serializable {

    //ID
    private Long id;

    //访问时间
    private String day;

    //访问IP
    private String ip;

    //浏览器agent
    private String userAgent;

    //设备版本
    private String os;

    //访问地址
    private String url;

    //省份
    private String provName;

    //城市
    private String cityName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProvName() {
        return provName;
    }

    public void setProvName(String provName) {
        this.provName = provName;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
