package com.sanq.product.books.entity;

import java.io.Serializable;

public class SortJoinUsers  implements Serializable {

	/**
	 *	version: 分类和用户关联
	 *----------------------
	 * 	author:xiezhyan
	 * 	date:2019-07-14
	 */
	private static final long serialVersionUID = 1L;
	
	/***/
	private Integer id;
	/**分类ID*/
	private Integer sortId;
	/**用户ID*/
	private Integer usersId;

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getSortId() {
		return sortId;
	}
	public void setSortId(Integer sortId) {
		this.sortId = sortId;
	}

	public Integer getUsersId() {
		return usersId;
	}
	public void setUsersId(Integer usersId) {
		this.usersId = usersId;
	}

}
