package com.sanq.product.books.service;

import com.sanq.product.books.entity.module.ServiceModule;
import com.sanq.product.books.entity.vo.UsersVo;
import com.sanq.product.config.utils.entity.Pager;
import com.sanq.product.config.utils.entity.Pagination;
import java.util.List;
import java.util.Map;

public interface UsersService {
	
	int save(UsersVo usersVo);
	
	int delete(UsersVo usersVo);
	
	int update(UsersVo usersVo, Integer id);
	
	UsersVo findById(Integer id);
	
	List<UsersVo> findList(UsersVo usersVo);
	
	Pager<UsersVo> findListByPage(UsersVo usersVo, Pagination pagination);
	
	int findCount(UsersVo usersVo);
	
	void saveByList(List<UsersVo> usersVos);

    Map<String, Integer> findSummaryUser(ServiceModule module);

    List<Map<String, Object>> findLineDataGroupTime();


}