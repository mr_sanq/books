package com.sanq.product.books.entity.vo;

import com.sanq.product.books.entity.RolePermission;

import java.util.List;

public class RolePermissionVo extends RolePermission {

	/**
	 *	version: 角色关联表扩展实体
	 *----------------------
	 * 	author:xiezhyan
	 * 	date:2019-05-21
	 */
	private static final long serialVersionUID = 1L;

	private List<Integer> permissionIds;

	public List<Integer> getPermissionIds() {
		return permissionIds;
	}

	public void setPermissionIds(List<Integer> permissionIds) {
		this.permissionIds = permissionIds;
	}
}
