package com.sanq.product.books.entity.vo;

import com.sanq.product.books.entity.SortJoinBooks;

import java.util.List;

public class SortJoinBooksVo extends SortJoinBooks{

	/**
	 *	version: 分类和小说关联扩展实体
	 *----------------------
	 * 	author:xiezhyan
	 * 	date:2019-07-13
	 */
	private static final long serialVersionUID = 1L;

	private List<Integer> sortIds;

	public List<Integer> getSortIds() {
		return sortIds;
	}

	public void setSortIds(List<Integer> sortIds) {
		this.sortIds = sortIds;
	}
}
