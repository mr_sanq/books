package com.sanq.product.books.service;

import com.sanq.product.books.entity.vo.BlockJoinBooksVo;
import com.sanq.product.config.utils.entity.Pager;
import com.sanq.product.config.utils.entity.Pagination;
import java.util.List;

public interface BlockJoinBooksService {
	
	int save(BlockJoinBooksVo blockJoinBooksVo);
	
	int delete(BlockJoinBooksVo blockJoinBooksVo);
	
	int update(BlockJoinBooksVo blockJoinBooksVo, Integer id);
	
	BlockJoinBooksVo findById(Integer id);
	
	List<BlockJoinBooksVo> findList(BlockJoinBooksVo blockJoinBooksVo);
	
	Pager<BlockJoinBooksVo> findListByPage(BlockJoinBooksVo blockJoinBooksVo, Pagination pagination);
	
	int findCount(BlockJoinBooksVo blockJoinBooksVo);
	
	void saveByList(List<BlockJoinBooksVo> blockJoinBooksVos);

    int saveRange(Integer blockId);

}