package com.sanq.product.books.entity;

import com.sanq.product.config.utils.entity.Base;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class Orders extends Base implements Serializable {

	/**
	 *	version: 
	 *----------------------
	 * 	author:xiezhyan
	 * 	date:2019-07-09
	 */
	private static final long serialVersionUID = 1L;
	
	/***/
	private Integer id;
	/**用户id*/
	private Integer userId;
	/**订单名称*/
	private String comboTitle;
	/**价格*/
	private BigDecimal comboPrice;
	/**支付状态 0.未支付 1. 已支付*/
	private Integer orderStatus;
	/**订单生成时间*/
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date orderCreateTime;
	/**订单号*/
	private String orderNum;
	/**订单支付时间*/
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date payTime;
	/**vip & glob数量*/
	private BigDecimal globVipCount;
	/***/
	private String payType;
	/**支付订单*/
	private String payOrderNum;
	/**1. 金币 2. vip*/
	private Integer orderType;

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getComboTitle() {
		return comboTitle;
	}
	public void setComboTitle(String comboTitle) {
		this.comboTitle = comboTitle;
	}

	public BigDecimal getComboPrice() {
		return comboPrice;
	}
	public void setComboPrice(BigDecimal comboPrice) {
		this.comboPrice = comboPrice;
	}

	public Integer getOrderStatus() {
		return orderStatus;
	}
	public void setOrderStatus(Integer orderStatus) {
		this.orderStatus = orderStatus;
	}

	public Date getOrderCreateTime() {
		return orderCreateTime;
	}
	public void setOrderCreateTime(Date orderCreateTime) {
		this.orderCreateTime = orderCreateTime;
	}

	public String getOrderNum() {
		return orderNum;
	}
	public void setOrderNum(String orderNum) {
		this.orderNum = orderNum;
	}

	public Date getPayTime() {
		return payTime;
	}
	public void setPayTime(Date payTime) {
		this.payTime = payTime;
	}

	public BigDecimal getGlobVipCount() {
		return globVipCount;
	}
	public void setGlobVipCount(BigDecimal globVipCount) {
		this.globVipCount = globVipCount;
	}

	public String getPayType() {
		return payType;
	}
	public void setPayType(String payType) {
		this.payType = payType;
	}

	public String getPayOrderNum() {
		return payOrderNum;
	}
	public void setPayOrderNum(String payOrderNum) {
		this.payOrderNum = payOrderNum;
	}

	public Integer getOrderType() {
		return orderType;
	}
	public void setOrderType(Integer orderType) {
		this.orderType = orderType;
	}

}
