package com.sanq.product.books.entity;

import java.io.Serializable;
import java.util.*;
import java.math.BigDecimal;
import org.springframework.format.annotation.DateTimeFormat;

public class Chapter  implements Serializable {

	/**
	 *	version: 章节表
	 *----------------------
	 * 	author:xiezhyan
	 * 	date:2019-07-09
	 */
	private static final long serialVersionUID = 1L;
	
	/***/
	private Long id;
	/**小说ID*/
	private Long bookId;
	/**章节名称*/
	private String chapterName;
	/**花费金币 0为免费*/
	private BigDecimal costGold;
	/**上一章*/
	private Long lastChapterId;
	/**下一章*/
	private Long nextChapterId;
	/***/
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date createTime;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getBookId() {
		return bookId;
	}

	public void setBookId(Long bookId) {
		this.bookId = bookId;
	}

	public Long getLastChapterId() {
		return lastChapterId;
	}

	public void setLastChapterId(Long lastChapterId) {
		this.lastChapterId = lastChapterId;
	}

	public Long getNextChapterId() {
		return nextChapterId;
	}

	public void setNextChapterId(Long nextChapterId) {
		this.nextChapterId = nextChapterId;
	}

	public String getChapterName() {
		return chapterName;
	}
	public void setChapterName(String chapterName) {
		this.chapterName = chapterName;
	}

	public BigDecimal getCostGold() {
		return costGold;
	}
	public void setCostGold(BigDecimal costGold) {
		this.costGold = costGold;
	}

	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

}
