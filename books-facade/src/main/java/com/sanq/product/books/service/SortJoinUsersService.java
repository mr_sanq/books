package com.sanq.product.books.service;

import com.sanq.product.books.entity.vo.SortJoinUsersVo;
import com.sanq.product.config.utils.entity.Pager;
import com.sanq.product.config.utils.entity.Pagination;
import java.util.List;

public interface SortJoinUsersService {
	
	int save(SortJoinUsersVo sortJoinUsersVo);
	
	int delete(SortJoinUsersVo sortJoinUsersVo);
	
	int update(SortJoinUsersVo sortJoinUsersVo, Integer id);
	
	SortJoinUsersVo findById(Integer id);
	
	List<SortJoinUsersVo> findList(SortJoinUsersVo sortJoinUsersVo);
	
	Pager<SortJoinUsersVo> findListByPage(SortJoinUsersVo sortJoinUsersVo, Pagination pagination);
	
	int findCount(SortJoinUsersVo sortJoinUsersVo);
	
	void saveByList(List<SortJoinUsersVo> sortJoinUsersVos);
}