package com.sanq.product.books.entity;

import com.sanq.product.config.utils.entity.Base;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

public class Member extends Base implements Serializable {

	/**
	 *	version: 后台登录账号
	 *----------------------
	 * 	author:xiezhyan
	 * 	date:2019-05-23
	 */
	private static final long serialVersionUID = 1L;
	
	/***/
	private Integer id;
	/***/
	private String loginName;
	/***/
	private String loginPwd;
	/***/
	private String realName;
	/***/
	private String email;
	/***/
	private String tel;
	/***/
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date createTime;
	/**账号状态：1.正常 0.锁定*/
	private Integer userStatus;
	/**1. 系统管理员*/
	private Integer userType;
	/**头像*/
	private String cover;

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getLoginPwd() {
		return loginPwd;
	}
	public void setLoginPwd(String loginPwd) {
		this.loginPwd = loginPwd;
	}

	public String getRealName() {
		return realName;
	}
	public void setRealName(String realName) {
		this.realName = realName;
	}

	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}

	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Integer getUserStatus() {
		return userStatus;
	}
	public void setUserStatus(Integer userStatus) {
		this.userStatus = userStatus;
	}

	public Integer getUserType() {
		return userType;
	}
	public void setUserType(Integer userType) {
		this.userType = userType;
	}

	public String getCover() {
		return cover;
	}
	public void setCover(String cover) {
		this.cover = cover;
	}

}
