package com.sanq.product.books.service;

import com.sanq.product.books.entity.vo.MemberVo;
import com.sanq.product.books.entity.vo.PermissionVo;
import com.sanq.product.config.utils.entity.Pager;
import com.sanq.product.config.utils.entity.Pagination;

import java.util.List;

public interface PermissionService {
	
	int save(PermissionVo permissionVo);
	
	int delete(PermissionVo permissionVo);
	
	int update(PermissionVo permissionVo, Integer id);
	
	PermissionVo findById(Integer id);
	
	List<PermissionVo> findList(PermissionVo permissionVo);
	
	Pager<PermissionVo> findListByPage(PermissionVo permissionVo, Pagination pagination);
	
	int findCount(PermissionVo permissionVo);
	
	void saveByList(List<PermissionVo> permissionVos);

    List<PermissionVo> findPermissionByMemberId(Integer parentId, MemberVo memberVo);

	List<PermissionVo> getPermissionByParentList(Integer parentId);

}