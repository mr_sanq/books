package com.sanq.product.books.entity.vo;

import com.sanq.product.books.entity.Suggestions;

public class SuggestionsVo extends Suggestions{

	/**
	 *	version: 意见反馈扩展实体
	 *----------------------
	 * 	author:xiezhyan
	 * 	date:2019-07-28
	 */
	private static final long serialVersionUID = 1L;

	private UsersVo usersVo;

	public UsersVo getUsersVo() {
		return usersVo;
	}

	public void setUsersVo(UsersVo usersVo) {
		this.usersVo = usersVo;
	}
}
