package com.sanq.product.books.service;

import com.sanq.product.books.entity.vo.SignVo;
import com.sanq.product.config.utils.entity.Pager;
import com.sanq.product.config.utils.entity.Pagination;
import java.util.List;

public interface SignService {
	
	int save(SignVo signVo);
	
	int delete(SignVo signVo);
	
	int update(SignVo signVo, Integer id);
	
	SignVo findById(Integer id);
	
	List<SignVo> findList(SignVo signVo);
	
	Pager<SignVo> findListByPage(SignVo signVo,Pagination pagination);
	
	int findCount(SignVo signVo);
	
	void saveByList(List<SignVo> signVos);

    int getSignByUserAndTime(SignVo signVo);
}