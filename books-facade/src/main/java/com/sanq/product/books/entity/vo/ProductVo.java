package com.sanq.product.books.entity.vo;

import com.sanq.product.books.entity.Product;

public class ProductVo extends Product{

	/**
	 *	version: 充值产品扩展实体
	 *----------------------
	 * 	author:xiezhyan
	 * 	date:2019-07-28
	 */
	private static final long serialVersionUID = 1L;

	private Integer isHot;

	public Integer getIsHot() {
		return isHot;
	}

	public void setIsHot(Integer isHot) {
		this.isHot = isHot;
	}
}
