package com.sanq.product.books.service;

import com.sanq.product.books.entity.vo.RoleVo;
import com.sanq.product.config.utils.entity.Pager;
import com.sanq.product.config.utils.entity.Pagination;

import java.util.List;

public interface RoleService {
	
	int save(RoleVo roleVo);
	
	int delete(RoleVo roleVo);
	
	int update(RoleVo roleVo, Integer id);
	
	RoleVo findById(Integer id);
	
	List<RoleVo> findList(RoleVo roleVo);
	
	Pager<RoleVo> findListByPage(RoleVo roleVo, Pagination pagination);
	
	int findCount(RoleVo roleVo);
	
	void saveByList(List<RoleVo> roleVos);
}