package com.sanq.product.books.entity.vo;

import com.sanq.product.books.entity.BaseData;

public class BaseDataVo extends BaseData{

	/**
	 *	version: 基础数据扩展实体
	 *----------------------
	 * 	author:xiezhyan
	 * 	date:2019-07-09
	 */
	private static final long serialVersionUID = 1L;

	private BaseDataVo baseDataVo;

	public BaseDataVo getBaseDataVo() {
		return baseDataVo;
	}

	public void setBaseDataVo(BaseDataVo baseDataVo) {
		this.baseDataVo = baseDataVo;
	}
}
