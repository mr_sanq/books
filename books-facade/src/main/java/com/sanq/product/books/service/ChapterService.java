package com.sanq.product.books.service;

import com.sanq.product.books.entity.vo.ChapterVo;
import com.sanq.product.books.entity.vo.UsersVo;
import com.sanq.product.config.utils.entity.Pager;
import com.sanq.product.config.utils.entity.Pagination;
import java.util.List;

public interface ChapterService {
	
	int save(ChapterVo chapterVo);
	
	int delete(ChapterVo chapterVo);
	
	int update(ChapterVo chapterVo, Long id);
	
	ChapterVo findById(Long id);
	
	List<ChapterVo> findList(ChapterVo chapterVo);
	
	Pager<ChapterVo> findListByPage(ChapterVo chapterVo, Pagination pagination);
	
	int findCount(ChapterVo chapterVo);
	
	void saveByList(List<ChapterVo> chapterVos);

    ChapterVo findTotalByBookId(Long bookId);

	Long findChapterByBookId(Long id, UsersVo usersVo);

    ChapterVo findChapterContentByBookIdAndChapter(Long booksId, Long chapterId);

	Long findPrevChapter(Long bookId, Long id);

	Long findNextChapter(Long bookId, Long id);

    void saveChapters(long l);
}