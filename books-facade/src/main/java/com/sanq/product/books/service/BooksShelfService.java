package com.sanq.product.books.service;

import com.sanq.product.books.entity.vo.BooksShelfVo;
import com.sanq.product.config.utils.entity.Pager;
import com.sanq.product.config.utils.entity.Pagination;
import java.util.List;

public interface BooksShelfService {
	
	int save(BooksShelfVo booksShelfVo);
	
	int delete(BooksShelfVo booksShelfVo);
	
	int update(BooksShelfVo booksShelfVo, Integer id);
	
	BooksShelfVo findById(Integer id);
	
	List<BooksShelfVo> findList(BooksShelfVo booksShelfVo);
	
	Pager<BooksShelfVo> findListByPage(BooksShelfVo booksShelfVo, Pagination pagination);
	
	int findCount(BooksShelfVo booksShelfVo);
	
	void saveByList(List<BooksShelfVo> booksShelfVos);
}