package com.sanq.product.books.service;

import com.sanq.product.books.entity.vo.JobsVo;
import com.sanq.product.config.utils.entity.Pager;
import com.sanq.product.config.utils.entity.Pagination;

import java.util.List;

public interface JobsService {
	
	int save(JobsVo jobsVo);
	
	int delete(JobsVo jobsVo);
	
	int update(JobsVo jobsVo, Long id);
	
	JobsVo findById(Long id);
	
	List<JobsVo> findList(JobsVo jobsVo);
	
	Pager<JobsVo> findListByPage(JobsVo jobsVo, Pagination pagination);
	
	int findCount(JobsVo jobsVo);
	
	void saveByList(List<JobsVo> jobsVos);
}