package com.sanq.product.books.entity.vo;

import com.sanq.product.books.entity.Discuss;

public class DiscussVo extends Discuss{

	/**
	 *	version: 评论扩展实体
	 *----------------------
	 * 	author:xiezhyan
	 * 	date:2019-07-09
	 */
	private static final long serialVersionUID = 1L;

	private UsersVo usersVo;

	private BooksVo booksVo;

	public BooksVo getBooksVo() {
		return booksVo;
	}

	public void setBooksVo(BooksVo booksVo) {
		this.booksVo = booksVo;
	}

	public UsersVo getUsersVo() {
		return usersVo;
	}

	public void setUsersVo(UsersVo usersVo) {
		this.usersVo = usersVo;
	}
}
