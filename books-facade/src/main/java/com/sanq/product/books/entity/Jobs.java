package com.sanq.product.books.entity;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

public class Jobs implements Serializable {

	/**
	 *	version: 定时任务列表
	 *----------------------
	 * 	author:xiezhyan
	 * 	date:2019-11-07
	 */
	private static final long serialVersionUID = 1L;
	
	/***/
	private Long id;
	/**任务类名*/
	@NotBlank(message = "请输入任务类名")
	private String jobClazz;
	/**任务名称*/
	@NotBlank(message = "请输入任务名称")
	private String jobName;
	/**描述*/
	private String jobDesc;
	/**额外参数*/
	private String extra;
	/**定时表达式*/
	@NotBlank(message = "请输入任务定时表达式")
	private String cron;
	/**是否开始执行*/
	@NotNull(message = "请选择是否开始执行")
	@Min(value = 0, message = "是否开始执行规则： 最小为0")
	@Max(value = 1, message = "是否开始执行规则： 最大为1")
	private Integer isEnable;
	/***/
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date createTime;

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	public String getJobClazz() {
		return jobClazz;
	}
	public void setJobClazz(String jobClazz) {
		this.jobClazz = jobClazz;
	}

	public String getJobName() {
		return jobName;
	}
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public String getJobDesc() {
		return jobDesc;
	}
	public void setJobDesc(String jobDesc) {
		this.jobDesc = jobDesc;
	}

	public String getExtra() {
		return extra;
	}
	public void setExtra(String extra) {
		this.extra = extra;
	}

	public String getCron() {
		return cron;
	}
	public void setCron(String cron) {
		this.cron = cron;
	}

	public Integer getIsEnable() {
		return isEnable;
	}
	public void setIsEnable(Integer isEnable) {
		this.isEnable = isEnable;
	}

	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

}
