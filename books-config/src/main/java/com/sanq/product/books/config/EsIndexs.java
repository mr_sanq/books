package com.sanq.product.books.config;

import java.util.Date;

/**
 * com.sanq.product.books.config.EsIndex
 *
 * @author sanq.Yan
 * @date 2019/8/31
 */
public class EsIndexs {
    private EsIndexs() {
        throw new IllegalStateException("EsIndexs Is No Init");
    }


    public static class Indexs {
        public static final String _DOC = "_doc";
        public static final String VISI_LOG_INDEX = "visilog{date}";
        public static final String BOOKS_INDEX = "books";
    }

    public static class ReplaceIndex {
        public static String getVisiLogIndex(Date date) {

            return Indexs.VISI_LOG_INDEX.replace("{date}", Common.DateConvert.getDateNoFormat(date));
        }

    }
}
