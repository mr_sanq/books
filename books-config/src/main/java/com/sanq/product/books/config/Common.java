package com.sanq.product.books.config;

import com.sanq.product.config.utils.date.DateUtil;

import java.util.Date;

/**
 * com.sanq.product.books.config.Common
 *
 * @author sanq.Yan
 * @date 2019/7/19
 */
public class Common {
    public static class DateConvert {
        public static String getDate(Date date) {
            return DateUtil.date2Str(date, "yyyy-MM-dd");
        }

        public static String getDateNoFormat(Date date) {
            return DateUtil.date2Str(date, "yyyyMMdd");
        }

        public static String getDateTime(Date date) {
            return DateUtil.date2Str(date, "yyyy-MM-dd HH:mm:ss");
        }


        public static String getYearAndMonth(Date date) {
            return DateUtil.date2Str(date, "yyyy-MM");
        }
    }
}
