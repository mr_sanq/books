package com.sanq.product.books.es.service;

import com.sanq.product.books.entity.VisiLog;
import com.sanq.product.books.entity.vo.AreaReportVo;
import com.sanq.product.books.entity.vo.BaseReportVo;
import com.sanq.product.books.entity.vo.OsReportVo;
import com.sanq.product.utils.es.support.BaseSearchSupport;

import java.util.List;

/**
 * com.sanq.product.books.es.service.VisiLogSearchService
 *
 * @author sanq.Yan
 * @date 2019/8/31
 */
public interface VisiLogSearchService extends BaseSearchSupport<VisiLog> {

    void saveLogs(long step);

    boolean createIndex() throws Exception;

    BaseReportVo getBaseReport(String index);

    List<OsReportVo> getOsReport(String index);

    List<AreaReportVo> getAreaReport(String index);
}
