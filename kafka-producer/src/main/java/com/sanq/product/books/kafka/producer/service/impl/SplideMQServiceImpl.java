package com.sanq.product.books.kafka.producer.service.impl;

import com.sanq.product.books.kafka.producer.service.SplideMQService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * com.sanq.product.books.kafka.producer.service.impl.SplideMQServiceImpl
 *
 * @author sanq.Yan
 * @date 2019/12/18
 */
@Service("splideMQService")
public class SplideMQServiceImpl implements SplideMQService {

    @Resource
    private KafkaTemplate<String, String> kafkaTemplate;
    @Resource
    private ThreadPoolTaskExecutor taskExecutor;

    @Value("${kafka.common.topic}")
    public String kafkaCommonTopic;


    @Override
    public void startSaveToDB(String data, String key) {
        taskExecutor.execute(() -> kafkaTemplate.send(kafkaCommonTopic, key, data));
    }
}
