package com.sanq.product.books.kafka.producer.service;

/**
 * com.sanq.product.books.kafka.producer.service.NoticeMQService
 *
 * @author sanq.Yan
 * @date 2019/8/22
 */
public interface NoticeMQService {
    void reply2Suggestion(Integer suggestionId);
}
