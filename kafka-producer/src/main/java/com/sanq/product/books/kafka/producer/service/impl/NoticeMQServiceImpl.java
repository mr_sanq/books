package com.sanq.product.books.kafka.producer.service.impl;

import com.sanq.product.books.kafka.producer.service.NoticeMQService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * com.sanq.product.books.kafka.producer.service.impl.NoticeMQServiceImpl
 *
 * @author sanq.Yan
 * @date 2019/8/22
 */
@Service("noticeMQService")
public class NoticeMQServiceImpl implements NoticeMQService {

    @Resource
    private KafkaTemplate<String, String> kafkaTemplate;
    @Resource
    private ThreadPoolTaskExecutor taskExecutor;


    @Value("${kafka.notice.reply}")
    public String kafkaNoticeReplyTopic;

    @Override
    public void reply2Suggestion(Integer suggestionId) {
        taskExecutor.execute(() -> {
            kafkaTemplate.send(kafkaNoticeReplyTopic, suggestionId + "");
        });
    }

}
