package com.sanq.product.books.kafka.producer.service;

import com.sanq.product.books.entity.vo.BooksVo;

import java.util.List;

/**
 * com.sanq.product.books.kafka.producer.service.BooksMQService
 *
 * @author sanq.Yan
 * @date 2019/8/13
 */
public interface BooksMQService {

    //删除小说
    void deleteByBooksId(Long booksId);

    //删除章节
    void deleteByChapterId(Long chapterId);

    //添加到ES
    void save2Es(Long booksId, String type);

    //批量添加到ES中
    void save2Es(List<BooksVo> booksVos, String type);

}
