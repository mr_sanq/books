package com.sanq.product.books.kafka.producer.service.impl;

import com.sanq.product.books.kafka.producer.service.VisiLogMQService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service("visiLogMQService")
public class VisiLogMQServiceImpl implements VisiLogMQService {

    @Resource
    private KafkaTemplate<String, String> kafkaTemplate;
    @Resource
    private ThreadPoolTaskExecutor taskExecutor;

    @Value("${kafka.visi.log}")
    public String kafkaVisiLog;

    @Override
    public void saveLog2Es(String type) {
        taskExecutor.execute(() -> {
            kafkaTemplate.send(kafkaVisiLog, type);
        });
    }
}
