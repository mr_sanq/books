package com.sanq.product.books.kafka.producer.service;

/**
 * com.sanq.product.books.kafka.producer.service.SplideMQService
 *
 * @author sanq.Yan
 * @date 2019/12/18
 */
public interface SplideMQService {

    void startSaveToDB(String data, String key);

}
