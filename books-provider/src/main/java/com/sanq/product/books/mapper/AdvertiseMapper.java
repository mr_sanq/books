package com.sanq.product.books.mapper;

import com.sanq.product.books.entity.vo.AdvertiseVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface AdvertiseMapper {
	
	int save(AdvertiseVo advertiseVo);
	
	int delete(AdvertiseVo advertiseVo);
	
	int update(AdvertiseVo advertiseVo);
	
	AdvertiseVo findById(Integer id);
	
	List<AdvertiseVo> findList(@Param("advertise") AdvertiseVo advertiseVo);
	
	List<AdvertiseVo> findListByPage(@Param("advertise") AdvertiseVo advertiseVo, @Param("startPage") int startPage, @Param("pageSize") int pageSize);
	
	int findCount(@Param("advertise") AdvertiseVo advertiseVo);

	void saveByList(@Param("advertiseVos") List<AdvertiseVo> advertiseVos);
}