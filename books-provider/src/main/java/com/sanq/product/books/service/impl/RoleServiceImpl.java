package com.sanq.product.books.service.impl;

import com.sanq.product.books.entity.vo.RoleVo;
import com.sanq.product.books.mapper.RoleMapper;
import com.sanq.product.books.service.RoleService;
import com.sanq.product.config.utils.entity.Pager;
import com.sanq.product.config.utils.entity.Pagination;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


@Service("roleService")
public class RoleServiceImpl implements RoleService {

	@Resource
	private RoleMapper roleMapper;
	
	@Override
	public int save(RoleVo roleVo) {
		return roleMapper.save(roleVo);
	}
	
	@Override
	public int delete(RoleVo roleVo) {
		return roleMapper.delete(roleVo);
	}	
	
	@Override
	public int update(RoleVo roleVo, Integer id) {
		RoleVo oldRoleVo = findById(id);
		
		if(null != oldRoleVo && null != roleVo) {
			if(null != roleVo.getId()) {
				oldRoleVo.setId(roleVo.getId());
			}
			if(null != roleVo.getRoleName()) {
				oldRoleVo.setRoleName(roleVo.getRoleName());
			}
			if(null != roleVo.getCreateTime()) {
				oldRoleVo.setCreateTime(roleVo.getCreateTime());
			}
			return roleMapper.update(oldRoleVo);
		}
		return 0;
	}
	
	@Override
	public RoleVo findById(Integer id) {
		return roleMapper.findById(id);
	}
	
	@Override
	public List<RoleVo> findList(RoleVo roleVo) {
		return roleMapper.findList(roleVo);
	}
	
	@Override
	public Pager<RoleVo> findListByPage(RoleVo roleVo,Pagination pagination) {
		pagination.setTotalCount(findCount(roleVo));
		
		List<RoleVo> datas = roleMapper.findListByPage(roleVo,pagination.getStartPage(),pagination.getPageSize());
		
		return new Pager<RoleVo>(pagination,datas);
	}
	
	@Override
	public int findCount(RoleVo roleVo) {
		return roleMapper.findCount(roleVo);
	}
	
	@Override
	public void saveByList(List<RoleVo> roleVos) {
		roleMapper.saveByList(roleVos);
	}
}