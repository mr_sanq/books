package com.sanq.product.books.mapper;

import com.sanq.product.books.entity.vo.ChapterVo;
import com.sanq.product.books.entity.vo.UsersVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ChapterMapper {
	
	int save(ChapterVo chapterVo);
	
	int delete(ChapterVo chapterVo);
	
	int update(ChapterVo chapterVo);
	
	ChapterVo findById(Long id);
	
	List<ChapterVo> findList(@Param("chapter") ChapterVo chapterVo);
	
	List<ChapterVo> findListByPage(@Param("chapter") ChapterVo chapterVo, @Param("startPage") int startPage, @Param("pageSize") int pageSize);
	
	int findCount(@Param("chapter") ChapterVo chapterVo);

	void saveByList(@Param("chapterVos") List<ChapterVo> chapterVos);

    ChapterVo findTotalByBookId(Long bookId);

	Long findChapterByBookId(@Param("bookId")Long id, @Param("usersVo")UsersVo usersVo);

    ChapterVo findChapterContentByBookIdAndChapter(@Param("booksId")Long booksId, @Param("chapterId")Long chapterId);

	Long findPrevChapter(@Param("bookId")Long bookId, @Param("id")Long id);

	Long findNextChapter(@Param("bookId")Long bookId, @Param("id")Long id);

}