package com.sanq.product.books.service.impl;

import com.sanq.product.books.entity.vo.CollectionVo;
import com.sanq.product.config.utils.entity.Pager;
import com.sanq.product.config.utils.entity.Pagination;
import com.sanq.product.books.service.CollectionService;
import com.sanq.product.books.mapper.CollectionMapper;
import java.util.List;
import java.math.BigDecimal;

import javax.annotation.Resource;
import org.springframework.stereotype.Service;


@Service("collectionService")
public class CollectionServiceImpl implements CollectionService {

	@Resource
	private CollectionMapper collectionMapper;
	
	@Override
	public int save(CollectionVo collectionVo) {
		return collectionMapper.save(collectionVo);
	}
	
	@Override
	public int delete(CollectionVo collectionVo) {
		return collectionMapper.delete(collectionVo);
	}	
	
	@Override
	public int update(CollectionVo collectionVo, Integer id) {
		CollectionVo oldCollectionVo = findById(id);
		
		if(null != oldCollectionVo && null != collectionVo) {
			if(null != collectionVo.getId()) {
				oldCollectionVo.setId(collectionVo.getId());
			}
			if(null != collectionVo.getUserId()) {
				oldCollectionVo.setUserId(collectionVo.getUserId());
			}
			if(null != collectionVo.getBookId()) {
				oldCollectionVo.setBookId(collectionVo.getBookId());
			}
			if(null != collectionVo.getCreateTime()) {
				oldCollectionVo.setCreateTime(collectionVo.getCreateTime());
			}
			return collectionMapper.update(oldCollectionVo);
		}
		return 0;
	}
	
	@Override
	public CollectionVo findById(Integer id) {
		return collectionMapper.findById(id);
	}
	
	@Override
	public List<CollectionVo> findList(CollectionVo collectionVo) {
		return collectionMapper.findList(collectionVo);
	}
	
	@Override
	public Pager<CollectionVo> findListByPage(CollectionVo collectionVo,Pagination pagination) {
		pagination.setTotalCount(findCount(collectionVo));
		
		List<CollectionVo> datas = collectionMapper.findListByPage(collectionVo,pagination.getStartPage(),pagination.getPageSize());
		
		return new Pager<CollectionVo>(pagination,datas);
	}
	
	@Override
	public int findCount(CollectionVo collectionVo) {
		return collectionMapper.findCount(collectionVo);
	}
	
	@Override
	public void saveByList(List<CollectionVo> collectionVos) {
		collectionMapper.saveByList(collectionVos);
	}
}