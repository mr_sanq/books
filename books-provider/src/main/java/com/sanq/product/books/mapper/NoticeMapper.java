package com.sanq.product.books.mapper;

import com.sanq.product.books.entity.vo.NoticeVo;
import java.util.List;

import org.apache.ibatis.annotations.Param;

public interface NoticeMapper {
	
	int save(NoticeVo noticeVo);
	
	int delete(NoticeVo noticeVo);
	
	int update(NoticeVo noticeVo);
	
	NoticeVo findById(Integer id);
	
	List<NoticeVo> findList(@Param("notice")  NoticeVo noticeVo);
	
	List<NoticeVo> findListByPage(@Param("notice") NoticeVo noticeVo,@Param("startPage") int startPage, @Param("pageSize") int pageSize);
	
	int findCount(@Param("notice")  NoticeVo noticeVo);

	void saveByList(@Param("noticeVos") List<NoticeVo> noticeVos);
}