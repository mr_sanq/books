package com.sanq.product.books.mapper;

import com.sanq.product.books.entity.vo.BlockJoinBooksVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface BlockJoinBooksMapper {
	
	int save(BlockJoinBooksVo blockJoinBooksVo);
	
	int delete(BlockJoinBooksVo blockJoinBooksVo);
	
	int update(BlockJoinBooksVo blockJoinBooksVo);
	
	BlockJoinBooksVo findById(Integer id);
	
	List<BlockJoinBooksVo> findList(@Param("blockJoinBooks") BlockJoinBooksVo blockJoinBooksVo);
	
	List<BlockJoinBooksVo> findListByPage(@Param("blockJoinBooks") BlockJoinBooksVo blockJoinBooksVo, @Param("startPage") int startPage, @Param("pageSize") int pageSize);
	
	int findCount(@Param("blockJoinBooks") BlockJoinBooksVo blockJoinBooksVo);

	void saveByList(@Param("blockJoinBooksVos") List<BlockJoinBooksVo> blockJoinBooksVos);

    int saveRange(Integer blockId);

}