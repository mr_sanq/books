package com.sanq.product.books.service.impl;

import com.sanq.product.books.entity.vo.JobsVo;
import com.sanq.product.books.mapper.JobsMapper;
import com.sanq.product.books.service.JobsService;
import com.sanq.product.config.utils.entity.Pager;
import com.sanq.product.config.utils.entity.Pagination;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


@Service("jobsService")
public class JobsServiceImpl implements JobsService {

    @Resource
    private JobsMapper jobsMapper;
    @Resource
    private ThreadPoolTaskExecutor taskExecutor;


    @Override
    public int save(JobsVo jobsVo) {
        int save = jobsMapper.save(jobsVo);
        return save;
    }

    @Override
    public int delete(JobsVo jobsVo) {
        int delete = jobsMapper.delete(jobsVo);
        return delete;
    }

    @Override
    public int update(JobsVo jobsVo, Long id) {
        JobsVo oldJobsVo = findById(id);

        if (null != oldJobsVo && null != jobsVo) {
            if (null != jobsVo.getId()) {
                oldJobsVo.setId(jobsVo.getId());
            }
            if (null != jobsVo.getJobClazz()) {
                oldJobsVo.setJobClazz(jobsVo.getJobClazz());
            }
            if (null != jobsVo.getJobName()) {
                oldJobsVo.setJobName(jobsVo.getJobName());
            }
            if (null != jobsVo.getJobDesc()) {
                oldJobsVo.setJobDesc(jobsVo.getJobDesc());
            }
            if (null != jobsVo.getExtra()) {
                oldJobsVo.setExtra(jobsVo.getExtra());
            }
            if (null != jobsVo.getCron()) {
                oldJobsVo.setCron(jobsVo.getCron());
            }
            if (null != jobsVo.getIsEnable()) {
                oldJobsVo.setIsEnable(jobsVo.getIsEnable());
            }
            if (null != jobsVo.getCreateTime()) {
                oldJobsVo.setCreateTime(jobsVo.getCreateTime());
            }
            int update = jobsMapper.update(oldJobsVo);

            return update;
        }
        return 0;
    }

    @Override
    public JobsVo findById(Long id) {
        return jobsMapper.findById(id);
    }

    @Override
    public List<JobsVo> findList(JobsVo jobsVo) {
        List<JobsVo> list = jobsMapper.findList(jobsVo);

        return list;
    }

    @Override
    public Pager<JobsVo> findListByPage(JobsVo jobsVo, Pagination pagination) {
        pagination.setTotalCount(findCount(jobsVo));

        List<JobsVo> datas = jobsMapper.findListByPage(jobsVo, pagination.getStartPage(), pagination.getPageSize());

        return new Pager<JobsVo>(pagination, datas);
    }

    @Override
    public int findCount(JobsVo jobsVo) {
        return jobsMapper.findCount(jobsVo);
    }

    @Override
    public void saveByList(List<JobsVo> jobsVos) {
        jobsMapper.saveByList(jobsVos);
    }

}