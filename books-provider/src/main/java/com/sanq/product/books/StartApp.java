package com.sanq.product.books;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * com.sanq.product.books.App
 *
 * @author sanq.Yan
 * @date 2019/7/9
 */
public class StartApp {

    public static void main(String[] args) {

        new ClassPathXmlApplicationContext("classpath:spring/profile/spring-dev-application.xml").start();

        try {
            System.in.read();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
