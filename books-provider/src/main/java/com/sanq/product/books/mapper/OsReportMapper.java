package com.sanq.product.books.mapper;

import com.sanq.product.books.entity.vo.OsReportVo;
import java.util.List;

import org.apache.ibatis.annotations.Param;

public interface OsReportMapper {
	
	int save(OsReportVo osReportVo);
	
	int delete(OsReportVo osReportVo);
	
	int update(OsReportVo osReportVo);
	
	OsReportVo findById(Integer id);
	
	List<OsReportVo> findList(@Param("osReport") OsReportVo osReportVo);
	
	List<OsReportVo> findListByPage(@Param("osReport") OsReportVo osReportVo, @Param("startPage") int startPage, @Param("pageSize") int pageSize);
	
	int findCount(@Param("osReport") OsReportVo osReportVo);

	void saveByList(@Param("osReportVos") List<OsReportVo> osReportVos);
}