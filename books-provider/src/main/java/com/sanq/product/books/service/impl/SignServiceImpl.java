package com.sanq.product.books.service.impl;

import com.sanq.product.books.entity.vo.SignVo;
import com.sanq.product.books.mapper.SignMapper;
import com.sanq.product.books.service.SignService;
import com.sanq.product.config.utils.entity.Pager;
import com.sanq.product.config.utils.entity.Pagination;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


@Service("signService")
public class SignServiceImpl implements SignService {

	@Resource
	private SignMapper signMapper;
	
	@Override
	public int save(SignVo signVo) {
		return signMapper.save(signVo);
	}
	
	@Override
	public int delete(SignVo signVo) {
		return signMapper.delete(signVo);
	}	
	
	@Override
	public int update(SignVo signVo, Integer id) {
		SignVo oldSignVo = findById(id);
		
		if(null != oldSignVo && null != signVo) {
			if(null != signVo.getId()) {
				oldSignVo.setId(signVo.getId());
			}
			if(null != signVo.getUserId()) {
				oldSignVo.setUserId(signVo.getUserId());
			}
			if(null != signVo.getLastSignTime()) {
				oldSignVo.setLastSignTime(signVo.getLastSignTime());
			}
			if(null != signVo.getSignCount()) {
				oldSignVo.setSignCount(signVo.getSignCount());
			}
			return signMapper.update(oldSignVo);
		}
		return 0;
	}
	
	@Override
	public SignVo findById(Integer id) {
		return signMapper.findById(id);
	}
	
	@Override
	public List<SignVo> findList(SignVo signVo) {
		return signMapper.findList(signVo);
	}
	
	@Override
	public Pager<SignVo> findListByPage(SignVo signVo,Pagination pagination) {
		pagination.setTotalCount(findCount(signVo));
		
		List<SignVo> datas = signMapper.findListByPage(signVo,pagination.getStartPage(),pagination.getPageSize());
		
		return new Pager<SignVo>(pagination,datas);
	}
	
	@Override
	public int findCount(SignVo signVo) {
		return signMapper.findCount(signVo);
	}
	
	@Override
	public void saveByList(List<SignVo> signVos) {
		signMapper.saveByList(signVos);
	}

	@Override
	public int getSignByUserAndTime(SignVo signVo) {
		return signMapper.getSignByUserAndTime(signVo);
	}
}