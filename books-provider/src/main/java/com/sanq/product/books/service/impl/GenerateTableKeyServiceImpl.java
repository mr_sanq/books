package com.sanq.product.books.service.impl;


import com.sanq.product.books.mapper.GenymationKeyMapper;
import com.sanq.product.books.service.GenerateTableKeyService;
import com.sanq.product.config.utils.string.StringUtil;
import com.sanq.product.redis.service.JedisPoolService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by XieZhyan on 2018/11/10.
 */
@Service("generateTableKeyService")
public class GenerateTableKeyServiceImpl implements GenerateTableKeyService {

    @Resource
    private JedisPoolService jedisClusterService;
    @Resource
    private GenymationKeyMapper genymationKeyMapper;

    @Override
    public Integer getTableKey(String name) {
        int max = 0;
        StringBuffer sb = new StringBuffer(KEY);
        sb.append(":").append(name);
        if (!jedisClusterService.exists(sb.toString())) {
            Map<String, Object> param = new HashMap<>();
            param.put("tableName", name);
            Map<String, Object> map = genymationKeyMapper.getMaxId(param);
            max = StringUtil.toInteger(map.get("id"));
        }

        return StringUtil.toInteger(jedisClusterService.incr(sb.toString(), max));
    }
}
