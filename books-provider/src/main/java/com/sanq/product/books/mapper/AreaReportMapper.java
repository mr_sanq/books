package com.sanq.product.books.mapper;

import com.sanq.product.books.entity.vo.AreaReportVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface AreaReportMapper {
	
	int save(AreaReportVo areaReportVo);
	
	int delete(AreaReportVo areaReportVo);
	
	int update(AreaReportVo areaReportVo);
	
	AreaReportVo findById(Integer id);
	
	List<AreaReportVo> findList(@Param("areaReport") AreaReportVo areaReportVo);
	
	List<AreaReportVo> findListByPage(@Param("areaReport") AreaReportVo areaReportVo, @Param("startPage") int startPage, @Param("pageSize") int pageSize);
	
	int findCount(@Param("areaReport") AreaReportVo areaReportVo);

	void saveByList(@Param("areaReportVos") List<AreaReportVo> areaReportVos);
}