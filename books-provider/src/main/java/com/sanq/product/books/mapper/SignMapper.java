package com.sanq.product.books.mapper;

import com.sanq.product.books.entity.vo.SignVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SignMapper {
	
	int save(SignVo signVo);
	
	int delete(SignVo signVo);
	
	int update(SignVo signVo);
	
	SignVo findById(Integer id);
	
	List<SignVo> findList(@Param("sign")  SignVo signVo);
	
	List<SignVo> findListByPage(@Param("sign") SignVo signVo,@Param("startPage") int startPage, @Param("pageSize") int pageSize);
	
	int findCount(@Param("sign")  SignVo signVo);

	void saveByList(@Param("signVos") List<SignVo> signVos);

    int getSignByUserAndTime(@Param("sign") SignVo signVo);

}