package com.sanq.product.books.service.impl;

import com.sanq.product.books.config.Redis;
import com.sanq.product.books.entity.vo.AdvertiseVo;
import com.sanq.product.books.mapper.AdvertiseMapper;
import com.sanq.product.books.service.AdvertiseService;
import com.sanq.product.config.utils.entity.Pager;
import com.sanq.product.config.utils.entity.Pagination;
import com.sanq.product.config.utils.string.StringUtil;
import com.sanq.product.config.utils.web.JsonUtil;
import com.sanq.product.redis.service.JedisPoolService;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


@Service("advertiseService")
public class AdvertiseServiceImpl implements AdvertiseService {

	@Resource
	private AdvertiseMapper advertiseMapper;
	@Resource
	private JedisPoolService jedisPoolService;
	@Resource
	private ThreadPoolTaskExecutor taskExecutor;

	public void deleteRedis(String dataKey) {
		taskExecutor.execute(() -> {
			String key = Redis.ReplaceKey.getAdvertiseKey(dataKey);
			if ("*".equals(dataKey)) {
				jedisPoolService.deletes(key);
			} else jedisPoolService.delete(key);

		});
	}
	
	@Override
	public int save(AdvertiseVo advertiseVo) {
		int result = advertiseMapper.save(advertiseVo);
		if (result != 0)
			deleteRedis(advertiseVo.getDataKey());
		return result;
	}
	
	@Override
	public int delete(AdvertiseVo advertiseVo) {
		int delete = advertiseMapper.delete(advertiseVo);
		if (delete != 0)
			deleteRedis("*");
		return delete;
	}	
	
	@Override
	public int update(AdvertiseVo advertiseVo, Integer id) {
		AdvertiseVo oldAdvertiseVo = findById(id);
		
		if(null != oldAdvertiseVo && null != advertiseVo) {
			if(null != advertiseVo.getId()) {
				oldAdvertiseVo.setId(advertiseVo.getId());
			}
			if(null != advertiseVo.getDataKey()) {
				oldAdvertiseVo.setDataKey(advertiseVo.getDataKey());
			}
			if(null != advertiseVo.getAdvName()) {
				oldAdvertiseVo.setAdvName(advertiseVo.getAdvName());
			}
			if(null != advertiseVo.getAdvUrl()) {
				oldAdvertiseVo.setAdvUrl(advertiseVo.getAdvUrl());
			}
			if(null != advertiseVo.getAdvDescription()) {
				oldAdvertiseVo.setAdvDescription(advertiseVo.getAdvDescription());
			}
			if(null != advertiseVo.getCreateTime()) {
				oldAdvertiseVo.setCreateTime(advertiseVo.getCreateTime());
			}
			if(null != advertiseVo.getImgUrl()) {
				oldAdvertiseVo.setImgUrl(advertiseVo.getImgUrl());
			}
			if(!StringUtil.isEmpty(advertiseVo.getTarget())) {
				oldAdvertiseVo.setTarget(advertiseVo.getTarget());
			}
			if(null != advertiseVo.getCanInSite()) {
				oldAdvertiseVo.setCanInSite(advertiseVo.getCanInSite());
			}
			if (null != advertiseVo.getWeight()) {
				oldAdvertiseVo.setWeight(advertiseVo.getWeight());
			}
			if (null != advertiseVo.getAdvLoadType()) {
				oldAdvertiseVo.setAdvLoadType(advertiseVo.getAdvLoadType());
			}
			int update = advertiseMapper.update(oldAdvertiseVo);

			if (update != 0) {
				deleteRedis("*");
			}

			return update;
		}
		return 0;
	}
	
	@Override
	public AdvertiseVo findById(Integer id) {
		return advertiseMapper.findById(id);
	}
	
	@Override
	public List<AdvertiseVo> findList(AdvertiseVo advertiseVo) {
		String key = Redis.ReplaceKey.getAdvertiseKey(advertiseVo.getDataKey());
		String json = jedisPoolService.get(key);
		List<AdvertiseVo> list;
		if(StringUtil.isEmpty(json)) {
			list = advertiseMapper.findList(advertiseVo);
			if(list != null && !list.isEmpty()) {
				jedisPoolService.set(key, JsonUtil.obj2Json(list));
			}
		} else
			list = JsonUtil.json2ObjList(json, AdvertiseVo.class);

		return list;
	}
	
	@Override
	public Pager<AdvertiseVo> findListByPage(AdvertiseVo advertiseVo,Pagination pagination) {
		pagination.setTotalCount(findCount(advertiseVo));
		
		List<AdvertiseVo> datas = advertiseMapper.findListByPage(advertiseVo,pagination.getStartPage(),pagination.getPageSize());
		
		return new Pager<AdvertiseVo>(pagination,datas);
	}
	
	@Override
	public int findCount(AdvertiseVo advertiseVo) {
		return advertiseMapper.findCount(advertiseVo);
	}
	
	@Override
	public void saveByList(List<AdvertiseVo> advertiseVos) {
		advertiseMapper.saveByList(advertiseVos);
		deleteRedis("*");
	}
}