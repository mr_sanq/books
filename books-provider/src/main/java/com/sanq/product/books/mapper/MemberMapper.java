package com.sanq.product.books.mapper;

import com.sanq.product.books.entity.vo.MemberVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface MemberMapper {
	
	int save(MemberVo memberVo);
	
	int delete(MemberVo memberVo);
	
	int update(MemberVo memberVo);
	
	MemberVo findById(Integer id);
	
	List<MemberVo> findList(@Param("member") MemberVo memberVo);
	
	List<MemberVo> findListByPage(@Param("member") MemberVo memberVo, @Param("startPage") int startPage, @Param("pageSize") int pageSize);
	
	int findCount(@Param("member") MemberVo memberVo);

	void saveByList(@Param("memberVos") List<MemberVo> memberVos);

    MemberVo login(@Param("member") MemberVo memberVo);

}