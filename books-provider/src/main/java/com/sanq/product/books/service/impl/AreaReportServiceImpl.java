package com.sanq.product.books.service.impl;

import com.sanq.product.books.entity.vo.AreaReportVo;
import com.sanq.product.config.utils.entity.Pager;
import com.sanq.product.config.utils.entity.Pagination;
import com.sanq.product.books.service.AreaReportService;
import com.sanq.product.books.mapper.AreaReportMapper;
import java.util.List;
import java.math.BigDecimal;

import javax.annotation.Resource;
import org.springframework.stereotype.Service;


@Service("areaReportService")
public class AreaReportServiceImpl implements AreaReportService {

	@Resource
	private AreaReportMapper areaReportMapper;
	
	@Override
	public int save(AreaReportVo areaReportVo) {
		return areaReportMapper.save(areaReportVo);
	}
	
	@Override
	public int delete(AreaReportVo areaReportVo) {
		return areaReportMapper.delete(areaReportVo);
	}	
	
	@Override
	public int update(AreaReportVo areaReportVo, Integer id) {
		AreaReportVo oldAreaReportVo = findById(id);
		
		if(null != oldAreaReportVo && null != areaReportVo) {
			if(null != areaReportVo.getId()) {
				oldAreaReportVo.setId(areaReportVo.getId());
			}
			if(null != areaReportVo.getDay()) {
				oldAreaReportVo.setDay(areaReportVo.getDay());
			}
			if(null != areaReportVo.getViews()) {
				oldAreaReportVo.setViews(areaReportVo.getViews());
			}
			if(null != areaReportVo.getIpViews()) {
				oldAreaReportVo.setIpViews(areaReportVo.getIpViews());
			}
			if(null != areaReportVo.getCityName()) {
				oldAreaReportVo.setCityName(areaReportVo.getCityName());
			}
			if(null != areaReportVo.getProvName()) {
				oldAreaReportVo.setProvName(areaReportVo.getProvName());
			}
			if(null != areaReportVo.getCreateTime()) {
				oldAreaReportVo.setCreateTime(areaReportVo.getCreateTime());
			}
			return areaReportMapper.update(oldAreaReportVo);
		}
		return 0;
	}
	
	@Override
	public AreaReportVo findById(Integer id) {
		return areaReportMapper.findById(id);
	}
	
	@Override
	public List<AreaReportVo> findList(AreaReportVo areaReportVo) {
		return areaReportMapper.findList(areaReportVo);
	}
	
	@Override
	public Pager<AreaReportVo> findListByPage(AreaReportVo areaReportVo,Pagination pagination) {
		pagination.setTotalCount(findCount(areaReportVo));
		
		List<AreaReportVo> datas = areaReportMapper.findListByPage(areaReportVo,pagination.getStartPage(),pagination.getPageSize());
		
		return new Pager<AreaReportVo>(pagination,datas);
	}
	
	@Override
	public int findCount(AreaReportVo areaReportVo) {
		return areaReportMapper.findCount(areaReportVo);
	}
	
	@Override
	public void saveByList(List<AreaReportVo> areaReportVos) {
		areaReportMapper.saveByList(areaReportVos);
	}
}