package com.sanq.product.books.mapper;

import com.sanq.product.books.entity.vo.RolePermissionVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface RolePermissionMapper {
	
	int save(RolePermissionVo rolePermissionVo);
	
	int delete(RolePermissionVo rolePermissionVo);
	
	int update(RolePermissionVo rolePermissionVo);
	
	RolePermissionVo findById(Integer id);
	
	List<RolePermissionVo> findList(@Param("rolePermission") RolePermissionVo rolePermissionVo);
	
	List<RolePermissionVo> findListByPage(@Param("rolePermission") RolePermissionVo rolePermissionVo, @Param("startPage") int startPage, @Param("pageSize") int pageSize);
	
	int findCount(@Param("rolePermission") RolePermissionVo rolePermissionVo);

	void saveByList(@Param("rolePermissionVos") List<RolePermissionVo> rolePermissionVos);

    List<Integer> findPermissionIdByRoleId(Integer roleId);

}