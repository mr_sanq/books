package com.sanq.product.books.service.impl;

import com.sanq.product.books.entity.vo.HistoryVo;
import com.sanq.product.books.mapper.HistoryMapper;
import com.sanq.product.books.service.HistoryService;
import com.sanq.product.config.utils.entity.Pager;
import com.sanq.product.config.utils.entity.Pagination;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


@Service("historyService")
public class HistoryServiceImpl implements HistoryService {

	@Resource
	private HistoryMapper historyMapper;
	
	@Override
	public int save(HistoryVo historyVo) {
		return historyMapper.save(historyVo);
	}
	
	@Override
	public int delete(HistoryVo historyVo) {
		return historyMapper.delete(historyVo);
	}	
	
	@Override
	public int update(HistoryVo historyVo, Integer id) {
		HistoryVo oldHistoryVo = findById(id);
		
		if(null != oldHistoryVo && null != historyVo) {
			if(null != historyVo.getId()) {
				oldHistoryVo.setId(historyVo.getId());
			}
			if(null != historyVo.getUserId()) {
				oldHistoryVo.setUserId(historyVo.getUserId());
			}
			if(null != historyVo.getBookId()) {
				oldHistoryVo.setBookId(historyVo.getBookId());
			}
			if(null != historyVo.getReadDay()) {
				oldHistoryVo.setReadDay(historyVo.getReadDay());
			}
			if(null != historyVo.getReadChapterId()) {
				oldHistoryVo.setReadChapterId(historyVo.getReadChapterId());
			}
			if(null != historyVo.getCreateTime()) {
				oldHistoryVo.setCreateTime(historyVo.getCreateTime());
			}
			if (null != historyVo.getReadChapterCount()) {
				oldHistoryVo.setReadChapterCount(historyVo.getReadChapterCount());
			}
 			return historyMapper.update(oldHistoryVo);
		}
		return 0;
	}
	
	@Override
	public HistoryVo findById(Integer id) {
		return historyMapper.findById(id);
	}
	
	@Override
	public List<HistoryVo> findList(HistoryVo historyVo) {
		return historyMapper.findList(historyVo);
	}
	
	@Override
	public Pager<HistoryVo> findListByPage(HistoryVo historyVo,Pagination pagination) {
		pagination.setTotalCount(findCount(historyVo));
		
		List<HistoryVo> datas = historyMapper.findListByPage(historyVo,pagination.getStartPage(),pagination.getPageSize());
		
		return new Pager<HistoryVo>(pagination,datas);
	}
	
	@Override
	public int findCount(HistoryVo historyVo) {
		return historyMapper.findCount(historyVo);
	}
	
	@Override
	public void saveByList(List<HistoryVo> historyVos) {
		historyMapper.saveByList(historyVos);
	}
}