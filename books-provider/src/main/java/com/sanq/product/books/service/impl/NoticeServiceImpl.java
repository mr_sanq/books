package com.sanq.product.books.service.impl;

import com.sanq.product.books.entity.vo.NoticeVo;
import com.sanq.product.books.mapper.NoticeMapper;
import com.sanq.product.books.service.NoticeService;
import com.sanq.product.config.utils.entity.Pager;
import com.sanq.product.config.utils.entity.Pagination;
import com.sanq.product.config.utils.string.StringUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


@Service("noticeService")
public class NoticeServiceImpl implements NoticeService {

	@Resource
	private NoticeMapper noticeMapper;
	
	@Override
	public int save(NoticeVo noticeVo) {
		return noticeMapper.save(noticeVo);
	}
	
	@Override
	public int delete(NoticeVo noticeVo) {
		return noticeMapper.delete(noticeVo);
	}	
	
	@Override
	public int update(NoticeVo noticeVo, Integer id) {
		NoticeVo oldNoticeVo = findById(id);
		
		if(null != oldNoticeVo && null != noticeVo) {
			if(null != noticeVo.getId()) {
				oldNoticeVo.setId(noticeVo.getId());
			}
			if(null != noticeVo.getUserId()) {
				oldNoticeVo.setUserId(noticeVo.getUserId());
			}
			if(null != noticeVo.getNoticeTitle()) {
				oldNoticeVo.setNoticeTitle(noticeVo.getNoticeTitle());
			}
			if(null != noticeVo.getNoticeContent()) {
				oldNoticeVo.setNoticeContent(noticeVo.getNoticeContent());
			}
			if(null != noticeVo.getIsRead()) {
				oldNoticeVo.setIsRead(noticeVo.getIsRead());
			}
			if(null != noticeVo.getCreateTime()) {
				oldNoticeVo.setCreateTime(noticeVo.getCreateTime());
			}
			if(null != noticeVo.getReadTime()) {
				oldNoticeVo.setReadTime(noticeVo.getReadTime());
			}
			if (StringUtil.isEmpty(noticeVo.getNoticeType())) {
				oldNoticeVo.setNoticeType(noticeVo.getNoticeType());
			}
			return noticeMapper.update(oldNoticeVo);
		}
		return 0;
	}
	
	@Override
	public NoticeVo findById(Integer id) {
		return noticeMapper.findById(id);
	}
	
	@Override
	public List<NoticeVo> findList(NoticeVo noticeVo) {
		return noticeMapper.findList(noticeVo);
	}
	
	@Override
	public Pager<NoticeVo> findListByPage(NoticeVo noticeVo,Pagination pagination) {
		pagination.setTotalCount(findCount(noticeVo));
		
		List<NoticeVo> datas = noticeMapper.findListByPage(noticeVo,pagination.getStartPage(),pagination.getPageSize());
		
		return new Pager<NoticeVo>(pagination,datas);
	}
	
	@Override
	public int findCount(NoticeVo noticeVo) {
		return noticeMapper.findCount(noticeVo);
	}
	
	@Override
	public void saveByList(List<NoticeVo> noticeVos) {
		noticeMapper.saveByList(noticeVos);
	}
}