package com.sanq.product.books.mapper;

import com.sanq.product.books.entity.vo.HistoryVo;
import java.util.List;

import org.apache.ibatis.annotations.Param;

public interface HistoryMapper {
	
	int save(HistoryVo historyVo);
	
	int delete(HistoryVo historyVo);
	
	int update(HistoryVo historyVo);
	
	HistoryVo findById(Integer id);
	
	List<HistoryVo> findList(@Param("history") HistoryVo historyVo);
	
	List<HistoryVo> findListByPage(@Param("history") HistoryVo historyVo, @Param("startPage") int startPage, @Param("pageSize") int pageSize);
	
	int findCount(@Param("history") HistoryVo historyVo);

	void saveByList(@Param("historyVos") List<HistoryVo> historyVos);
}