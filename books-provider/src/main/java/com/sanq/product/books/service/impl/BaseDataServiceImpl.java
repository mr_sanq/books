package com.sanq.product.books.service.impl;

import com.sanq.product.books.entity.vo.BaseDataVo;
import com.sanq.product.books.mapper.BaseDataMapper;
import com.sanq.product.books.service.BaseDataService;
import com.sanq.product.config.utils.entity.Pager;
import com.sanq.product.config.utils.entity.Pagination;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


@Service("baseDataService")
public class BaseDataServiceImpl implements BaseDataService {

	@Resource
	private BaseDataMapper baseDataMapper;

	@Override
	public int save(BaseDataVo baseDataVo) {

		return baseDataMapper.save(baseDataVo);
	}
	
	@Override
	public int delete(BaseDataVo baseDataVo) {
		return baseDataMapper.delete(baseDataVo);
	}	
	
	@Override
	public int update(BaseDataVo baseDataVo, Integer id) {
		BaseDataVo oldBaseDataVo = findById(id);
		
		if(null != oldBaseDataVo && null != baseDataVo) {
			if(null != baseDataVo.getId()) {
				oldBaseDataVo.setId(baseDataVo.getId());
			}
			if(null != baseDataVo.getDataName()) {
				oldBaseDataVo.setDataName(baseDataVo.getDataName());
			}
			if(null != baseDataVo.getParentId()) {
				oldBaseDataVo.setParentId(baseDataVo.getParentId());
			}
			if(null != baseDataVo.getDataKey()) {
				oldBaseDataVo.setDataKey(baseDataVo.getDataKey());
			}
			if(null != baseDataVo.getDataDesc()) {
				oldBaseDataVo.setDataDesc(baseDataVo.getDataDesc());
			}
			if(null != baseDataVo.getCreateTime()) {
				oldBaseDataVo.setCreateTime(baseDataVo.getCreateTime());
			}
			return baseDataMapper.update(oldBaseDataVo);
		}
		return 0;
	}
	
	@Override
	public BaseDataVo findById(Integer id) {
		return baseDataMapper.findById(id);
	}
	
	@Override
	public List<BaseDataVo> findList(BaseDataVo baseDataVo) {
		return baseDataMapper.findList(baseDataVo);
	}
	
	@Override
	public Pager<BaseDataVo> findListByPage(BaseDataVo baseDataVo,Pagination pagination) {
		pagination.setTotalCount(findCount(baseDataVo));
		
		List<BaseDataVo> datas = baseDataMapper.findListByPage(baseDataVo,pagination.getStartPage(),pagination.getPageSize());
		
		return new Pager<BaseDataVo>(pagination,datas);
	}
	
	@Override
	public int findCount(BaseDataVo baseDataVo) {
		return baseDataMapper.findCount(baseDataVo);
	}
	
	@Override
	public void saveByList(List<BaseDataVo> baseDataVos) {
		baseDataMapper.saveByList(baseDataVos);
	}

}