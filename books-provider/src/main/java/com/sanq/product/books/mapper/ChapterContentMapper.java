package com.sanq.product.books.mapper;

import com.sanq.product.books.entity.vo.ChapterContentVo;
import java.util.List;

import org.apache.ibatis.annotations.Param;

public interface ChapterContentMapper {
	
	int save(ChapterContentVo chapterContentVo);
	
	int delete(ChapterContentVo chapterContentVo);
	
	int update(ChapterContentVo chapterContentVo);
	
	ChapterContentVo findById(Integer id);
	
	List<ChapterContentVo> findList(@Param("chapterContent") ChapterContentVo chapterContentVo);
	
	List<ChapterContentVo> findListByPage(@Param("chapterContent") ChapterContentVo chapterContentVo, @Param("startPage") int startPage, @Param("pageSize") int pageSize);
	
	int findCount(@Param("chapterContent") ChapterContentVo chapterContentVo);

	void saveByList(@Param("chapterContentVos") List<ChapterContentVo> chapterContentVos);
}