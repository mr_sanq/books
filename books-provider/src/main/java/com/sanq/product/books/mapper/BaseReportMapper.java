package com.sanq.product.books.mapper;

import com.sanq.product.books.entity.vo.BaseReportVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface BaseReportMapper {
	
	int save(BaseReportVo baseReportVo);
	
	int delete(BaseReportVo baseReportVo);
	
	int update(BaseReportVo baseReportVo);
	
	BaseReportVo findById(Integer id);
	
	List<BaseReportVo> findList(@Param("baseReport") BaseReportVo baseReportVo);
	
	List<BaseReportVo> findListByPage(@Param("baseReport") BaseReportVo baseReportVo, @Param("startPage") int startPage, @Param("pageSize") int pageSize);
	
	int findCount(@Param("baseReport") BaseReportVo baseReportVo);

	void saveByList(@Param("baseReportVos") List<BaseReportVo> baseReportVos);

    List<Map<String, Object>> findLineDataGroupTime(@Param("a") char a);
}