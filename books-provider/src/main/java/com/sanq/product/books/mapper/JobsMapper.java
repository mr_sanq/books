package com.sanq.product.books.mapper;

import com.sanq.product.books.entity.vo.JobsVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface JobsMapper {
	
	int save(JobsVo jobsVo);
	
	int delete(JobsVo jobsVo);
	
	int update(JobsVo jobsVo);
	
	JobsVo findById(Long id);
	
	List<JobsVo> findList(@Param("jobs") JobsVo jobsVo);
	
	List<JobsVo> findListByPage(@Param("jobs") JobsVo jobsVo, @Param("startPage") int startPage, @Param("pageSize") int pageSize);
	
	int findCount(@Param("jobs") JobsVo jobsVo);

	void saveByList(@Param("jobsVos") List<JobsVo> jobsVos);
}