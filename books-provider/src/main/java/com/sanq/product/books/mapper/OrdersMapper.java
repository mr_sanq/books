package com.sanq.product.books.mapper;

import com.sanq.product.books.entity.module.ServiceModule;
import com.sanq.product.books.entity.vo.OrdersVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface OrdersMapper {
	
	int save(OrdersVo ordersVo);
	
	int delete(OrdersVo ordersVo);
	
	int update(OrdersVo ordersVo);
	
	OrdersVo findById(Integer id);
	
	List<OrdersVo> findList(@Param("orders") OrdersVo ordersVo);
	
	List<OrdersVo> findListByPage(@Param("orders") OrdersVo ordersVo, @Param("startPage") int startPage, @Param("pageSize") int pageSize);
	
	int findCount(@Param("orders") OrdersVo ordersVo);

	void saveByList(@Param("ordersVos") List<OrdersVo> ordersVos);

    Map<String, Integer> findSummaryOrder(ServiceModule module);

    List<Map<String, Object>> findLineDataGroupTime();
    List<Map<String, Object>> findTotalMoneyLineGroupTime();


}