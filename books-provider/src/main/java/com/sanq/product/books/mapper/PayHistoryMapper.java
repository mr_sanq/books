package com.sanq.product.books.mapper;

import com.sanq.product.books.entity.vo.PayHistoryVo;
import java.util.List;

import org.apache.ibatis.annotations.Param;

public interface PayHistoryMapper {
	
	int save(PayHistoryVo payHistoryVo);
	
	int delete(PayHistoryVo payHistoryVo);
	
	int update(PayHistoryVo payHistoryVo);
	
	PayHistoryVo findById(Integer id);
	
	List<PayHistoryVo> findList(@Param("payHistory") PayHistoryVo payHistoryVo);
	
	List<PayHistoryVo> findListByPage(@Param("payHistory") PayHistoryVo payHistoryVo, @Param("startPage") int startPage, @Param("pageSize") int pageSize);
	
	int findCount(@Param("payHistory") PayHistoryVo payHistoryVo);

	void saveByList(@Param("payHistoryVos") List<PayHistoryVo> payHistoryVos);
}