package com.sanq.product.books.service.impl;

import com.sanq.product.books.entity.vo.MemberVo;
import com.sanq.product.books.mapper.MemberMapper;
import com.sanq.product.books.service.MemberService;
import com.sanq.product.config.utils.entity.Pager;
import com.sanq.product.config.utils.entity.Pagination;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


@Service("memberService")
public class MemberServiceImpl implements MemberService {

	@Resource
	private MemberMapper memberMapper;
	
	@Override
	public int save(MemberVo memberVo) {
		return memberMapper.save(memberVo);
	}
	
	@Override
	public int delete(MemberVo memberVo) {
		return memberMapper.delete(memberVo);
	}	
	
	@Override
	public int update(MemberVo memberVo, Integer id) {
		MemberVo oldMemberVo = findById(id);
		
		if(null != oldMemberVo && null != memberVo) {
			if(null != memberVo.getId()) {
				oldMemberVo.setId(memberVo.getId());
			}
			if(null != memberVo.getLoginName()) {
				oldMemberVo.setLoginName(memberVo.getLoginName());
			}
			if(null != memberVo.getLoginPwd()) {
				oldMemberVo.setLoginPwd(memberVo.getLoginPwd());
			}
			if(null != memberVo.getRealName()) {
				oldMemberVo.setRealName(memberVo.getRealName());
			}
			if(null != memberVo.getEmail()) {
				oldMemberVo.setEmail(memberVo.getEmail());
			}
			if(null != memberVo.getTel()) {
				oldMemberVo.setTel(memberVo.getTel());
			}
			if(null != memberVo.getCreateTime()) {
				oldMemberVo.setCreateTime(memberVo.getCreateTime());
			}
			if(null != memberVo.getUserStatus()) {
				oldMemberVo.setUserStatus(memberVo.getUserStatus());
			}
			if(null != memberVo.getUserType()) {
				oldMemberVo.setUserType(memberVo.getUserType());
			}
			if(null != memberVo.getCover()) {
				oldMemberVo.setCover(memberVo.getCover());
			}
			return memberMapper.update(oldMemberVo);
		}
		return 0;
	}
	
	@Override
	public MemberVo findById(Integer id) {
		return memberMapper.findById(id);
	}
	
	@Override
	public List<MemberVo> findList(MemberVo memberVo) {
		return memberMapper.findList(memberVo);
	}
	
	@Override
	public Pager<MemberVo> findListByPage(MemberVo memberVo,Pagination pagination) {
		pagination.setTotalCount(findCount(memberVo));
		
		List<MemberVo> datas = memberMapper.findListByPage(memberVo,pagination.getStartPage(),pagination.getPageSize());
		
		return new Pager<MemberVo>(pagination,datas);
	}
	
	@Override
	public int findCount(MemberVo memberVo) {
		return memberMapper.findCount(memberVo);
	}
	
	@Override
	public void saveByList(List<MemberVo> memberVos) {
		memberMapper.saveByList(memberVos);
	}

	@Override
	public MemberVo login(MemberVo memberVo) {
		return memberMapper.login(memberVo);
	}
}