package com.sanq.product.books.mapper;

import com.sanq.product.books.entity.vo.SortJoinUsersVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SortJoinUsersMapper {
	
	int save(SortJoinUsersVo sortJoinUsersVo);
	
	int delete(SortJoinUsersVo sortJoinUsersVo);
	
	int update(SortJoinUsersVo sortJoinUsersVo);
	
	SortJoinUsersVo findById(Integer id);
	
	List<SortJoinUsersVo> findList(@Param("sortJoinUsers") SortJoinUsersVo sortJoinUsersVo);
	
	List<SortJoinUsersVo> findListByPage(@Param("sortJoinUsers") SortJoinUsersVo sortJoinUsersVo, @Param("startPage") int startPage, @Param("pageSize") int pageSize);
	
	int findCount(@Param("sortJoinUsers") SortJoinUsersVo sortJoinUsersVo);

	void saveByList(@Param("sortJoinUsersVos") List<SortJoinUsersVo> sortJoinUsersVos);
}