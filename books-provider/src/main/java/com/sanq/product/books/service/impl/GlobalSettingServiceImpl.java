package com.sanq.product.books.service.impl;

import com.sanq.product.books.config.Redis;
import com.sanq.product.books.entity.vo.GlobalSettingVo;
import com.sanq.product.books.mapper.GlobalSettingMapper;
import com.sanq.product.books.service.GlobalSettingService;
import com.sanq.product.config.utils.entity.Pager;
import com.sanq.product.config.utils.entity.Pagination;
import com.sanq.product.config.utils.string.StringUtil;
import com.sanq.product.config.utils.web.JsonUtil;
import com.sanq.product.redis.service.JedisPoolService;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.List;


@Service("globalSettingService")
public class GlobalSettingServiceImpl implements GlobalSettingService {

	@Resource
	private GlobalSettingMapper globalSettingMapper;
	@Resource
	private JedisPoolService jedisPoolService;
	@Resource
	private ThreadPoolTaskExecutor taskExecutor;

	public void deleteByRedis(String type) {
		taskExecutor.execute(() -> {

			String key = Redis.ReplaceKey.getSettingKey(type);

			if ("*".equals(type)) {
				jedisPoolService.deletes(key);
			} else jedisPoolService.delete(key);
		});

	}
	
	@Override
	public int save(GlobalSettingVo globalSettingVo) {
		int save = globalSettingMapper.save(globalSettingVo);
		if (save != 0) {
			deleteByRedis(globalSettingVo.getSettingType());
		}
		return save;
	}
	
	@Override
	public int delete(GlobalSettingVo globalSettingVo) {
		int delete = globalSettingMapper.delete(globalSettingVo);
		if (delete != 0) {
			deleteByRedis("*");
		}
		return delete;
	}	
	
	@Override
	public int update(GlobalSettingVo globalSettingVo, Integer id) {
		GlobalSettingVo oldGlobalSettingVo = findById(id);
		
		if(null != oldGlobalSettingVo && null != globalSettingVo) {
			if(null != globalSettingVo.getId()) {
				oldGlobalSettingVo.setId(globalSettingVo.getId());
			}
			if(null != globalSettingVo.getSettingType()) {
				oldGlobalSettingVo.setSettingType(globalSettingVo.getSettingType());
			}
			if(null != globalSettingVo.getSettingContent()) {
				oldGlobalSettingVo.setSettingContent(globalSettingVo.getSettingContent());
			}
			if(null != globalSettingVo.getCreateTime()) {
				oldGlobalSettingVo.setCreateTime(globalSettingVo.getCreateTime());
			}
			int update = globalSettingMapper.update(oldGlobalSettingVo);
			if (update != 0) {
				deleteByRedis(oldGlobalSettingVo.getSettingType());
			}

			return update;
		}
		return 0;
	}
	
	@Override
	public GlobalSettingVo findById(Integer id) {
		return globalSettingMapper.findById(id);
	}
	
	@Override
	public List<GlobalSettingVo> findList(GlobalSettingVo globalSettingVo) {
		String key = Redis.ReplaceKey.getSettingKey(globalSettingVo.getSettingType());

		String json = jedisPoolService.get(key);

		List<GlobalSettingVo> list;
		if (StringUtil.isEmpty(json)) {

			list = globalSettingMapper.findList(globalSettingVo);
			if (list != null && !list.isEmpty()) {
				jedisPoolService.set(key, JsonUtil.obj2Json(list));
			}

		} else list = JsonUtil.json2ObjList(json, GlobalSettingVo.class);

		return list;
	}
	
	@Override
	public Pager<GlobalSettingVo> findListByPage(GlobalSettingVo globalSettingVo,Pagination pagination) {
		pagination.setTotalCount(findCount(globalSettingVo));
		
		List<GlobalSettingVo> datas = globalSettingMapper.findListByPage(globalSettingVo,pagination.getStartPage(),pagination.getPageSize());
		
		return new Pager<GlobalSettingVo>(pagination,datas);
	}
	
	@Override
	public int findCount(GlobalSettingVo globalSettingVo) {
		return globalSettingMapper.findCount(globalSettingVo);
	}
	
	@Override
	public void saveByList(List<GlobalSettingVo> globalSettingVos) {
		globalSettingMapper.saveByList(globalSettingVos);
		deleteByRedis("*");
	}
}