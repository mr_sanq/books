package com.sanq.product.books.service.impl;

import com.sanq.product.books.config.Redis;
import com.sanq.product.books.entity.vo.MemberVo;
import com.sanq.product.books.entity.vo.PermissionVo;
import com.sanq.product.books.mapper.PermissionMapper;
import com.sanq.product.books.service.PermissionService;
import com.sanq.product.config.utils.entity.Pager;
import com.sanq.product.config.utils.entity.Pagination;
import com.sanq.product.config.utils.string.StringUtil;
import com.sanq.product.config.utils.web.JsonUtil;
import com.sanq.product.redis.service.JedisPoolService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


@Service("permissionService")
public class PermissionServiceImpl implements PermissionService {

	@Resource
	private PermissionMapper permissionMapper;
	@Resource
	private JedisPoolService jedisPoolService;
	
	@Override
	public int save(PermissionVo permissionVo) {
		return permissionMapper.save(permissionVo);
	}
	
	@Override
	public int delete(PermissionVo permissionVo) {
		return permissionMapper.delete(permissionVo);
	}	
	
	@Override
	public int update(PermissionVo permissionVo, Integer id) {
		PermissionVo oldPermissionVo = findById(id);
		
		if(null != oldPermissionVo && null != permissionVo) {
			if(null != permissionVo.getId()) {
				oldPermissionVo.setId(permissionVo.getId());
			}
			if(null != permissionVo.getParentId()) {
				oldPermissionVo.setParentId(permissionVo.getParentId());
			}
			if(null != permissionVo.getPermissionType()) {
				oldPermissionVo.setPermissionType(permissionVo.getPermissionType());
			}
			if(null != permissionVo.getPermissionName()) {
				oldPermissionVo.setPermissionName(permissionVo.getPermissionName());
			}
			if(null != permissionVo.getPermissionUrl()) {
				oldPermissionVo.setPermissionUrl(permissionVo.getPermissionUrl());
			}
			if(null != permissionVo.getIsHome()) {
				oldPermissionVo.setIsHome(permissionVo.getIsHome());
			}
			if(null != permissionVo.getOrderNum()) {
				oldPermissionVo.setOrderNum(permissionVo.getOrderNum());
			}
			if(null != permissionVo.getCreateTime()) {
				oldPermissionVo.setCreateTime(permissionVo.getCreateTime());
			}
			if (null != permissionVo.getIcon()) {
				oldPermissionVo.setIcon(permissionVo.getIcon());
			}
			return permissionMapper.update(oldPermissionVo);
		}
		return 0;
	}
	
	@Override
	public PermissionVo findById(Integer id) {
		return permissionMapper.findById(id);
	}
	
	@Override
	public List<PermissionVo> findList(PermissionVo permissionVo) {
		return permissionMapper.findList(permissionVo);
	}
	
	@Override
	public Pager<PermissionVo> findListByPage(PermissionVo permissionVo, Pagination pagination) {
		pagination.setTotalCount(findCount(permissionVo));
		
		List<PermissionVo> datas = permissionMapper.findListByPage(permissionVo,pagination.getStartPage(),pagination.getPageSize());
		
		return new Pager<PermissionVo>(pagination,datas);
	}
	
	@Override
	public int findCount(PermissionVo permissionVo) {
		return permissionMapper.findCount(permissionVo);
	}
	
	@Override
	public void saveByList(List<PermissionVo> permissionVos) {
		permissionMapper.saveByList(permissionVos);
	}

	@Override
	public List<PermissionVo> getPermissionByParentList(Integer parentId) {
		return permissionMapper.getPermissionByParentList(parentId);
	}

	@Override
	public List<PermissionVo> findPermissionByMemberId(Integer parentId, MemberVo memberVo) {

		String key = Redis.ReplaceKey.getPermissionListKey(memberVo.getToken());

		String json = jedisPoolService.get(key);

		List<PermissionVo> list;
		if (StringUtil.isEmpty(json)) {
			List<PermissionVo> voList = permissionMapper.findPermissionByMemberId(parentId, memberVo.getId());
			list = findChildList(voList, memberVo.getId());

			if (list != null && !list.isEmpty())
				jedisPoolService.set(key, JsonUtil.obj2Json(list));
		} else
			list = JsonUtil.json2ObjList(json, PermissionVo.class);

		return list;
	}

	private final List<PermissionVo> findChildList(List<PermissionVo> list, Integer memberId) {
		if(list == null || list.isEmpty())
			return list;

		for(PermissionVo permissionVo : list) {
			permissionVo.setChilds(findChildList(permissionMapper.findPermissionByMemberId(permissionVo.getId(), memberId), memberId));
		}

		return list;
	}
}