package com.sanq.product.books.mapper;

import com.sanq.product.books.entity.vo.BaseDataVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface BaseDataMapper {
	
	int save(BaseDataVo baseDataVo);
	
	int delete(BaseDataVo baseDataVo);
	
	int update(BaseDataVo baseDataVo);
	
	BaseDataVo findById(Integer id);
	
	List<BaseDataVo> findList(@Param("baseData") BaseDataVo baseDataVo);
	
	List<BaseDataVo> findListByPage(@Param("baseData") BaseDataVo baseDataVo, @Param("startPage") int startPage, @Param("pageSize") int pageSize);
	
	int findCount(@Param("baseData") BaseDataVo baseDataVo);

	void saveByList(@Param("baseDataVos") List<BaseDataVo> baseDataVos);

}