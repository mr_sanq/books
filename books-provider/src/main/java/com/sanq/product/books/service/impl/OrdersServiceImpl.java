package com.sanq.product.books.service.impl;

import com.sanq.product.books.entity.module.ServiceModule;
import com.sanq.product.books.entity.vo.OrdersVo;
import com.sanq.product.books.mapper.OrdersMapper;
import com.sanq.product.books.service.OrdersService;
import com.sanq.product.config.utils.entity.Pager;
import com.sanq.product.config.utils.entity.Pagination;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;


@Service("ordersService")
public class OrdersServiceImpl implements OrdersService {

	@Resource
	private OrdersMapper ordersMapper;
	
	@Override
	public int save(OrdersVo ordersVo) {
		return ordersMapper.save(ordersVo);
	}
	
	@Override
	public int delete(OrdersVo ordersVo) {
		return ordersMapper.delete(ordersVo);
	}	
	
	@Override
	public int update(OrdersVo ordersVo, Integer id) {
		OrdersVo oldOrdersVo = findById(id);
		
		if(null != oldOrdersVo && null != ordersVo) {
			if(null != ordersVo.getId()) {
				oldOrdersVo.setId(ordersVo.getId());
			}
			if(null != ordersVo.getUserId()) {
				oldOrdersVo.setUserId(ordersVo.getUserId());
			}
			if(null != ordersVo.getComboTitle()) {
				oldOrdersVo.setComboTitle(ordersVo.getComboTitle());
			}
			if(null != ordersVo.getComboPrice()) {
				oldOrdersVo.setComboPrice(ordersVo.getComboPrice());
			}
			if(null != ordersVo.getOrderStatus()) {
				oldOrdersVo.setOrderStatus(ordersVo.getOrderStatus());
			}
			if(null != ordersVo.getOrderCreateTime()) {
				oldOrdersVo.setOrderCreateTime(ordersVo.getOrderCreateTime());
			}
			if(null != ordersVo.getOrderNum()) {
				oldOrdersVo.setOrderNum(ordersVo.getOrderNum());
			}
			if(null != ordersVo.getPayTime()) {
				oldOrdersVo.setPayTime(ordersVo.getPayTime());
			}
			if(null != ordersVo.getGlobVipCount()) {
				oldOrdersVo.setGlobVipCount(ordersVo.getGlobVipCount());
			}
			if(null != ordersVo.getPayType()) {
				oldOrdersVo.setPayType(ordersVo.getPayType());
			}
			if(null != ordersVo.getPayOrderNum()) {
				oldOrdersVo.setPayOrderNum(ordersVo.getPayOrderNum());
			}
			if(null != ordersVo.getOrderType()) {
				oldOrdersVo.setOrderType(ordersVo.getOrderType());
			}
			return ordersMapper.update(oldOrdersVo);
		}
		return 0;
	}
	
	@Override
	public OrdersVo findById(Integer id) {
		return ordersMapper.findById(id);
	}
	
	@Override
	public List<OrdersVo> findList(OrdersVo ordersVo) {
		return ordersMapper.findList(ordersVo);
	}
	
	@Override
	public Pager<OrdersVo> findListByPage(OrdersVo ordersVo,Pagination pagination) {
		pagination.setTotalCount(findCount(ordersVo));
		
		List<OrdersVo> datas = ordersMapper.findListByPage(ordersVo,pagination.getStartPage(),pagination.getPageSize());
		
		return new Pager<OrdersVo>(pagination,datas);
	}
	
	@Override
	public int findCount(OrdersVo ordersVo) {
		return ordersMapper.findCount(ordersVo);
	}
	
	@Override
	public void saveByList(List<OrdersVo> ordersVos) {
		ordersMapper.saveByList(ordersVos);
	}

	@Override
	public Map<String, Integer> findSummaryOrder(ServiceModule module) {
		return ordersMapper.findSummaryOrder(module);
	}

	@Override
	public List<Map<String, Object>> findLineDataGroupTime() {
		return ordersMapper.findLineDataGroupTime();
	}

	/**
	 * 查询前10条记录 新增金额折线图
	 *
	 * @return
	 */
	@Override
	public List<Map<String, Object>> findTotalMoneyLineGroupTime() {
		return ordersMapper.findTotalMoneyLineGroupTime();
	}
}