package com.sanq.product.books.service.impl;

import com.sanq.product.books.config.Redis;
import com.sanq.product.books.entity.vo.BlockJoinBooksVo;
import com.sanq.product.books.mapper.BlockJoinBooksMapper;
import com.sanq.product.books.service.BlockJoinBooksService;
import com.sanq.product.config.utils.entity.Pager;
import com.sanq.product.config.utils.entity.Pagination;
import com.sanq.product.redis.service.JedisPoolService;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


@Service("blockJoinBooksService")
public class BlockJoinBooksServiceImpl implements BlockJoinBooksService {

	@Resource
	private BlockJoinBooksMapper blockJoinBooksMapper;
	@Resource
	private JedisPoolService jedisPoolService;
	@Resource
	private ThreadPoolTaskExecutor taskExecutor;

	private void deleteRedis(String blockId) {
		taskExecutor.execute(() -> {
			String totalKey = Redis.ReplaceKey.getBooksListBlockPage(blockId, "-1");

			if ("*".equals(blockId)) {
				jedisPoolService.deletes(totalKey);
			} else jedisPoolService.delete(totalKey);
		});
	}
	
	@Override
	public int save(BlockJoinBooksVo blockJoinBooksVo) {
		int result = blockJoinBooksMapper.save(blockJoinBooksVo);
		if (result != 0)
			deleteRedis(blockJoinBooksVo.getBlockId() + "");

		return result;
	}
	
	@Override
	public int delete(BlockJoinBooksVo blockJoinBooksVo) {
		int delete = blockJoinBooksMapper.delete(blockJoinBooksVo);
		if (delete != 0) {
			deleteRedis("*");
		}
		return delete;
	}	
	
	@Override
	public int update(BlockJoinBooksVo blockJoinBooksVo, Integer id) {
		BlockJoinBooksVo oldBlockJoinBooksVo = findById(id);
		
		if(null != oldBlockJoinBooksVo && null != blockJoinBooksVo) {
			if(null != blockJoinBooksVo.getId()) {
				oldBlockJoinBooksVo.setId(blockJoinBooksVo.getId());
			}
			if(null != blockJoinBooksVo.getBlockId()) {
				oldBlockJoinBooksVo.setBlockId(blockJoinBooksVo.getBlockId());
			}
			if(null != blockJoinBooksVo.getBookId()) {
				oldBlockJoinBooksVo.setBookId(blockJoinBooksVo.getBookId());
			}
			if(null != blockJoinBooksVo.getCreateTime()) {
				oldBlockJoinBooksVo.setCreateTime(blockJoinBooksVo.getCreateTime());
			}
			int update = blockJoinBooksMapper.update(oldBlockJoinBooksVo);

			if (update != 0)
				deleteRedis(oldBlockJoinBooksVo.getBlockId() + "");

			return update;
		}
		return 0;
	}
	
	@Override
	public BlockJoinBooksVo findById(Integer id) {
		return blockJoinBooksMapper.findById(id);
	}
	
	@Override
	public List<BlockJoinBooksVo> findList(BlockJoinBooksVo blockJoinBooksVo) {
		return blockJoinBooksMapper.findList(blockJoinBooksVo);
	}
	
	@Override
	public Pager<BlockJoinBooksVo> findListByPage(BlockJoinBooksVo blockJoinBooksVo,Pagination pagination) {
		pagination.setTotalCount(findCount(blockJoinBooksVo));
		
		List<BlockJoinBooksVo> datas = blockJoinBooksMapper.findListByPage(blockJoinBooksVo,pagination.getStartPage(),pagination.getPageSize());
		
		return new Pager<BlockJoinBooksVo>(pagination,datas);
	}
	
	@Override
	public int findCount(BlockJoinBooksVo blockJoinBooksVo) {
		return blockJoinBooksMapper.findCount(blockJoinBooksVo);
	}
	
	@Override
	public void saveByList(List<BlockJoinBooksVo> blockJoinBooksVos) {
		blockJoinBooksMapper.saveByList(blockJoinBooksVos);
		deleteRedis("*");
	}

	@Override
	public int saveRange(Integer blockId) {
		int result = blockJoinBooksMapper.saveRange(blockId);
		if (result != -1)
			deleteRedis(blockId + "");
		return result;
	}
}