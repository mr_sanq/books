package com.sanq.product.books.mapper;

import java.util.Map;

public interface AreaMapper {

	Map<String, String> getCityNameByIp(String country);

}