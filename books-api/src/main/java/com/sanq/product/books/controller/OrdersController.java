package com.sanq.product.books.controller;

import com.sanq.product.books.config.Redis;
import com.sanq.product.books.entity.vo.OrdersVo;
import com.sanq.product.books.entity.vo.ProductVo;
import com.sanq.product.books.entity.vo.UsersVo;
import com.sanq.product.books.service.OrdersService;
import com.sanq.product.books.service.ProductService;
import com.sanq.product.books.service.UsersService;
import com.sanq.product.config.utils.annotation.LogAnnotation;
import com.sanq.product.config.utils.entity.Pager;
import com.sanq.product.config.utils.entity.Pagination;
import com.sanq.product.config.utils.entity.Response;
import com.sanq.product.config.utils.string.StringUtil;
import com.sanq.product.config.utils.web.JsonUtil;
import com.sanq.product.redis.service.JedisPoolService;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/app/orders")
public class OrdersController {

	@Resource
	private OrdersService ordersService;
	@Resource
	private JedisPoolService jedisPoolService;
	@Resource
	private ProductService productService;
	@Resource
	private ThreadPoolTaskExecutor taskExecutor;
	@Resource
	private UsersService usersService;


	@LogAnnotation(description = "分页查询数据")
	@GetMapping(value="/list")
	public Response findListByPager(HttpServletRequest request, OrdersVo ordersVo, Pagination pagination) {

		UsersVo usersVo = JsonUtil.json2Obj(jedisPoolService.get(Redis.ReplaceKey.getTokenUser(ordersVo.getToken())), UsersVo.class);
		ordersVo.setUserId(usersVo.getId());
		Pager<OrdersVo> pager = ordersService.findListByPage(ordersVo, pagination);

		return pager != null ? new Response().success(pager) : new Response().failure();
	}

	@LogAnnotation(description = "添加数据")
	@PostMapping(value="/save")
	public Response add(HttpServletRequest request, @RequestBody OrdersVo ordersVo) {
		String token = Redis.ReplaceKey.getTokenUser(ordersVo.getToken());
		UsersVo usersVo = JsonUtil.json2Obj(jedisPoolService.get(token), UsersVo.class);

		ProductVo productVo = productService.findById(ordersVo.getProductId());

		if (productVo == null)
			return new Response().failure("充值产品不能为空");


		ordersVo.setUserId(usersVo.getId());
		ordersVo.setComboTitle(productVo.getProductName());
		ordersVo.setComboPrice(productVo.getPrice());
		ordersVo.setOrderNum(StringUtil.uuid());
		ordersVo.setGlobVipCount(productVo.getObtain().add(productVo.getExtraObtain()));
		ordersVo.setOrderType(productVo.getProductType());

		int result = ordersService.save(ordersVo);

		if (result != 0) {
			//TODO 假装已经支付完成， 给用户充值
			taskExecutor.execute(() -> {
				usersVo.setGold(ordersVo.getGlobVipCount().add(usersVo.getGold()));
				int update = usersService.update(usersVo, usersVo.getId());

				if (update != 0) {
					jedisPoolService.set(token, JsonUtil.obj2Json(usersVo));
				}
			});
		}

		return result == 1 ? new Response().success() : new Response().failure();
	}
}