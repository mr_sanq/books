package com.sanq.product.books.controller;

import com.sanq.product.books.entity.vo.ProductVo;
import com.sanq.product.books.service.ProductService;
import com.sanq.product.config.utils.annotation.LogAnnotation;
import com.sanq.product.config.utils.entity.Response;
import com.sanq.product.security.annotation.IgnoreSecurity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/app/product")
public class ProductController {

	@Resource
	private ProductService productService;

	@IgnoreSecurity
	@LogAnnotation(description = "查询所有数据")
	@GetMapping(value="/all")
	public Response findList(HttpServletRequest request, ProductVo productVo) {

		List<ProductVo> list = productService.findList(productVo);
		return list != null ? new Response().success(list) : new Response().failure();
	}
}