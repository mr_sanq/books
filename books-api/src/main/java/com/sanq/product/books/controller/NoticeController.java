package com.sanq.product.books.controller;

import com.sanq.product.books.config.Redis;
import com.sanq.product.books.entity.vo.NoticeVo;
import com.sanq.product.books.entity.vo.UsersVo;
import com.sanq.product.books.service.NoticeService;
import com.sanq.product.books.utils.WebUtil;
import com.sanq.product.config.utils.annotation.LogAnnotation;
import com.sanq.product.config.utils.entity.Pager;
import com.sanq.product.config.utils.entity.Pagination;
import com.sanq.product.config.utils.entity.Response;
import com.sanq.product.config.utils.web.JsonUtil;
import com.sanq.product.redis.service.JedisPoolService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/app/notice")
public class NoticeController {

	@Resource
	private NoticeService noticeService;
	@Resource
	private JedisPoolService jedisPoolService;


	@LogAnnotation(description = "分页查询数据")
	@GetMapping(value="/list")
	public Response findListByPager(HttpServletRequest request, NoticeVo noticeVo, Pagination pagination) {

		UsersVo usersVo = JsonUtil.json2Obj(jedisPoolService.get(Redis.ReplaceKey.getTokenUser(WebUtil.getToken(request))), UsersVo.class);

		if ("ALL".equals(noticeVo.getIsRead())) {
			noticeVo.setIsRead(null);
		}

		noticeVo.setUserId(usersVo.getId());
		Pager<NoticeVo> pager = noticeService.findListByPage(noticeVo, pagination);

		return pager != null ? new Response().success(pager) : new Response().failure();
	}

	@LogAnnotation(description = "通过ID修改数据")
	@PutMapping(value="/update/{id}")
	public Response updateByKey(HttpServletRequest request,
								@RequestBody NoticeVo noticeVo,
								@PathVariable("id") Integer id) {

		int result = noticeService.update(noticeVo, id);

		return result == 1 ? new Response().success() : new Response().failure();
	}
}