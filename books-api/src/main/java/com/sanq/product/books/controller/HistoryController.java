package com.sanq.product.books.controller;

import com.sanq.product.books.config.Redis;
import com.sanq.product.books.entity.vo.BooksShelfVo;
import com.sanq.product.books.entity.vo.HistoryVo;
import com.sanq.product.books.entity.vo.UsersVo;
import com.sanq.product.books.service.BooksShelfService;
import com.sanq.product.books.service.HistoryService;
import com.sanq.product.config.utils.annotation.LogAnnotation;
import com.sanq.product.config.utils.entity.Pager;
import com.sanq.product.config.utils.entity.Pagination;
import com.sanq.product.config.utils.entity.Response;
import com.sanq.product.config.utils.string.StringUtil;
import com.sanq.product.config.utils.web.JsonUtil;
import com.sanq.product.redis.service.JedisPoolService;
import com.sanq.product.security.annotation.IgnoreSecurity;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/app/history")
public class HistoryController {

	@Resource
	private HistoryService historyService;
	@Resource
	private JedisPoolService jedisPoolService;
	@Resource
	private ThreadPoolTaskExecutor taskExecutor;
	@Resource
	private BooksShelfService booksShelfService;

	@LogAnnotation(description = "删除数据")
	@DeleteMapping(value="/delete")
	public Response deleteById(HttpServletRequest request, @RequestBody HistoryVo historyVo) {

		int result = historyService.delete(historyVo);
		return result == 1 ? new Response().success() : new Response().failure();
	}

	@LogAnnotation(description = "分页查询数据")
	@GetMapping(value="/list")
	public Response findListByPager(HttpServletRequest request, HistoryVo historyVo, Pagination pagination) {

		Pager<HistoryVo> pager = historyService.findListByPage(historyVo, pagination);

		return pager != null ? new Response().success(pager) : new Response().failure();
	}

	@IgnoreSecurity
	@LogAnnotation(description = "添加数据")
	@PostMapping(value="/save")
	public Response add(HttpServletRequest request, @RequestBody final HistoryVo historyVo) {
		if (StringUtil.isEmpty(historyVo.getToken()))
			return new Response().failure();

		final UsersVo usersVo = JsonUtil.json2Obj(jedisPoolService.get(Redis.ReplaceKey.getTokenUser(historyVo.getToken())), UsersVo.class);

		if (usersVo == null)
			return new Response().failure();

		taskExecutor.execute(() -> {
			BooksShelfVo shelfVo = new BooksShelfVo();
			shelfVo.setUserId(usersVo.getId());
			shelfVo.setBookId(historyVo.getBookId());

			int count = booksShelfService.findCount(shelfVo);

			if (count == 0)
				booksShelfService.save(shelfVo);
		});

		historyVo.setUserId(usersVo.getId());

		int result = historyService.save(historyVo);

		return result != 0 ? new Response().success() : new Response().failure();
	}

	@LogAnnotation(description = "通过ID修改数据")
	@PutMapping(value="/update/{id}")
	public Response updateByKey(HttpServletRequest request,
        @RequestBody HistoryVo historyVo,
        @PathVariable("id") Integer id) {

		int result = historyService.update(historyVo, id);

		return result == 1 ? new Response().success() : new Response().failure();
	}
}