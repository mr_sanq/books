package com.sanq.product.books.controller;

import com.sanq.product.books.entity.vo.SortVo;
import com.sanq.product.books.service.SortService;
import com.sanq.product.config.utils.annotation.LogAnnotation;
import com.sanq.product.config.utils.entity.Response;
import com.sanq.product.security.annotation.IgnoreSecurity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/app/sort")
public class SortController {

	@Resource
	private SortService sortService;

	@IgnoreSecurity
	@LogAnnotation(description = "通过ID得到详情")
	@GetMapping(value="/get/{id}")
	public Response getById(HttpServletRequest request, @PathVariable("id") Integer id) {
		SortVo sortVo = sortService.findById(id);

		return sortVo != null ? new Response().success(sortVo) : new Response().failure();
	}

	@IgnoreSecurity
	@LogAnnotation(description = "查询所有数据")
	@GetMapping(value="/all")
	public Response findList(HttpServletRequest request, SortVo sortVo) {

		List<SortVo> list = sortService.findList(sortVo);
		return list != null ? new Response().success(list) : new Response().failure();
	}

}