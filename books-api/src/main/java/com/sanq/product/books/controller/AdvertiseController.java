package com.sanq.product.books.controller;

import com.sanq.product.books.entity.vo.AdvertiseVo;
import com.sanq.product.books.service.AdvertiseService;
import com.sanq.product.config.utils.annotation.LogAnnotation;
import com.sanq.product.config.utils.entity.Response;
import com.sanq.product.security.annotation.IgnoreSecurity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/app/advertise")
public class AdvertiseController {

	@Resource
	private AdvertiseService advertiseService;

	@IgnoreSecurity
	@LogAnnotation(description = "查询所有数据")
	@GetMapping(value="/all")
	public Response findList(HttpServletRequest request, AdvertiseVo advertiseVo) {

		List<AdvertiseVo> list = advertiseService.findList(advertiseVo);
		return list != null ? new Response().success(list) : new Response().failure();
	}

}