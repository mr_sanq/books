# 享阅读

## 项目背景
1. 本意是想通过该系统对多年所学技能中和学习， 达到融汇贯通的地步。于是出现了该小说系统 

2. 针对该架构拿来即用， 快速开发其他系统

## 项目介绍
享阅读是一款基于SpringMVC + MyBatis实现的小说系统， 目前只有H5端。 前端采用vue的框架cube-ui, 采用前后端分离的结构，系统中针对数据接口安全做了一定的限制， 很大程度上保证了数据的安全性。

## 技术架构
- 核心框架： SpringMVC + MyBatis + Spring
- 数据库： MySQL
- RPC框架： Zookeeper + Dubbo
- 缓存框架： Redis
- 消息机制： Kafka
- 检索系统： ElasticSearch
- Js框架： Vue.js
- UI框架：
  1. 后端： element-ui
  2. 前端： cube-ui

## 软件环境
- JDK1.8
- MySQL5.7

## 准备事项
- 准备
  1. OSS仓库
  2. 短信发送平台
- 修改
  1. 上传文件发送短信在```books-utils```中， 修改```resources/profile/config.*.properties```的配置项
  2. 上传文件和发送短信具体实现在```books-utils/src/main/java/com/**/utils/```下
  3. 也可根据自己的平台进行单独修改

## 本地部署： 使用idea为开发工具
> 说明

该系统依赖X_Util工具项目， 需要在maven的配置文件中进行仓库配置
```xml
<!--在profiles标签内-->
<profile>
    <id>jitpack.io</id>
    <activation>
        <activeByDefault>false</activeByDefault>
        <jdk>1.8</jdk>
    </activation>
    <repositories>
        <!-- jitpack.io地址-->
        <repository>
        <id>jitpack.io</id>
        <url>https://www.jitpack.io</url>
        <releases>
            <enabled>true</enabled>
        </releases>
        <snapshots>
            <enabled>true</enabled>
        </snapshots>
        </repository>
    </repositories>   
</profile>

<!--紧邻profiles标签-->
<activeProfiles>
    <activeProfile>jitpack.io</activeProfile>
</activeProfiles>
```

1. 将项目导入到idea中， 

2. 分别将项目中的以下模块依次执行clean， install

```lua
- books-config                    -- 配置文件
- books-facade                    -- 接口， 实体
- books-search-facade             -- 搜索接口， 实体
- books-quartz                    -- 定时任务
- kafka-producer                  -- 消息队列
```

3. 执行books-provider项目中StartApp和books-search项目中的StartApp，启动服务端

4. tomcat启动books-admin（后台接口），books-api（web端接口）， books-utils（工具类单独项目）， kafka-consumer（消息处理项目）

5. 前端
    - 前端采用vue项目， 所以直接通过npm run dev启动

6. 数据库配置

> 在数据爬取过程中可能会遇到如表情等4个字节的数据， 所以数据库， 表， 字段的格式必须是 **utf8mb4** 
    
> my.cnf 的配置

```
[mysqld]
sql_mode='STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION'
max_connections=200
character_set_server=utf8mb4
character-set-client-handshake=FALSE
collation-server=utf8mb4_unicode_ci
init_connect='SET NAMES utf8mb4'

[client]
default-character-set=utf8mb4
character_set_client=utf8mb4

[mysql]
default-character-set=utf8mb4
```

同时还需要在客户端设置

```
set names utf8mb4
```

> 检查是否设置成功


```
SHOW VARIABLES WHERE Variable_name LIKE 'character_set_%' OR Variable_name LIKE 'collation%';
```


## 博客内容
以上关于环境配置， 接口安全，Kafka、ES的使用都可在我的掘金博客中找到

[谢大大的掘金](https://juejin.im/user/5d358b226fb9a07ebb057152/posts)

[谢先生的GitHub](https://github.com/xiezhyan/X_Util)

## 写到最后的话

1. 本系统开源只提供参考和学习

2. 其他可定制系统
    - [流量变现平台：快速搭建你的广告联盟系统](http://thanks_studio.gitee.io/adsence/)
    - [便捷后台开发框架：持续更新中](https://gitee.com/thanks_studio/cabinet)

## License

享阅读 is [GPL v2.0 licensed](./LICENSE).

## 配图
### H5
![输入图片说明](https://images.gitee.com/uploads/images/2019/1011/112103_e7f32a19_1367523.png "1.PNG")
![输入图片说明](https://images.gitee.com/uploads/images/2019/1011/112134_8a7f8931_1367523.png "2.PNG")
![输入图片说明](https://images.gitee.com/uploads/images/2019/1011/112141_d61e9fc2_1367523.png "3.PNG")
![输入图片说明](https://images.gitee.com/uploads/images/2019/1011/112148_7ffbf70c_1367523.png "4.PNG")
![输入图片说明](https://images.gitee.com/uploads/images/2019/1011/112154_e4095791_1367523.png "5.PNG")
![输入图片说明](https://images.gitee.com/uploads/images/2019/1011/112202_04274ea5_1367523.png "6.PNG")
![输入图片说明](https://images.gitee.com/uploads/images/2019/1011/112209_6e2475ca_1367523.png "7.PNG")

### 后台
![输入图片说明](https://images.gitee.com/uploads/images/2019/1011/113941_e042bec2_1367523.png "1.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/1011/113947_8e975397_1367523.png "2.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/1011/113955_847e17ec_1367523.png "3.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/1011/114001_8663691f_1367523.png "4.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/1011/114008_104120c9_1367523.png "5.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/1011/114015_8bd69277_1367523.png "6.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/1011/114022_3b84abcb_1367523.png "7.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/1011/114031_a2640085_1367523.png "8.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/1011/114037_05058582_1367523.png "9.png")