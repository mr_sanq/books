import Vue from 'vue'
import './cube-ui'
import App from './App.vue'
import router from './router'
import store from './store'

//fastclick
import FastClick from 'fastclick'
FastClick.attach(document.body)

//解决浏览器白屏, 保证兼容性
import 'babel-polyfill'

//设置时间格式
import moment from 'moment/moment'
Vue.filter('datetime', function (value, formatString) {
  formatString = formatString || 'YYYY-MM-DD HH:mm:ss';
  return moment(value).format(formatString);
})
Vue.filter('date', function (value, formatString) {
  formatString = formatString || 'YYYY-MM-DD';
  return moment(value).format(formatString);
})
Vue.filter('month-day', function (value, formatString) {
  formatString = formatString || 'MM-DD';
  return moment(value).format(formatString);
})

import 'assets/css/styles.css'
import 'amfe-flexible'

//TODO 需要换掉
import load from 'assets/img/load.png'

//懒加载
import lazyload from "vue-lazyload"
Vue.use(
  lazyload, { loading: load, error: load }
)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
