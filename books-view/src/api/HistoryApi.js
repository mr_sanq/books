import $axios from 'utils/Request.js'

//列表分页
export const getHistoryList = data => {
    return $axios({
        url: '/app/history/list',
        method: 'get',
        data
    });
}
//保存
export const saveHistory = data => {
    return $axios({
        url: '/app/history/save',
        method: 'post',
        data
    });
}
