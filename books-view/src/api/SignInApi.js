import $axios from 'utils/Request.js'

//保存
export const saveSignIn = data => {
    return $axios({
        url: '/app/sign_in/save',
        method: 'post',
        data
    });
}

export const validateTodayIsSign = data => {
    return $axios({
        url: '/app/sign_in/validateTodayIsSign',
        method: 'get',
        data
    });
}

export const findSignInList = data => {
    return $axios({
        url: '/app/sign_in/all',
        method: 'get',
        data
    });
}

export const getSignByUserAndTime = data => {
    return $axios({
        url: '/app/sign/getSignByUserAndTime',
        method: 'get',
        data
    });
}