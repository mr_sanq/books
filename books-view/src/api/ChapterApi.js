import $axios from 'utils/Request.js'

export const getChapterList = data => {
    return $axios({
        url: '/app/chapter/list',
        method: 'get',
        data
    });
}

export const findTotalByBookId = data => {
    return $axios({
        url: '/app/chapter/findTotalByBookId',
        method: 'get',
        data
    });
}

export const findChapterContentByBookIdAndChapter = data => {
    return $axios({
        url: '/app/chapter/findChapterContentByBookIdAndChapter',
        method: 'get',
        data
    });
}
