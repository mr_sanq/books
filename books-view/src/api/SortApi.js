import $axios from 'utils/Request.js'

//获取所有数据
export const getSortAllList = data => {
    return $axios({
        url: '/app/sort/all',
        method: 'get',
        data
    });
}

export const getSortById = data => {
    return $axios({
        url: '/app/sort/get/' + data.id,
        method: 'get',
        data
    });
}