import $axios from 'utils/Request.js'

//获取所有数据
export const getAdvertiseAllList = data => {
    return $axios({
        url: '/app/advertise/all',
        method: 'get',
        data
    });
}
