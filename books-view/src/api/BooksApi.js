import $axios from 'utils/Request.js'

//列表分页
export const getBooksById = data => {
    return $axios({
        url: '/app/books/get/' + data.id,
        method: 'get',
        data
    });
}

//列表分页
export const getBooksList = data => {
    return $axios({
        url: '/app/books/list',
        method: 'get',
        data
    });
}

//获取所有数据
export const getBooksAllList = data => {
    return $axios({
        url: '/app/books/all',
        method: 'get',
        data
    });
}

//保存
export const saveBooks = data => {
    return $axios({
        url: '/app/books/save',
        method: 'post',
        data
    });
}

//删除数据
export const deleteBooks = data => {
    return $axios({
        url: '/app/books/delete',
        method: 'delete',
        data
    });
}

//修改数据
export const updateBooksById = data => {
    return $axios({
        url: '/app/books/update/' + data.id,
        method: 'put',
        data
    });
}


export const findListBySortIds = data => {
    return $axios({
        url: '/app/books/findListBySortIds',
        method: 'get',
        data
    });
}