import $axios from 'utils/Request.js'

//列表分页
export const getCollectionList = data => {
    return $axios({
        url: '/app/collection/list',
        method: 'get',
        data
    });
}

//保存
export const saveCollection = data => {
    return $axios({
        url: '/app/collection/save',
        method: 'post',
        data
    });
}

//删除数据
export const deleteCollection = data => {
    return $axios({
        url: '/app/collection/delete',
        method: 'delete',
        data
    });
}
