import $axios from 'utils/Request.js'

//列表分页
export const getOrdersList = data => {
    return $axios({
        url: '/app/orders/list',
        method: 'get',
        data
    });
}

//保存
export const saveOrders = data => {
    return $axios({
        url: '/app/orders/save',
        method: 'post',
        data
    });
}
